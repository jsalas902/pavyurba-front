module.exports = {
  packages: {
    'angular2-text-mask': {
      ignorableDeepImportMatchers: [
        /text-mask-core\//,
      ]
    },
    'ngx-toastr':  {
      ignorableDeepImportMatchers: [
        /@angular\//,
      ]
    },
    'angular2_photoswipe':  {
      ignorableDeepImportMatchers: [
        /photoswipe\//,
      ]
    },
    'agm-direction':  {
      ignorableDeepImportMatchers: [
        /@agm\//,
      ]
    },
    'ngx-echarts':  {
      ignorableDeepImportMatchers: [
        /echarts\//,
      ]
    },
  },
};
