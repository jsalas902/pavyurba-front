export class CalendariosValidator {
    public static CALENDARIOS_VALIDATION_MESSAGES = {
        'documento': [
            { message: 'Archivo es requerido' },
        ],        
        'tipo': [
            { type: 'required', message: 'Tipo es requerido' },
        ], 
        'fechaDesde': [
            { type: 'required', message: 'Fecha desde es requerido' },
        ],
        'asistenciaIncidencia': [
            { type: 'required', message: 'Seleccione Asistencia o Incidencia' },
        ],
        // 'fechaFacturacion': [
        //     { type: 'required', message: 'Fecha facturación es requerido' },
        // ],
        'observaciones': [
            { type: 'maxlength', message: 'Las observaciones no pueden tener más de 500 caracteres' }
        ],
        'idObra': [
            { type: 'required', message: 'Obra es requerido' },
        ],
        'idEmpleado': [
            { type: 'required', message: 'Empleado es requerido' },
        ],
    }
}