export class CalendariosTypes {

    public constructor(init?: Partial<CalendariosTypes>) {
      Object.assign(this, init);
      CalendariosTypes.Asistencia = 'ASISTENCIA';
      CalendariosTypes.Incidencia = 'INCIDENCIA';
    }
  
    static Asistencia = 'ASISTENCIA';
    static Incidencia = 'INCIDENCIA';
  }
  