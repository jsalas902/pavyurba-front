export class IncidenciasTypes {

    public constructor(init?: Partial<IncidenciasTypes>) {
      Object.assign(this, init);
      IncidenciasTypes.Vacaciones = 'VACACION';
      IncidenciasTypes.Falta = 'FALTA';
      IncidenciasTypes.Enfermedad = 'ENFERMEDAD';
    }
  
    static Vacaciones = 'VACACION';
    static Falta = 'FALTA';
    static Enfermedad = 'ENFERMEDAD';
  }
  