export class CalendariosIncidencias {

    public constructor(init?: Partial<CalendariosIncidencias>) {
        Object.assign(this, init);
    }

    id: number;
    observaciones: string;
    fechaDesde: any;
    fechaHasta: any;
    idEmpleado: number;
    idObra: number;
    tipo: string;
    incidencia: string;
    documento: any;
}