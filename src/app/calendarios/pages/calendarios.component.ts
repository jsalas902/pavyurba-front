import { Component, OnInit, ChangeDetectorRef, Input, ViewChild, TemplateRef } from '@angular/core';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import {
  CalendarEventTimesChangedEvent,
  CalendarView,
  CalendarEvent,
  CalendarEventAction
} from 'angular-calendar';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

import { ConfirmDialogService } from '@app/_services';
import { BusinessService } from '@app/business/services/business.service';
import { ToastrService } from 'ngx-toastr';
import { Business } from '@app/business/models/Business';
import { Router } from '@angular/router';
import { Pagination } from '@app/_models';
import * as moment from 'moment';
import 'moment/locale/es';

import { CalendariosService } from '@app/calendarios/services/calendarios.service';
import { CalendariosIncidencias } from '@app/calendarios/models/CalendariosIncidencias';

@Component({
  selector: 'app-calendarios',
  templateUrl: './calendarios.component.html',
  styleUrls: ['./calendarios.component.css']
})
export class CalendariosComponent implements OnInit {

  @BlockUI('componentsCalendario') blockUIList: NgBlockUI;

  @Input() idCalendariosListar: string;
  titelModal: string;
  tipoEntrada: string;

  public breadcrumb: any;
  options = {
    close: true,
    expand: true,
    minimize: true,
    reload: true
  };

  @BlockUI('events') blockUIEvents: NgBlockUI;

  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  locale: string = 'es';

  CalendarView = CalendarView;

  viewDate: Date = new Date();
  activeDayIsOpen = false;
  modalData: {
    action: string;
    event: CalendarEvent;
  };

  diaSelect;
  fechaSelect;
  mesSelect;
  mes;
  anno;
  dia;

  rows: CalendariosIncidencias[];
  rowsFiltered: CalendariosIncidencias[]; 
  pagination: Pagination;
  listaCalendarios: CalendariosIncidencias[];
  cargaCompleta = false;
  
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  private modalRef;

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [
    /*// {
    //   start: subDays(startOfDay(new Date()), 1),
    //   end: addDays(new Date(), 1),
    //   title: 'INCIDENCIA',
    //   color: colors.red,
    //   actions: this.actions,
    //   allDay: true,
    //   resizable: {
    //     beforeStart: true,
    //     afterEnd: true
    //   },
    //   draggable: true
    // },
    {
      start: subDays(startOfDay(new Date()), 1),
      title: 'ASISTENCIA',
      meta : {
        type : 1
      }
    }, 
    {
      start: startOfDay(new Date()),
      title: 'INCIDENCIA',
      meta : {
        type : 3
      }
    },     
    {
      start: startOfDay(new Date()),
      title: 'ASISTENCIA',
      meta : {
        type : 3
      }
    },
    {
      start: subDays(endOfMonth(new Date()), 3),
      end: addDays(endOfMonth(new Date()), 3),
      title: 'ASISTENCIA',
      meta : {
        type : 5
      }
    },
    {
      start: addHours(startOfDay(new Date()), 2),
      end: new Date(),
      title: 'INCIDENCIA',
      meta : {
        type : 7
      }
    },
    // {
    //   start: subDays(endOfMonth(new Date()), 3),
    //   end: addDays(endOfMonth(new Date()), 3),
    //   title: 'INCIDENCIA',
    //   color: colors.blue,
    //   allDay: true
    // },
    // {
    //   start: addHours(startOfDay(new Date()), 2),
    //   end: new Date(),
    //   title: 'INCIDENCIA',
    //   color: colors.yellow,
    //   actions: this.actions,
    //   resizable: {
    //     beforeStart: true,
    //     afterEnd: true
    //   },
    //   draggable: true
    // }*/
  ];

  

  ngOnInit() {
    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = '';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;

    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    this.getAllData();
    this.getAllDataCalendar();
  }
  constructor(private modal: NgbModal,
    private toastr: ToastrService,
    private confirmDialogService: ConfirmDialogService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private modalService: NgbModal,
    private calendariosService: CalendariosService,
    private businessService: BusinessService) {
      moment.locale('es');
      this.diaSelect = moment(this.viewDate).format('dddd');
      this.fechaSelect = moment(this.viewDate).format('DD [de ]');
      this.mesSelect = moment(this.viewDate).format('MMMM');

      this.dia = moment(this.viewDate).format('DD');
      this.mes = moment(this.viewDate).format('MM');
      this.anno = moment(this.viewDate).format('YYYY');
    }

  dayToday() {
    this.diaSelect = moment(this.viewDate).format('dddd');
    this.fechaSelect = moment(this.viewDate).format('DD [de ]');
    this.mesSelect = moment(this.viewDate).format('MMMM');

    this.dia = moment(this.viewDate).format('DD');
    this.mes = moment(this.viewDate).format('MM');
    this.anno = moment(this.viewDate).format('YYYY');

    this.getAllData();
    this.getAllDataCalendar();
  }

  monthChange() {
    // this.dia = moment(this.viewDate).format('DD');
    this.mes = moment(this.viewDate).format('MM');
    this.anno = moment(this.viewDate).format('YYYY');
    this.getAllDataCalendar();
  }
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    this.diaSelect = moment(date).format('dddd');
    this.fechaSelect = moment(date).format('DD [de ]');
    this.mesSelect = moment(date).format('MMMM');

    this.dia = moment(date).format('DD');
    this.mes = moment(date).format('MM');
    this.anno = moment(date).format('YYYY');

    this.getAllData();

    
    // if (isSameMonth(date, this.viewDate)) {
    //   this.viewDate = date;
    //   if (
    //     (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
    //     events.length === 0
    //   ) {
    //     this.activeDayIsOpen = false;
    //   } else {
    //     this.activeDayIsOpen = true;
    //   }
    // }
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  reloadEvents() {
    this.blockUIEvents.start('Loading..');

    setTimeout(() => {
      this.blockUIEvents.stop();
    }, 2500);
  }

  ngAfterViewInit(): void {
    this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();
    this.calendariosService.ListarTodosGrid(this.anno, this.mes, this.dia).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaDesde = moment(element.fechaDesde).format('DD/MM');
          element.fechaHasta = moment(element.fechaHasta).format('DD/MM');
        });
        this.listaCalendarios = list;
        this.listaCalendarios = [...this.listaCalendarios];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getAllDataCalendar() {
    this.reloadStart();
    this.calendariosService.ListarTodosCalendar(this.anno, this.mes, this.query).subscribe(
      (data) => {
        console.log('lista:',data);

        var list =  data.data;
        this.events=[];
        list.forEach(element => {
          // console.log(element.fecha);
          
          // console.log(startOfDay(element.fecha));
          // console.log(startOfDay(new Date()));
          
          this.events.push({
            start: addDays(element.fecha, 1),
            // start: addDays(element.fecha, 0),
            // end: startOfDay(element.fecha),
            title: element.tipoEntrada,
            meta : {
              type : element.cantidad
            }
          })
        });        
        this.refresh.next();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaCalendarios;
    this.rowsFiltered = this.listaCalendarios;
  }


  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, row: any) {
    if(row !== null){
      this.idCalendariosListar =  row.id.toString();
      this.titelModal = 'Editar Entrada de Calendario';
      this.tipoEntrada = row.tipoEntrada;
    }else{
      this.idCalendariosListar =  null;
      this.tipoEntrada = null;
      this.titelModal = 'Nueva Entrada en Calendario';  // Necesario para setear el titulo   
    }
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: 'lg' });
  }
  onCloseModal(e) {
    this.modalRef.close();
    this.getAllData();
    this.getAllDataCalendar();
  }

  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.calendariosService.ListarTodosGrid(this.anno, this.mes, this.dia).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaDesde = moment(element.fechaDesde).format('DD/MM');
          element.fechaHasta = moment(element.fechaHasta).format('DD/MM');
        });
        this.listaCalendarios = list;
        this.listaCalendarios = [...this.listaCalendarios];
        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  
  downloadFile(row) {
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = row.adjunto.url;
    a.target = "_blank";
    a.download = "TEST";
    a.click();
    a.remove();
  }

  CountAsistencias(day: any): number {

    let count = 0;
    day.events.forEach(element => {
      if(element.title === "ASISTENCIA") {
        count += element.meta.type;
      }
    });
    
    return count;
  }

  CountIncidencias(day: any): number {
    // console.log(day);
    let count = 0;
    day.events.forEach(element => {
      if(element.title === "INCIDENCIA") {
        count += element.meta.type;
      }
    });
    return count;
  }

  removeRegistro(row: any) {
    this.confirmDialogService.confirmThis(`Está seguro que desea eliminar la entrada?`, () =>  {
      this.delete(row);      
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario`);
    });
  }

  delete(row: any) {
    if(row.tipoEntrada == "INCIDENCIA"){
      this.calendariosService.EliminarIncidencia(row.id.toString()).subscribe(
        data => {
          this.toastr.success(`${data.mensaje}`,'Eliminado' , { timeOut: 2500, closeButton: true });
          this.getAllData();
          this.getAllDataCalendar();
        },
        error => {
          this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
      });
    } else {
      this.calendariosService.EliminarCalendario(row.id.toString()).subscribe(
        data => {
          this.toastr.success(`${data.mensaje}`,'Eliminado' , { timeOut: 2500, closeButton: true });
          this.getAllData();
          this.getAllDataCalendar();
        },
        error => {
          this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
      });
    }
  }
}
