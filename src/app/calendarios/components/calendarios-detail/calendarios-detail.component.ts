import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef, Injectable } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbDatepickerI18n, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { Obra } from '@app/obras/models/Obra'
import { ProvidersService } from '@app/providers/services/providers.service';
import { MaterialsService } from '@app/materials/services/materials.service';
import { ObrasService } from '@app/obras/services/obras.service';
import { EmployeesService } from '@app/employees/services/employees.service';
import { CalendariosValidator } from "@app/calendarios/validation/calendarios.validator";
import { CalendariosTypes } from "@app/calendarios/models/constantes/CalendariosTypes";
import { IncidenciasTypes } from "@app/calendarios/models/constantes/IncidenciasTypes";

import { NgbDateCustomParserFormatter} from '@app/_helpers/dateformat';
import { Employee } from '@app/employees/models/Employee';
import { CalendariosIncidencias } from '@app/calendarios/models/CalendariosIncidencias';
import { CalendariosService } from '../../services/calendarios.service';
import { CustomDatepickerI18nService, I18n } from '@app/calendarios/services/custom-datepicker-i18n.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';


// Range datepicker Start
const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;
// Range datepicker Ends

@Component({
  selector: 'app-calendarios-detail',
  templateUrl: './calendarios-detail.component.html',
  styleUrls: ['./calendarios-detail.component.css'],
  providers: [
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18nService }
  ] 
  // providers: [
  //   {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}
  // ]
})
export class CalendariosDetailComponent implements OnInit {

  @Input() idCalendarios: string;
  @Input() tipoEntrada: string;
  @Output() closeModal = new EventEmitter<boolean>();
  @ViewChild('labelImport', { static: true })
  labelImport: ElementRef;
  fileToUpload: File = null;
  @BlockUI('componentsCalendariosDetail') blockUIList: NgBlockUI;

  calendariosIncidencias: CalendariosIncidencias = new CalendariosIncidencias();
  obras: Obra;
  employeesCombo: Employee;
  calendariosForm: FormGroup;
  titleDocumento: string;
  sizeArchivo: number;
  typeArchivo: string;
  baseArchivo: string | ArrayBuffer;
  submitted: boolean = false;
  uploadFile: boolean = false;
  edicion: boolean = false;
  archivoValid: boolean = false;
  fechaDesde;
  fechaHasta;
  porRango: boolean = true;
  asistenciaIncidenciaNg: string = "ASISTENCIA";
  mensaje: string = '';
  maxLength: number = 500;
  // fechaFacturacion;
  d: any;
  d1: any;
  calendariosTypes: any;
  incidenciasTypes: any;
  readonly DELIMITER = '-';
  calendarios_validation_messages = CalendariosValidator.CALENDARIOS_VALIDATION_MESSAGES;
  dateNow = moment().format("YYYY-MM-DD");
  hoy = null;

  // Range datepicker start
  hoveredDate: NgbDateStruct;

  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;
  calendarNew: NgbCalendar;
  
  constructor(
    private toastr: ToastrService,      
    private providersService: ProvidersService,
    private materialsService: MaterialsService,
    private obrasService: ObrasService,
    private employeesService: EmployeesService,
    private calendariosService: CalendariosService,
    private formBuilder: FormBuilder,
    calendar: NgbCalendar
  ) {
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getToday();
    this.hoy = calendar.getToday();
    console.log(this.hoy);
  }

  ngOnInit(): void {
    // this.fromDate = calendar.getToday();
    // this.toDate = this.fromModel(this.dateNow);
    // this.onDateChange(this.fromDate);
    this.calendariosTypes = [{"texto":CalendariosTypes.Asistencia}, {"texto":CalendariosTypes.Incidencia}];
    this.incidenciasTypes = [{"texto":IncidenciasTypes.Vacaciones}, {"texto":IncidenciasTypes.Falta}, {"texto":IncidenciasTypes.Enfermedad}];
    this.titleDocumento = "Subir Documento";
    this.calendariosForm = this.formBuilder.group({
      id: [null],
      porRango: [true],
      tipo: [null],
      observaciones: [null],
      incidencia: [null],
      asistenciaIncidencia: [this.asistenciaIncidenciaNg, [Validators.required]],
      idObra: [null],
      idEmpleado: [null, [Validators.required]],
      fechaDesde: [this.fromDate, [Validators.required]],
      fechaHasta: [null],
      documento: [null],
    });

    if (this.idCalendarios != null) {
      this.edicion = true;
      this.asistenciaIncidencia();
    }
    this.obtenerObras();
    this.obtenerEmployees();
  
  }

  asistenciaIncidencia() {
    if(this.tipoEntrada === "ASISTENCIA") {
      this.calendariosService.ObtenerPorIdCalendar(this.idCalendarios).subscribe(
        data => {
          this.obtenerPatchCalendar(data);
        },
        error => {
          // this.toastr.error(error.mensaje);
          this.toastr.error(error);
        }
      );
    } else {
      this.calendariosService.ObtenerPorIdIncidencia(this.idCalendarios).subscribe(
        data => {
          this.obtenerPatchCalendar(data);
        },
        error => {
          // this.toastr.error(error.mensaje);
          this.toastr.error(error);
        }
      );
    }
  }

  obtenerObras() {
    this.obrasService.ListarObrasCombo().subscribe(
      data => {
        this.obras = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  obtenerEmployees() {
    this.employeesService.ListarEmployeesCombo().subscribe(
      data => {
        this.employeesCombo = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  get f() { return this.calendariosForm.controls; }
  add() {
    this.submitted = true;
    console.log(this.calendariosForm);
    if (this.calendariosForm.invalid) {
      return;
    } else {
      if(this.uploadFile) {
        this.submitted = false;
        this.uploadFile = false;
        return;
      }
    }
    
    this.calendariosIncidencias = this.calendariosForm.value;
    
    // console.log(this.calendariosForm.value instanceof Albaran);

    if((this.calendariosIncidencias.documento == null) && (this.asistenciaIncidenciaNg == "INCIDENCIA") && !this.edicion){
      this.archivoValid = true;
      this.calendariosIncidencias.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModelFix(this.fechaDesde);
      this.calendariosIncidencias.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModelFix(this.fechaHasta);
      // var fechaNew = this.calendariosIncidencias.fechaRecepcion;
      // this.calendariosIncidencias.fechaRecepcion = "";
      // this.calendariosIncidencias.fechaRecepcion = this.toModelFix(this.fechaRecepcion);
      return
    } else {
      this.archivoValid = false
    }
    
    this.reloadStart();
    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }

  }
  create(){
    let calendariosIncidenciasTem = new CalendariosIncidencias();

    this.calendariosIncidencias.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    this.calendariosIncidencias.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);
    calendariosIncidenciasTem = this.changeObject(this.calendariosIncidencias);
    calendariosIncidenciasTem.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    calendariosIncidenciasTem.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);

    calendariosIncidenciasTem.fechaHasta = calendariosIncidenciasTem.fechaHasta == null ? calendariosIncidenciasTem.fechaDesde : calendariosIncidenciasTem.fechaHasta

    if(this.asistenciaIncidenciaNg == "INCIDENCIA"){ 
      var dataIncidencia = {
        id: calendariosIncidenciasTem.id,
        tipo: calendariosIncidenciasTem.tipo,
        incidencia: calendariosIncidenciasTem.incidencia,
        // fechaDesde: calendariosIncidenciasTem.fechaDesde,
        // fechaHasta: calendariosIncidenciasTem.fechaHasta,
        fechaDesde: this.toModel(this.fromDate),
        fechaHasta: (this.toDate != null) ? this.toModel(this.toDate) : this.toModel(this.fromDate),
        observaciones: calendariosIncidenciasTem.observaciones,
        documento: calendariosIncidenciasTem.documento, 
        idEmpleado: calendariosIncidenciasTem.idEmpleado
      }

      this.calendariosService.CrearIncidencia(dataIncidencia).subscribe(
        data => {
          this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
          this.reiniciarFormulario();
          // this.getAllData();
          this.reloadStop();
          // this.closeModal.emit(true);          
        },
        error => {
          // this.reloadStop();
          // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
          this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
          this.reloadStop();
      });
    } else {
      var dataCalendario = {
        id: calendariosIncidenciasTem.id,
        // fechaDesde: calendariosIncidenciasTem.fechaDesde,
        // fechaHasta: calendariosIncidenciasTem.fechaHasta,
        fechaDesde: this.toModel(this.fromDate),
        fechaHasta: (this.toDate != null) ? this.toModel(this.toDate) : this.toModel(this.fromDate),
        observaciones: calendariosIncidenciasTem.observaciones,
        idEmpleado: calendariosIncidenciasTem.idEmpleado,
        idObra: calendariosIncidenciasTem.idObra
      }

      var incidencia = this.asistenciaIncidenciaNg;
      this.onDateChange(this.hoy);
      this.calendariosService.CrearCalendario(dataCalendario).subscribe(
        data => {
          this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
          this.reiniciarFormulario();
          // this.getAllData();
          this.reloadStop();
          // this.closeModal.emit(true);
          this.calendariosForm.patchValue({
            idObra: calendariosIncidenciasTem.idObra,
            asistenciaIncidencia: incidencia,
            fechaDesde: this.hoy,
            fechaHasta: this.hoy,
          });
        },
        error => {
          // this.reloadStop();
          // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
          this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
          this.reloadStop();
      });
    }
  }

  edit(){
    let calendariosIncidenciasTem = new CalendariosIncidencias();

    this.calendariosIncidencias.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    this.calendariosIncidencias.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);
    calendariosIncidenciasTem = this.changeObject(this.calendariosIncidencias);
    calendariosIncidenciasTem.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    calendariosIncidenciasTem.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);

    calendariosIncidenciasTem.fechaHasta = calendariosIncidenciasTem.fechaHasta == null ? calendariosIncidenciasTem.fechaDesde : calendariosIncidenciasTem.fechaHasta
    
    if(this.asistenciaIncidenciaNg == "INCIDENCIA"){ 
      var dataIncidencia = {
        id: calendariosIncidenciasTem.id,
        tipo: calendariosIncidenciasTem.tipo,
        incidencia: calendariosIncidenciasTem.incidencia,
        // fechaDesde: calendariosIncidenciasTem.fechaDesde,
        // fechaHasta: calendariosIncidenciasTem.fechaHasta,
        fechaDesde: this.toModel(this.fromDate),
        fechaHasta: (this.toDate != null) ? this.toModel(this.toDate) : this.toModel(this.fromDate),
        observaciones: calendariosIncidenciasTem.observaciones,
        documento: calendariosIncidenciasTem.documento, 
        idEmpleado: calendariosIncidenciasTem.idEmpleado
      }

      this.calendariosService.EditarIncidencia(dataIncidencia).subscribe(
        data => {
          this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
          this.reiniciarFormulario();
          // this.getAllData();
          this.reloadStop();
          this.closeModal.emit(true);
        },
        error => {
          // this.reloadStop();
          // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
          this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
          this.reloadStop();
      });
    } else {
      var dataCalendario = {
        id: calendariosIncidenciasTem.id,
        // fechaDesde: calendariosIncidenciasTem.fechaDesde,
        // fechaHasta: calendariosIncidenciasTem.fechaHasta,
        fechaDesde: this.toModel(this.fromDate),
        fechaHasta: (this.toDate != null) ? this.toModel(this.toDate) : this.toModel(this.fromDate),
        observaciones: calendariosIncidenciasTem.observaciones,
        idEmpleado: calendariosIncidenciasTem.idEmpleado,
        idObra: calendariosIncidenciasTem.idObra
      }

      this.calendariosService.EditarCalendario(dataCalendario).subscribe(
        data => {
          this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
          this.reiniciarFormulario();
          // this.getAllData();
          this.reloadStop();
          this.closeModal.emit(true);
        },
        error => {
          // this.reloadStop();
          // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
          this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
          this.reloadStop();
      });
    }
  }

  reiniciarFormulario() {
    this.calendariosIncidencias = new CalendariosIncidencias();
    this.calendariosForm.reset();
    this.submitted = false;
  }

  obtenerPatchCalendar(data){
    this.calendariosIncidencias = this.changeObject(data);

    this.asistenciaIncidenciaNg = this.tipoEntrada;

    this.calendariosForm.patchValue({
      id: this.calendariosIncidencias.id,
      fechaDesde: this.fromDate = this.calendariosIncidencias.fechaDesde,
      fechaHasta: this.toDate = this.calendariosIncidencias.fechaHasta,
      tipo: this.calendariosIncidencias.tipo,
      idEmpleado: this.calendariosIncidencias.idEmpleado,
      idObra: this.calendariosIncidencias.idObra,
      observaciones: this.calendariosIncidencias.observaciones,
      asistenciaIncidencia: this.tipoEntrada,
      incidencia: this.calendariosIncidencias.incidencia,
      documento: this.calendariosIncidencias.documento,
    });
  }
  changeObject(calendariosIncidencias: CalendariosIncidencias) {
    let calendariosIncidenciasTem: CalendariosIncidencias = new CalendariosIncidencias();
    calendariosIncidenciasTem.id = (calendariosIncidencias.id != null) ? calendariosIncidencias.id : 0;
    calendariosIncidenciasTem.tipo = calendariosIncidencias.tipo;
    calendariosIncidenciasTem.incidencia = calendariosIncidencias.incidencia;
    calendariosIncidenciasTem.idEmpleado = calendariosIncidencias.idEmpleado;
    calendariosIncidenciasTem.fechaDesde = this.fechaDesde = (calendariosIncidencias.fechaDesde !== null) ? this.fromModel(calendariosIncidencias.fechaDesde) : null;
    calendariosIncidenciasTem.fechaHasta = this.fechaHasta = (calendariosIncidencias.fechaHasta !== null) ? this.fromModel(calendariosIncidencias.fechaHasta) : null;
    calendariosIncidenciasTem.idObra = calendariosIncidencias.idObra;
    calendariosIncidenciasTem.observaciones = calendariosIncidencias.observaciones;
    if(calendariosIncidencias.documento != null && calendariosIncidencias.documento != undefined){
      calendariosIncidenciasTem.documento = {
        "base64": this.baseArchivo,
        "nombre": this.titleDocumento,
        "mimeType": this.typeArchivo,
        "tamano": this.sizeArchivo  
      };
    } else {
      calendariosIncidenciasTem.documento = null;
    }

    return calendariosIncidenciasTem;
  }
  toModel(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format();
  }
  toModelFix(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format('DD-MM-YYYY');
  }
  onFileChange(event: any) {
    this.fileToUpload  = event.target.files.item(0);

    var reader = new FileReader();
    reader.onload = this.convertFileSimple.bind(this, this.fileToUpload);
    reader.readAsBinaryString(this.fileToUpload);
  }

  convertFileSimple(file: File, event: any) {
    const binaryString = event.target.result;
    this.titleDocumento = file.name;
    this.sizeArchivo = file.size;
    this.typeArchivo = file.type;
    this.baseArchivo = btoa(binaryString);
    this.archivoValid = false
    this.uploadFile = false;
  }
  

  document() {
    this.uploadFile = true;
    let element: HTMLElement = document.getElementById('documento') as HTMLElement;
    element.click();
  }
  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[2], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[0], 10)
      };
    }
    return null;
  }

  changeRango(){
    this.porRango = !this.porRango;
  }

  changeAsistenciaIncidencia(){
    console.log(this.asistenciaIncidenciaNg)
  }

  // hoveredDate: NgbDateStruct | null = null;

  // fromDate: NgbDateStruct;
  // toDate: NgbDateStruct | null = null;

  // constructor(calendar: NgbCalendar) {
  //   this.fromDate = calendar.getToday();
  //   this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);
  // }

  // onDateChange(date: NgbDate) {
  //   if (!this.fromDate && !this.toDate) {
  //     this.fromDate = date;
  //   } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
  //     this.toDate = date;
  //   } else {
  //     this.toDate = null;
  //     this.fromDate = date;
  //   }
  // }

  // isHovered(date: NgbDate) {
  //   return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  // }

  // isInside(date: NgbDate) {
  //   return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  // }

  // isRange(date: NgbDate) {
  //   return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  // }
  // Range datepicker starts
  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  onDateChange(date: NgbDateStruct) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && after(date, this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate);
  isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);
  // Range datepicker ends

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }
}
