﻿import { Routes, RouterModule } from '@angular/router';
import { PublicLayoutComponent } from './_layout/public-layout/public-layout.component';
import { PrivateLayoutComponent } from './_layout/private-layout/private-layout.component';
import { AuthGuard } from './_guards/auth.guard';
import { RegisterComponent } from './register';
import { LoginComponent } from './login';
import { ChangelogComponent } from './changelog/changelog.component';
import { FullLayoutComponent } from './_layout/full-layout/full-layout.component';
import { PrivacyPolicyComponent } from './login/privacy-policy/privacy-policy.component';
import { TermsConditionComponent } from './login/terms-condition/terms-condition.component';
import { HomeComponent } from './_layout/home/home.component';

const appRoutes: Routes = [
  { path: 'privacypolicy', component: PrivacyPolicyComponent },
  { path: 'termCondition', component: TermsConditionComponent },
  // Public layout
  {
    path: '',
    component: PublicLayoutComponent,
    children: [
      { path: 'register', component: RegisterComponent,
        //canActivate: [AuthGuard],
        //data: { roles: Roles.Everything }
      },
      { path: 'login', component: LoginComponent,
        //canActivate: [AuthGuard],
        //data: { roles: Roles.Everything }
      },
      { path: '', component: LoginComponent },

    ]
  },
  {
    path: '',
    component: PrivateLayoutComponent,
    children: [
      { path: 'logout', component: LoginComponent, canActivate: [AuthGuard] },
      { path: 'changelog', component: ChangelogComponent, canActivate: [AuthGuard] },
      { path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuard],
        //data: { roles: Roles.Everything }
      },   
      { path: 'business', loadChildren: () => import('../app/business/business.module').then(m => m.BusinessModule)
      , canActivate: [AuthGuard] },    
      { path: 'clients', loadChildren: () => import('../app/clients/clients.module').then(m => m.ClientsModule)
      , canActivate: [AuthGuard] },    
      { path: 'providers', loadChildren: () => import('../app/providers/providers.module').then(m => m.ProvidersModule)
      , canActivate: [AuthGuard] },    
      { path: 'materials', loadChildren: () => import('../app/materials/materials.module').then(m => m.MaterialsModule)
      , canActivate: [AuthGuard] },    
      { path: 'albaranes', loadChildren: () => import('../app/albaranes/albaranes.module').then(m => m.AlbaranesModule)
      , canActivate: [AuthGuard] },    
      { path: 'builders', loadChildren: () => import('../app/builders/builders.module').then(m => m.BuildersModule)
      , canActivate: [AuthGuard] },    
      { path: 'settings', loadChildren: () => import('../app/settings/settings.module').then(m => m.SettingsModule)
      , canActivate: [AuthGuard] },    
      { path: 'dietas', loadChildren: () => import('../app/dietas/dietas.module').then(m => m.DietasModule)
      , canActivate: [AuthGuard] },    
      { path: 'employees', loadChildren: () => import('../app/employees/employees.module').then(m => m.EmployeesModule)
      , canActivate: [AuthGuard] },    
      { path: 'obras', loadChildren: () => import('../app/obras/obras.module').then(m => m.ObrasModule)
      , canActivate: [AuthGuard] },  
      { path: 'huchas', loadChildren: () => import('../app/huchas/huchas.module').then(m => m.HuchasModule)
      , canActivate: [AuthGuard] },  
      { path: 'contratos', loadChildren: () => import('../app/contratos/contratos.module').then(m => m.ContratosModule)
      , canActivate: [AuthGuard] },    
      { path: 'facturas', loadChildren: () => import('../app/facturas/facturas.module').then(m => m.FacturasModule)
      , canActivate: [AuthGuard] },  
      { path: 'parte-trabajo', loadChildren: () => import('../app/parte-trabajo/parte-trabajo.module').then(m => m.ParteTrabajoModule)
      , canActivate: [AuthGuard] }, 
      { path: 'informes', loadChildren: () => import('../app/informes/informes.module').then(m => m.InformesModule)
      , canActivate: [AuthGuard] },  
      { path: 'gastos-generales', loadChildren: () => import('../app/gastos-generales/gastos-generales.module').then(m => m.GastosGeneralesModule)
      , canActivate: [AuthGuard] }, 
      { path: 'nominas', loadChildren: () => import('../app/nominas/nominas.module').then(m => m.NominasModule)
      , canActivate: [AuthGuard] },
      { path: 'calendarios', loadChildren: () => import('../app/calendarios/calendarios.module').then(m => m.CalendariosModule)
      , canActivate: [AuthGuard] },    
      { path: 'estadisticas', loadChildren: () => import('../app/estadisticas/estadisticas.module').then(m => m.EstadisticasModule)
      , canActivate: [AuthGuard] }, 
      // { path: 'Containers', loadChildren: () => import('../app/components/containers/containers.module').then(m => m.ContainersModule)
      // , canActivate: [AuthGuard] },    
      // { path: 'brands', loadChildren: () => import('../app/components/brands/brands.module').then(m => m.BrandsModule)
      // , canActivate: [AuthGuard] },    
      // { path: 'orders', loadChildren: () => import('../app/components/orders/orders.module').then(m => m.OrdersModule)
      // , canActivate: [AuthGuard] },  
      // { path: 'samples', loadChildren: () => import('../app/components/samples/samples.module').then(m => m.SamplesModule)
      // , canActivate: [AuthGuard] },

      // { path: 'IncidentsReports',
      //   loadChildren: () => import('../app/components/incidents-reports/incidents-reports.module').then(m => m.IncidentsReportsModule),
      //   canActivate: [AuthGuard],
      //   //data: { roles: Roles.Everything }
      // }
    ],
  },
  // otherwise redirect to home
  { path: '**', redirectTo: '/' }
];

export const routing = RouterModule.forRoot(appRoutes, { scrollOffset: [0, 0], scrollPositionRestoration: 'top' });
