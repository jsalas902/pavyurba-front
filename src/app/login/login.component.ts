﻿import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../_services';
import { AlertService } from '../_services/alert.service';
import { UserLogin } from '../_models/login/UserLogin';
import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  isPageLoaded = false;
  userLogin: UserLogin = new UserLogin();

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public authService: AuthenticationService,
    private renderer: Renderer2
  ) {
    this.route.queryParams.subscribe(params => {
      this.returnUrl = params['returnUrl'];
    });
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: [this.userLogin.email, Validators.required],
      clave: [this.userLogin.clave, Validators.required],
      rememberMe: false
    });
    // Remember Me
    if (localStorage.getItem('remember')) {
      this.renderer.removeClass(document.body, 'bg-full-screen-image');
      localStorage.removeItem('currentLayoutStyle');
      this.router.navigate(['/home']);
    } else if (localStorage.getItem('ACCESS_USER_LL_CMS')) {
      // Force logout on login if not remember me
      this.renderer.removeClass(document.body, 'bg-full-screen-image');
      localStorage.removeItem('currentLayoutStyle');
      this.router.navigate(['/home']);
    } else {
      this.authService.logout();
      this.isPageLoaded = true;
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  tryLogin() {
    this.submitted = true;
    this.loading = true;
    if (this.loginForm.invalid) {
      this.loading = false;
      return;
    }

    this.userLogin = new UserLogin(this.loginForm.value);

    this.authService.login(this.userLogin).subscribe(
      data => {
        if (this.loginForm.controls['rememberMe'] && this.loginForm.controls['rememberMe'].value) {
          localStorage.setItem('remember', 'true');
        } else {
          localStorage.removeItem('remember');
        }
        this.ObtenerPerfilUsuario(data.data.access_token);
      },
      error => {
        this.toastr.error(error);
        this.loading = false;
        this.submitted = false;
      }
    );
  }

  ObtenerPerfilUsuario(token: string) {
    this.authService.showUser(token).subscribe(
      data => {
        console.log('perfil:',data.nombre);
        this.authService.loginUser(data, token);
        localStorage.removeItem('currentLayoutStyle');
        let returnUrl = '/home';
        if (this.returnUrl) {
          returnUrl = this.returnUrl;
        }
        this.router.navigate([returnUrl]);
        window.location.href = window.location.href
      },
      error => {
        this.toastr.error(error);
      }
    );
  }
}
