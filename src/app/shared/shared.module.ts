import { NgModule } from '@angular/core';
import { CardModule } from 'src/app/content/partials/general/card/card.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ToastrModule } from 'ngx-toastr';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderComponent } from './components/loader/loader.component';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { FormWizardModule } from './components/form-wizard/form-wizard.module';
import { BlockUIModule } from 'ng-block-ui';
import { BlockTemplateComponent } from '../_layout/blockui/block-template.component';
import { BreadcrumbModule } from '../_layout/breadcrumb/breadcrumb.module';
import { Angular2PhotoswipeModule } from 'angular2_photoswipe';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedPipesModule } from './pipes/shared-pipes.module';
import { LocationsModule } from '@app/locations/locations.module';

import { SharedComponentsModule } from './components/shared-components.module';
import { ConfirmDeleteComponent } from './components/confirm-delete/confirm-delete.component';
import { ConfirmDefaultComponent } from './components/confirm-default/confirm-default.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';

@NgModule({
  declarations: [
  ConfirmDeleteComponent,
  ConfirmDefaultComponent,
  LoaderComponent,
  SidebarComponent
],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxDatatableModule.forRoot({
      messages: {
      emptyMessage: 'No hay datos disponibles',
      totalMessage: 'total',
      selectedMessage: 'seleccionado'
      }
    }),
    NgbModule,
    SharedComponentsModule,
    SharedPipesModule,
    LocationsModule,
    PerfectScrollbarModule,
    FormWizardModule,
    CardModule,
    BreadcrumbModule,
    Angular2PhotoswipeModule,
    NgSelectModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-center'
    }),
    BlockUIModule.forRoot({
      template: BlockTemplateComponent
    })
  ],
  exports: [
    ConfirmDeleteComponent,
    ConfirmDefaultComponent,
    SidebarComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule,
    NgxDatatableModule,
    NgbModule,
    PerfectScrollbarModule,
    FormWizardModule,
    SharedComponentsModule,
    SharedPipesModule,
    LocationsModule,
    LoaderComponent,
    CardModule,
    BlockUIModule,
    BreadcrumbModule,
    Angular2PhotoswipeModule,
    NgSelectModule
  ],
  providers: [
    DatePipe,
    // { 
    //   provide: HTTP_INTERCEPTORS, 
    //   useClass: LoaderInterceptor, 
    //   multi: true 
    // }
  ],
  entryComponents: [ ConfirmDeleteComponent, ConfirmDefaultComponent],
})
export class SharedModule { }
