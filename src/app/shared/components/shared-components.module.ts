import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BtnLoadingComponent } from './btn-loading/btn-loading.component';
import { FeatherIconComponent } from './feather-icon/feather-icon.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { SharedPipesModule } from '../pipes/shared-pipes.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlockUIModule } from 'ng-block-ui';
import { BlockTemplateComponent } from 'src/app/_layout/blockui/block-template.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormWizardModule } from './form-wizard/form-wizard.module';
import { CardModule } from 'src/app/content/partials/general/card/card.module';
import { BreadcrumbModule } from 'src/app/_layout/breadcrumb/breadcrumb.module';
import { Angular2PhotoswipeModule } from 'angular2_photoswipe';
import { NgSelectModule } from '@ng-select/ng-select';
import { ToastrModule } from 'ngx-toastr';
import { CustomListComponent } from './list/custom-list.component';
import { CustomButtonComponent } from './buttons/custom-button.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { CalendariosObrasComponent } from '@app/estadisticas/components/calendarios-obras/calendarios-obras.component';
import { CalendariosObrasDetailComponent } from '@app/estadisticas/components/calendarios-obras/calendarios-obras-detail/calendarios-obras-detail.component';


const components = [
  BtnLoadingComponent,
  FeatherIconComponent,
  CustomButtonComponent,
  CustomListComponent,
  ConfirmDialogComponent,
  CalendariosObrasComponent,
  CalendariosObrasDetailComponent
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedPipesModule,
    PerfectScrollbarModule,
    NgbModule,
    HttpClientModule,
    NgxDatatableModule.forRoot({
      messages: {
      emptyMessage: 'No hay datos disponibles',
      totalMessage: 'total',
      selectedMessage: 'seleccionado'
      }
    }),
    FormWizardModule,
    CardModule,
    BreadcrumbModule,
    Angular2PhotoswipeModule,
    NgSelectModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-center'
    }),
    BlockUIModule.forRoot({
      template: BlockTemplateComponent
    })
  ],
  declarations: components,
  exports: components,
  entryComponents: [ ConfirmDialogComponent],
})
export class SharedComponentsModule { }
