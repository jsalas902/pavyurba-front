import { Component, Input, OnInit } from '@angular/core';

import { Business } from 'src/app/business/models/Business'
import { Head } from 'src/app/_models/shares/Head'
import { Pagination } from '@app/_models';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { EmployeesService } from '@app/employees/services/employees.service';
import { IncidentsService } from '@app/_services/incidents.service';

@Component({
  selector: 'app-custom-list',
  templateUrl: './custom-list.component.html',
  styleUrls: ['./custom-list.component.css']
})
export class CustomListComponent implements OnInit {
  @BlockUI('componentsList') blockUIComponetsList: NgBlockUI;
  @Input() idListar: string;
  @Input() rowsthead: Head[];
  @Input() switchCase: string;
  @Input() isFooter: boolean;
  @Input() isHead: boolean;
  rows: any;
  pagination: Pagination;
  query: string;
  skip: string;
  take: string;

  constructor(
    private toastr: ToastrService,
    private employeesService: EmployeesService,
    private incidentsService: IncidentsService,
    ) { }

  ngOnInit(): void {
    this.reloadStart();
    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = ' ';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.onswitchCase();
  }
  onswitchCase (){
    switch (this.switchCase) {
      case 'hist-cambios':
        this.obtenerHistory();
        break;
      case 'incidencias':
        this.obtenerIncidencias();
        break;
      case 'calendario':
        this.obtenerCalendarioEmpleado();
        break;
    
      default:
        break;
    }
  }
  obtenerHistory() {
    this.employeesService.ListarHistorico(this.skip, this.take, this.query, this.idListar).subscribe(
      data => {
        this.rows = data.data;
        this.reloadStop();
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  } 
  obtenerIncidencias() {
    this.incidentsService.ListarIncidencias(this.skip, this.take, this.query, this.idListar).subscribe(
      data => {
        this.rows = data.data;
        this.reloadStop();
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  } 
  // Corregir con el servicio de calendario
  obtenerCalendarioEmpleado() {
    this.incidentsService.ListarIncidencias(this.skip, this.take, this.query, this.idListar).subscribe(
      data => {
        this.rows = [];
        this.reloadStop();
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  } 
  pageChanged(page: number): void {
    this.pagination.current_page = page;
    this.onswitchCase();
  }
  refreshCountries() {
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.onswitchCase();
  }
  reloadStart() {
    this.blockUIComponetsList.start('Loading..');
  }

  reloadStop() {
    this.blockUIComponetsList.stop();
    this.blockUIComponetsList.isActive = false;
    this.blockUIComponetsList.reset();
  }
}
