import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { SharedService } from 'src/app/_services/shared.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  isLoading: Subject<boolean> = this.sharedService.isLoading;

  constructor(private sharedService: SharedService) {}

  ngOnInit() {
  }

}
