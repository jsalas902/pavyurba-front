import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDefaultComponent } from './confirm-default.component';

describe('ConfirmDefaultComponent', () => {
  let component: ConfirmDefaultComponent;
  let fixture: ComponentFixture<ConfirmDefaultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmDefaultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
