import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  @Input() _menuSettingsConfig: any;
  @Output() pageActive = new EventEmitter<string>();

  public config: PerfectScrollbarConfigInterface = { wheelPropagation: false };

  constructor() { }

  ngOnInit(): void {
  }
  resetOtherActiveMenu() {
    for (let i = 0; i < this._menuSettingsConfig.vertical_menu.items.length; i++) {
      this._menuSettingsConfig.vertical_menu.items[i]['isSelected'] = false;
      this._menuSettingsConfig.vertical_menu.items[i]['hover'] = false;
    }
  }
  toggleMenu(event, child, isSubmenuOfSubmenu) {
    this.resetOtherActiveMenu();
    this.pageActive.emit(child.page);
    if (child['isSelected'] === true) {
      child['isSelected'] = false;
    } else {
      child['isSelected'] = true;
    }

    if (child['hover'] === true) {
      child['hover'] = false;
    } else {
      child['hover'] = true;
    }   
  }
}
