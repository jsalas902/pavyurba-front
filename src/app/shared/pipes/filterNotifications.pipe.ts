import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filternotifications',
    pure: false
})

export class FilterNotificationsPipe implements PipeTransform {
    transform(items: any[], args: any[]): any {
        return items.filter(item => item.read.toString().indexOf(args['arg']) !== -1);
    }
}