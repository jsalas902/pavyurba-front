export class LocationsValidator {
    public static LOCATIONS_VALIDATION_MESSAGES = {        
        'localidad': [
            { type: 'required', message: 'Localidad es requerido' },
            { type: 'maxlength', message: 'Localidad no puede tener mas de 100 caracteres' }
        ],
        'provincia': [
            { type: 'required', message: 'Provincia es requerido' },
            { type: 'maxlength', message: 'Provincia no puede tener mas de 100 caracteres' }
        ],
    }
}