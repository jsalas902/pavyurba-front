export class Location {

    public constructor(init?: Partial<Location>) {
        Object.assign(this, init);
    }

    id: number;
    idProvincia: string;
    localidad: string;
    habilitado: boolean;
    fechaHoraLog: Date;
    usuarioLog: string ;
    selected: boolean;

}