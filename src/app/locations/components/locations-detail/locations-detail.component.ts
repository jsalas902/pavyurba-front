import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@app/locations/models/Location';

import { LocationsService } from '@app/locations/services/locations.service';
import { LocationsValidator } from "@app/locations/validation/locations.validator";

@Component({
  selector: 'app-locations-detail',
  templateUrl: './locations-detail.component.html',
  styleUrls: ['./locations-detail.component.css']
})
export class LocationsDetailComponent implements OnInit {

  @Input() idLocations: string;
  @Output() closeModal = new EventEmitter<boolean>();

  locations: Location = new Location();
  provincias: any;
  locationsForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  locations_validation_messages = LocationsValidator.LOCATIONS_VALIDATION_MESSAGES;

  
  constructor(
    private toastr: ToastrService, 
    private locationsService: LocationsService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.locationsForm = this.formBuilder.group({
      id: [null],
      idProvincia: [null, [Validators.required]],
      localidad: [null, [Validators.required, Validators.maxLength(100)]],
    });

    if (this.idLocations != null) {
      this.edicion = true;
      this.obtenerLocations();
    }
    this.obtenerProvincias();
  }

  obtenerProvincias() {
    this.locationsService.ListarProvinciasCombo().subscribe(
      data => {
        this.provincias = data;
      },
      error => {
        // this.toastr.error(error);
        this.toastr.error(error.mensaje);
      }
    );
  }

  obtenerLocations() {
    this.locationsService.ObtenerPorId(this.idLocations).subscribe(
      data => {
        this.obtenerPatchLocation(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  get f() { return this.locationsForm.controls; }
  add() {
    this.submitted = true;

    if (this.locationsForm.invalid) {
      return;
    }
    
    this.locations = this.locationsForm.value;
    console.log(this.locationsForm.value instanceof Location);
    
    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }

  }
  create(){
    let locationsTem = new Location();
    locationsTem = this.changeObject(this.locations);
   
    this.locationsService.Crear(locationsTem).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
        this.closeModal.emit(false);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  edit(){
    let locationsTem = new Location();
    locationsTem = this.changeObject(this.locations);

    this.locationsService.Editar(locationsTem).subscribe(
      data => {
        this.closeModal.emit(false);
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  reiniciarFormulario() {
    this.locations = new Location();
    this.locationsForm.reset();
    this.submitted = false;
  }

  obtenerPatchLocation(data){
    this.locations = this.changeObject(data);
    this.locationsForm.patchValue({
      id: this.locations.id,
      localidad: this.locations.localidad,
      idProvincia: this.locations.idProvincia,
    });
  }
  changeObject(locations: Location) {
    let locationsTem: Location = new Location();
    locationsTem.id = locations.id;
    locationsTem.localidad = locations.localidad;
    locationsTem.idProvincia = locations.idProvincia;
    return locationsTem;
  }

}
