import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { LocationsService } from '@app/locations/services/locations.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-detail',
  templateUrl: './modal-detail.component.html',
  styleUrls: ['./modal-detail.component.css']
})
export class ModalDetailComponent implements OnInit {
  @Input() idLocationsModal: string;
  @Input() titel: string;
  @Output() setModal = new EventEmitter<boolean>();
  cargaCompletaModal:boolean;

  constructor(private modalService: NgbModal,private locationsService: LocationsService) { }

  ngOnInit(): void {
    this.cargaCompletaModal = false;
  }
  
  ngAfterViewInit(): void {
    this.cargaCompletaModal = true;
  }

  onCloseModal(e) {
    if (!e) this.setModal.emit(true);
        
    this.locationsService.onCloseModal();
  }
}
