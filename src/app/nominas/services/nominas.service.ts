import { Pagination } from '../../_models';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { GenericService, AuthenticationService } from '@app/_services';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Business } from '@app/business/models/Business';

@Injectable({
  providedIn: 'root'
})
export class NominasService  extends GenericService{

  constructor(private http: HttpClient,
    public authService: AuthenticationService,
    private injector: Injector) {
  super();
  }

  ListarTodos(idEmpresa: number, ano: number, mes: number, skip: string, take: string, query: string): Observable<Pagination> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/gastosNominas/listarNominasPorEmpresaMesPaginado/' + idEmpresa + '/' + ano + '/' + mes + '?skip=' + skip + '&take=' + take + '&query=' + query, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  ListarTodosPorObras(idObras: string, tipoFiltroGeneral: number, skip: string, take: string, query: string): Observable<Pagination> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/gastosNominas/listarNominasPorObraPaginado/' + idObras + '?tipoFiltroGeneral=' + tipoFiltroGeneral + '&skip=' + skip + '&take=' + take + '&query=' + query, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  ObtenerPorId(id: string): Observable<Business> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.get(environment.apiUrl + '/gastosNominas/obtenerNominasPorEmpresaMesPorId/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta.data;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  ObtenerPorObraId(id: string): Observable<Business> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.get(environment.apiUrl + '/gastosNominas/obtenerNominasPorObraPorId/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta.data;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }
  
  Crear(nominas: any): Observable<any> {
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));

    return this.http.post(environment.apiUrl + '/gastosNominas/cargarGastosMes', nominas, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  Editar(nominas: any): Observable<any> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));

    return this.http.put(environment.apiUrl + '/gastosNominas/editarGastoNomina/' + nominas.id, nominas, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  EditarPorObra(nominas: any): Observable<any> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));

    return this.http.put(environment.apiUrl + '/gastosNominas/editarGastoNominaPorObra/' + nominas.id, nominas, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }
}

