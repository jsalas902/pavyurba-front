export class Nominas {

    public constructor(init?: Partial<Nominas>) {
        Object.assign(this, init);
    }

    id: number;
    trabajador: string;
    dni: string;
    totalBruto: number;
    embargo: number;
    irpf: number;
    total: number;
    totalSeguridadSocial: number;
    archivo: any;
    idProveedor: string;
    proveedor: string;
    material: string;
    idMaterial: number ;
    idObra: number ;
    obra: string;
    habilitado: boolean;
    fechaHoraLog: Date;
    usuarioLog: string;
    ano: number;
    mes: number;
    idEmpresa: number;
}