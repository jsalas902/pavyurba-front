export class NominasValidator {
    public static NOMINAS_VALIDATION_MESSAGES = {
        'archivo': [
            { message: 'Archivo es requerido' },
        ],        
        'idEmpresa': [
            { type: 'required', message: 'Empresa es requerido' },
        ],        
        'mes': [
            { type: 'required', message: 'Mes es requerido' },
        ],
        'ano': [
            { type: 'required', message: 'Año es requerido' },
        ],
        'dni': [
            { type: 'required', message: 'DNI es requerido' },
        ],
        'totalBruto': [
            { type: 'required', message: 'Sueldo es requerido' },
        ],
        'embargo': [
            { type: 'required', message: 'Embargo es requerido' },
        ],
        'irpf': [
            { type: 'required', message: 'IRPF es requerido' },
        ],
        'total': [
            { type: 'required', message: 'Total es requerido' },
        ],
        'totalSeguridadSocial': [
            { type: 'required', message: 'Seguridad Social es requerido' },
        ],


        
    }
}