import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Contrato } from '@app/contratos/models/Contrato';
import * as moment from 'moment';

import { Router } from '@angular/router';
import { Pagination } from '@app/_models';
import { ContratosService } from '@app/contratos/services/contratos.service';

@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.css']
})
export class ContratosComponent implements OnInit {

  @BlockUI('componentsContratos') blockUIList: NgBlockUI;

  @Input() idObras: string;  


  
  rows: Contrato[];
  rowsFiltered: Contrato[];
  
  pagination: Pagination;
  listaContratos: Contrato[];
  cargaCompleta = false;
  titelModal: string;
  dato: number;
  type: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  private modalRef;

  constructor(private toastr: ToastrService,
              private contratosService: ContratosService,
              private confirmDialogService: ConfirmDialogService,
              private router: Router,
              private cdRef: ChangeDetectorRef
              
              ) {
}


  ngOnInit(): void {

    this.titelModal = 'Subida de Contrato';

    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = '';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;

    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    this.getAllData();
  }

  ngAfterViewInit(): void {
    // this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();
    this.contratosService.ListarPorObras(this.idObras, this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
          element.valor = (element.valor != null) ? Number(element.valor).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.valor;      
        });
        this.listaContratos = data.data;
        this.listaContratos = [...this.listaContratos];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaContratos;
    this.rowsFiltered = this.listaContratos;
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any) {
    this.idObras = this.idObras;
    this.cargaCompleta = true;
    this.contratosService.addModelContrato(modal);
  }
  onSetModal(e) {
    this.getAllData();
  }
  downloadFile(row) {
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = row.contrato.url;
    a.target = "_blank";
    a.download = "TEST";
    a.click();
    a.remove();
  }

  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.contratosService.ListarPorObras(this.idObras, this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
          element.valor = (element.valor != null) ? Number(element.valor).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.valor;      
        });
        this.listaContratos = data.data;
        this.listaContratos = [...this.listaContratos];

        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }
}
