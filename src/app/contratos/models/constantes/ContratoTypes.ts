export class ContratoTypes {

    public constructor(init?: Partial<ContratoTypes>) {
      Object.assign(this, init);
      ContratoTypes.Expansion = 'EXPANSION';
      ContratoTypes.Inicial = 'INICIAL';
    }
  
    static Expansion = 'EXPANSION';
    static Inicial = 'INICIAL';
  }
  