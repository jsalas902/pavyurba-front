export class Contrato {

    public constructor(init?: Partial<Contrato>) {
        Object.assign(this, init);
    }

    idObra: string;
    contrato: any;
    tipo: string;
    valor: number;
}