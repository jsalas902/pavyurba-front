export class ContratosValidator {
    public static CONTRATOS_VALIDATION_MESSAGES = {        
        'tipo': [
            { type: 'required', message: 'Tipo es requerido' },
        ],
        'valor': [
            { type: 'required', message: 'Importe es requerido' },
        ],
        'contrato': [
            { type: 'required', message: 'El contrato es requerido' },
        ],
    }
}