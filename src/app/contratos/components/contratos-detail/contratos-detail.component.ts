import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Contrato } from '@app/contratos/models/Contrato';

import { ContratosService } from '@app/contratos/services/contratos.service';
import { ContratosValidator } from "@app/contratos/validation/contratos.validator";
import { ContratoTypes } from '@app/contratos/models/constantes/ContratoTypes';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-contratos-detail',
  templateUrl: './contratos-detail.component.html',
  styleUrls: ['./contratos-detail.component.css']
})
export class ContratosDetailComponent implements OnInit {

  @Input() idObras: string;
  @Output() closeModal = new EventEmitter<boolean>();
  @BlockUI('componentsContratosDetail') blockUIList: NgBlockUI;

  contrato: Contrato = new Contrato();
  contratosForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  contratoTypes: any;
  titleDocumento: string;
  sizeArchivo: number;
  typeArchivo: string;
  baseArchivo: string | ArrayBuffer;
  uploadFile: boolean = false;
  archivoValid: boolean = false;
  contratos_validation_messages = ContratosValidator.CONTRATOS_VALIDATION_MESSAGES;
  fileToUpload: File = null;
  
  constructor(
    private toastr: ToastrService, 
    private contratosService: ContratosService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.titleDocumento = "Subir Documento";
    this.contratosForm = this.formBuilder.group({
      tipo: [null, [Validators.required]],
      valor: [null, [Validators.required]],
      contrato: [null],
    });

    this.contratoTypes = [{"texto":ContratoTypes.Expansion}, {"texto":ContratoTypes.Inicial}];
  }

  get f() { return this.contratosForm.controls; }
  add() {
    this.submitted = true;

    if (this.contratosForm.invalid) {
      return;
    } else {
      if(this.uploadFile) {
        this.submitted = false;
        this.uploadFile = false;
        return;
      }
    }
    
    this.contrato = this.contratosForm.value;
    if((this.contrato.contrato == null)){
      this.archivoValid = true;
      return
    } else {
      this.archivoValid = false
    }
    this.reloadStart();
    this.create(); 
  }
  create(){
    let contratosTem = new Contrato();
    contratosTem = this.changeObject(this.contrato);
   
    this.contratosService.Crear(contratosTem).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
        this.reloadStop();
        this.closeModal.emit(false);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        this.reloadStop();
    });
  }

  reiniciarFormulario() {
    this.contrato = new Contrato();
    this.contratosForm.reset();
    this.submitted = false;
  }
  changeObject(contrato: Contrato) {
    let contratosTem: Contrato = new Contrato();
    contratosTem.idObra = this.idObras;
    contratosTem.tipo = contrato.tipo;
    contratosTem.valor = contrato.valor;

    if(contrato.contrato != null && contrato.contrato != undefined){
      contratosTem.contrato = {
        "base64": this.baseArchivo,
        "nombre": this.titleDocumento,
        "mimeType": this.typeArchivo,
        "tamano": this.sizeArchivo  
      };
    } else {
      contratosTem.contrato = null;
      // albaranesTem.archivo = {
      //   "base64": "",
      //   "nombre": "",
      //   "mimeType": "",
      //   "tamano": 0  
      // };
    }

    return contratosTem;
  }
  onFileChange(event: any) {
    console.log(event);
    this.fileToUpload  = event.target.files.item(0);
    var reader = new FileReader();
    reader.onload = this.convertFileSimple.bind(this, this.fileToUpload);
    reader.readAsBinaryString(this.fileToUpload);
  }

  convertFileSimple(file: File, event: any) {
    const binaryString = event.target.result;
    this.titleDocumento = file.name;
    this.sizeArchivo = file.size;
    this.typeArchivo = file.type;
    this.baseArchivo = btoa(binaryString);
    this.archivoValid = false
    this.uploadFile = false;
  }

  document() {
    this.uploadFile = true;
    let element: HTMLElement = document.getElementById('contrato') as HTMLElement;
    element.click();
  }
  
  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }
}
