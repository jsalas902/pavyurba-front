import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ContratosService } from '@app/contratos/services/contratos.service';

@Component({
  selector: 'app-modal-contratos-detail',
  templateUrl: './modal-contratos-detail.component.html',
  styleUrls: ['./modal-contratos-detail.component.css']
})
export class ModalContratosDetailComponent implements OnInit {
  @Input() idObras: string;
  @Input() titel: string;
  @Output() setModal = new EventEmitter<boolean>();
  cargaCompletaModal:boolean;

  constructor(private modalService: NgbModal,private contratosService: ContratosService) { }

  ngOnInit(): void {
    this.cargaCompletaModal = false;
  }
  
  ngAfterViewInit(): void {
    this.cargaCompletaModal = true;
  }

  onCloseModal(e) {
    if (!e) this.setModal.emit(true);
        
    this.contratosService.onCloseModal();
  }
}
