import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxDatatableFooterModule } from 'ngx-datatable-footer';
import { NgSelectModule } from '@ng-select/ng-select';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';
import { CardModule } from '../content/partials/general/card/card.module';
import { BreadcrumbModule } from '../_layout/breadcrumb/breadcrumb.module';
import { BlockUIModule } from 'ng-block-ui';
import { BlockTemplateComponent } from '../_layout/blockui/block-template.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared/shared.module';

import { EmployeesComponent } from './pages/employees.component';
import { EmployeesDetailComponent } from './components/employees-detail/employees-detail.component';
import { ConditionsEmployeesComponent } from '../conditions/components/conditions-employees/conditions-employees.component';
import { HuchasModule } from '../huchas/huchas.module';
import { InformesModule } from '../informes/informes.module';

@NgModule({
  imports: [
    CommonModule,
    CardModule,
    NgbModule,
    BreadcrumbModule,
    NgSelectModule,
    UiSwitchModule,
    FormsModule,
    ReactiveFormsModule,
    ClipboardModule,
    PerfectScrollbarModule,
    NgxDatatableModule,
    BlockUIModule.forRoot({
      template: BlockTemplateComponent
    }),
    NgxDatatableFooterModule.forRoot(),
    SharedModule,
    HuchasModule,
    InformesModule,
    RouterModule.forChild([
      {
        path: 'list/:type',
        component: EmployeesComponent
      },
    ]),
  ],  
  declarations: [EmployeesComponent, EmployeesDetailComponent, ConditionsEmployeesComponent],
})
export class EmployeesModule { }
