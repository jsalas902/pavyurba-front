import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { NgbDateStruct, NgbTimeStruct, NgbDate, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { Employee } from '@app/employees/models/Employee';
import { Location } from '@app/locations/models/Location';
import { Business } from '@app/business/models/Business';
import { Condition } from '@app/conditions/models/condition';

import { LocationsService } from '@app/locations/services/locations.service';
import { EmployeesService } from '@app/employees/services/employees.service';
import { BusinessService } from '@app/business/services/business.service';
import { ConditionsService } from '@app/conditions/services/conditions.service';
import { EmployeesValidator } from "@app/employees/validation/employees.validator";
import { CivilStates } from '@app/_models/constantes/CivilStates';
import { AutonomousClasses } from '@app/_models/constantes/AutonomousClasses';
import { EmployeeTypes } from '@app/employees/models/constantes/EmployeeTypes';

import { NgbDateCustomParserFormatter} from '@app/_helpers/dateformat';
import { CustomDatepickerI18nService, I18n } from '@app/calendarios/services/custom-datepicker-i18n.service';


import {Injectable} from '@angular/core';
import {NgbCalendar, NgbDateAdapter, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import { async } from '@angular/core/testing';


const now = new Date();
@Component({
  selector: 'app-employees-detail',
  templateUrl: './employees-detail.component.html',
  styleUrls: ['./employees-detail.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18nService }
   ]
})
export class EmployeesDetailComponent implements OnInit {
  
  @BlockUI('componentsDetailEmployees') blockUIList: NgBlockUI;
  @Input() idEmployees: string;
  @Input() dietasFueraMadrid: string;
  @Input() dietasDentroMadrid: string;
  @Output() closeModal = new EventEmitter<boolean>();

  employees: Employee = new Employee();
  locations: Location;
  idProvincia: string = null;
  idLocalidad: string = null;
  provincias: any;
  business: Business;
  employeesForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  sumarCondicion: boolean = false;
  titleAccion: string = "";
  iconAccion: string = "";
  employees_validation_messages = EmployeesValidator.EMPLOYEES_VALIDATION_MESSAGES;
  civilStates: any;
  autonomousClasses: any;
  employeeTypes: any;
  fechaAlta;
  fechaBaja;
  fechaConversion;
  d: any;
  d2: any;
  d3: any;
  readonly DELIMITER = '-';
  // boolean tipo de trabajador
  isTipoTrabajador: boolean = false;
  isAlmacenaHucha: boolean = false;

  public repeatList: FormArray;
  conditions: any;
  // Modal location
  cargaCompleta = false;
  
  mensaje: string = '';
  mensajeDir: string = '';
  maxLength: number = 500;

  get condiciones() {
    return this.employeesForm.get('condiciones') as FormArray;
  }

  constructor(
    private toastr: ToastrService, 
    private locationsService: LocationsService,
    private employeesService: EmployeesService,
    private businessService: BusinessService,
    private conditionsService: ConditionsService,
    private formBuilder: FormBuilder,
  ) {}

    ngOnInit(): void {
    this.reloadStart();
    this.obtenerConditions();
    this.employeesForm = this.formBuilder.group({
      id: [null],
      identificacion: [null, [Validators.required]],
      nombre: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      nacionalidad: [null],
      estadoCivil: [null, [Validators.required]],
      numeroSeguroSocial: [null],
      tipo: [null, [Validators.required]],
      fijo: ["true"],
      fechaAlta: [null, [Validators.required]],
      fechaBaja: [null],
      fechaConversion: [null],
      claseAutonomo: [null],
      email: [null],
      telefono: [null],
      emailAdicional: [null],
      telefonoAdicional: [null],
      repercusionGastosGenerales: [false],
      almacenaHucha: [false],
      hucha: [null],
      huchaBase: [null],
      direccion: [null],
      codigoPostal: [null],
      observaciones: [null, [Validators.maxLength(500)]],
      dietaDentroMadrid: [this.dietasDentroMadrid, [Validators.required]],
      dietaFueraMadrid: [this.dietasFueraMadrid, [Validators.required]],
      idProvincia: [null, [Validators.required]],
      idLocalidad: [null, [Validators.required]],
      idEmpresa: [null, [Validators.required]],
      condiciones: this.formBuilder.array([this.createCondicion()]),
    });
    this.repeatList = this.employeesForm.get('condiciones') as FormArray;
    if (this.idEmployees != null) {
      this.edicion = true;
      this.obtenerEmployees();
      // this.setearConditions();
    }else {
      this.reloadStop();
    }
    // this.obtenerLocations();
    this.obtenerProvincias();
    this.obtenerBusiness();
    
    
    this.civilStates = [{"texto":CivilStates.Soltero}, {"texto":CivilStates.Casado}, {"texto":CivilStates.Divorciado}, {"texto":CivilStates.Viudo}, {"texto":CivilStates.UnionLibre}];
    this.autonomousClasses = [{"texto":AutonomousClasses.Empresa}, {"texto":AutonomousClasses.Otros}];
    this.employeeTypes = [{"texto":EmployeeTypes.AseguradoJornal}, {"texto":EmployeeTypes.AseguradoMetro}, {"texto":EmployeeTypes.Autonomo}];
   
  }
  createCondicion(): FormGroup {
      return this.formBuilder.group({
        id: [this.idEmployees],
        idCondicion: [null],
        valorCondicionDM: [null],
        valorCondicionFM: [null],
      });
  }
  addRepeat() {
    this.sumarCondicion = true;
    this.repeatList = this.employeesForm.get('condiciones') as FormArray;
    this.repeatList.push(this.createCondicion());
  }
  removeRepeat(index) {
    this.repeatList.removeAt(index);
  }
  // setearConditions() {
  //   this.employees.condiciones.forEach(element => {
  //     this.addRepeat()
  //   });
  // }
  obtenerEmployees() {
    this.employeesService.ObtenerPorId(this.idEmployees).subscribe(
      data => {
        this.obtenerPatchEmployee(data);
        this.reloadStop();
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  async obtenerBusiness() {
    // this.business = await this.businessService.ListarEmpresasCombo().toPromise();
    this.businessService.ListarEmpresasCombo().subscribe(
      data => {
        this.business = data;
      },
      error => {
        this.toastr.error(error);
      }
    );
  }

  obtenerProvincias() {
    this.locationsService.ListarProvinciasCombo().subscribe(
      data => {
        this.provincias = data;
      },
      error => {
        // this.toastr.error(error);
        this.toastr.error(error.mensaje);
      }
    );
  }

  async obtenerLocations() {
    //  this.locations = await this.locationsService.ListarLocalidadesCombo().toPromise();
    this.locationsService.ListarLocalidadesCombo(this.idProvincia).subscribe(
      data => {
        this.locations = data;
      },
      error => {
        this.toastr.error(error);
      }
    );
  }
  async obtenerConditions() {
    // this.conditions = await this.conditionsService.ListarTodos().toPromise();
    this.conditionsService.ListarCondicionesCombo().subscribe(
      data => {
        this.conditions =data;
      },
      error => {
        this.toastr.error(error);
      }
    );
  } 

  get f() { return this.employeesForm.controls; }
  add() {
    this.submitted = true;
    if (this.employeesForm.invalid) {
        return;
    } else {
      if(this.sumarCondicion) {
        this.submitted = false;
        this.sumarCondicion = false;
        return;
      }
    }

    this.employees = this.employeesForm.value;
    
    this.reloadStart();
    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }

  }
  create(){
    let employeesTem = new Employee();
    for (let index = 0; index < this.employees.condiciones.length; index++) {
      const element = this.employees.condiciones[index];
      if(element.idCondicion === null) {
        this.employees.condiciones.splice(index, 1);
        --index;
      }
      
    }

    this.employees.fechaAlta = (this.fechaAlta == undefined || this.fechaAlta == "" || this.fechaAlta == null) ? null : this.toModel(this.fechaAlta);
    this.employees.fechaBaja = (this.fechaBaja == undefined || this.fechaBaja == "" || this.fechaBaja == null) ? null : this.toModel(this.fechaBaja);
    this.employees.fechaConversion = (this.fechaConversion == undefined || this.fechaConversion == "" || this.fechaConversion == null) ? null : this.toModel(this.fechaConversion);
    employeesTem = this.changeObject(this.employees, true);
    employeesTem.fechaAlta = (this.fechaAlta == undefined || this.fechaAlta == "" || this.fechaAlta == null) ? null : this.toModel(this.fechaAlta);
    employeesTem.fechaBaja = (this.fechaBaja == undefined || this.fechaBaja == "" || this.fechaBaja == null) ? null : this.toModel(this.fechaBaja);
    employeesTem.fechaConversion = (this.fechaConversion == undefined || this.fechaConversion == "" || this.fechaConversion == null) ? null : this.toModel(this.fechaConversion);
    
    employeesTem.condiciones = null;

    // console.log(employeesTem);
    // this.employees.fechaAlta = "";
    // this.employees.fechaAlta = (this.fechaAlta != undefined && this.fechaAlta != "" && this.fechaAlta != null) ? this.toModelFix(this.fechaAlta) : "";
    // this.employees.fechaBaja = "";
    // this.employees.fechaBaja = (this.fechaBaja != undefined && this.fechaBaja != "" && this.fechaBaja != null) ? this.toModelFix(this.fechaBaja) : "";
    // this.employees.fechaConversion = "";
    // this.employees.fechaConversion = (this.fechaConversion != undefined && this.fechaConversion != "" && this.fechaConversion != null) ? this.toModelFix(this.fechaConversion) : "";

    this.employeesService.Crear(employeesTem).subscribe(
      data => {
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        this.reloadStop();
        // this.getAllData();
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        this.reloadStop();
    });
  }

  edit(){
    let employeesTem = new Employee();
    // console.log(this.employees.condiciones);

    // for (let index = 0; index < this.employees.condiciones.length; index++) {
    //   const element = this.employees.condiciones[index];
    //   if(element.idCondicion === null) {
    //     this.employees.condiciones.splice(index, 1);
    //     --index;
    //   }
      
    // }
    this.employees.fechaAlta = (this.fechaAlta == undefined || this.fechaAlta == "" || this.fechaAlta == null) ? null : this.toModel(this.fechaAlta);
    this.employees.fechaBaja = (this.fechaBaja == undefined || this.fechaBaja == "" || this.fechaBaja == null) ? null : this.toModel(this.fechaBaja);
    this.employees.fechaConversion = (this.fechaConversion == undefined || this.fechaConversion == "" || this.fechaConversion == null) ? null : this.toModel(this.fechaConversion);
    employeesTem = this.changeObject(this.employees, true);
    employeesTem.fechaAlta = (this.fechaAlta == undefined || this.fechaAlta == "" || this.fechaAlta == null) ? null : this.toModel(this.fechaAlta);
    employeesTem.fechaBaja = (this.fechaBaja == undefined || this.fechaBaja == "" || this.fechaBaja == null) ? null : this.toModel(this.fechaBaja);
    employeesTem.fechaConversion = (this.fechaConversion == undefined || this.fechaConversion == "" || this.fechaConversion == null) ? null : this.toModel(this.fechaConversion);
    
    if(employeesTem.condiciones.length > 0) {
      employeesTem.condiciones.pop();
    }
    // this.employees.fechaAlta = "";
    // this.employees.fechaAlta = (this.fechaAlta != undefined && this.fechaAlta != "" && this.fechaAlta != null) ? this.toModelFix(this.fechaAlta) : "";
    // this.employees.fechaBaja = "";
    // this.employees.fechaBaja = (this.fechaBaja != undefined && this.fechaBaja != "" && this.fechaBaja != null) ? this.toModelFix(this.fechaBaja) : "";
    // this.employees.fechaConversion = "";
    // this.employees.fechaConversion = (this.fechaConversion != undefined && this.fechaConversion != "" && this.fechaConversion != null) ? this.toModelFix(this.fechaConversion) : "";

    // employeesTem.fijo = (employeesTem.fijo == "true") ? true : false;
    // this.isAlmacenaHucha = (employeesTem.almacenaHucha) ? this.isAlmacenaHucha : !this.isAlmacenaHucha;
    this.employeesService.Editar(employeesTem).subscribe(
      data => {
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
        this.reloadStop();
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        this.reloadStop();
    });
  }

  reiniciarFormulario() {
    this.employees = new Employee();
    this.employeesForm.reset();
    this.submitted = false;
  }
  obtenerPatchEmployee(data){
    this.employees = this.changeObject(data, false);
    
    this.employeesForm.patchValue({
      id: this.employees.id,
      identificacion: this.employees.identificacion,
      nombre: this.employees.nombre,
      nacionalidad: this.employees.nacionalidad,
      estadoCivil: this.employees.estadoCivil,
      numeroSeguroSocial: this.employees.numeroSeguroSocial,
      tipo: this.employees.tipo,
      fijo: this.employees.fijo,
      // fechaAlta: this.fechaAlta,
      // fechaBaja: this.fechaBaja,
      // fechaConversion: this.fechaConversion,
      claseAutonomo: this.employees.claseAutonomo,
      email: this.employees.email,
      emailAdicional: this.employees.emailAdicional,
      telefono: this.employees.telefono,
      telefonoAdicional: this.employees.telefonoAdicional,
      observaciones: this.employees.observaciones,
      direccion: this.employees.direccion,
      codigoPostal: this.employees.codigoPostal,
      dietaDentroMadrid: this.employees.dietaDentroMadrid,
      dietaFueraMadrid: this.employees.dietaFueraMadrid,
      repercusionGastosGenerales: this.employees.repercusionGastosGenerales,
      almacenaHucha: this.employees.almacenaHucha,
      hucha: this.employees.hucha,
      huchaBase: this.employees.huchaBase,
      idLocalidad: this.employees.idLocalidad,
      idProvincia: this.employees.idProvincia,
      idEmpresa: this.employees.idEmpresa,
      condiciones: this.employees.condiciones,
    });
  }
  toModel(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format();
  }
  toModelFix(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format('DD-MM-YYYY');
  }
  changeObject(employees: Employee, valid: boolean) {
    // console.log(employees);
    let employeesTem: Employee = new Employee();
    employeesTem.id = employees.id;
    employeesTem.identificacion = employees.identificacion;
    employeesTem.nombre = employees.nombre;
    employeesTem.nacionalidad = employees.nacionalidad;
    employeesTem.estadoCivil = employees.estadoCivil;
    employeesTem.numeroSeguroSocial = employees.numeroSeguroSocial;
    employeesTem.tipo = employees.tipo;

    if(valid) {
      employeesTem.fijo = (employees.fijo == "true") ? 'true' : 'false';
    } else {
      employeesTem.fijo = (employees.fijo) ? 'true' : 'false';
    }
    
    employeesTem.fechaAlta = this.fechaAlta = (employees.fechaAlta !== null) ? this.fromModel(employees.fechaAlta) : null;
    employeesTem.fechaBaja = this.fechaBaja = (employees.fechaBaja !== null) ? this.fromModel(employees.fechaBaja) : null;
    employeesTem.fechaConversion = this.fechaConversion = (employees.fechaConversion !== null) ? this.fromModel(employees.fechaConversion) : null;
    employeesTem.claseAutonomo = employees.claseAutonomo;
    employeesTem.email = (employees.email == "") ? null : employees.email;
    employeesTem.emailAdicional = (employees.emailAdicional == "") ? null : employees.emailAdicional;
    employeesTem.telefono = (employees.telefono == "" || employees.telefono == null) ? null : employees.telefono;
    employeesTem.telefonoAdicional = (employees.telefonoAdicional == "") ? null : employees.telefonoAdicional;
    employeesTem.observaciones = employees.observaciones;
    employeesTem.direccion = employees.direccion;
    employeesTem.codigoPostal = employees.codigoPostal;
    employeesTem.dietaDentroMadrid = employees.dietaDentroMadrid;
    employeesTem.dietaFueraMadrid = employees.dietaFueraMadrid;
    employeesTem.repercusionGastosGenerales = employees.repercusionGastosGenerales;
    employeesTem.almacenaHucha = employees.almacenaHucha;
    employeesTem.hucha = (employees.hucha == null) ? 0 : employees.hucha;
    employeesTem.huchaBase = (employees.huchaBase == null) ? 0 : employees.huchaBase;
    employeesTem.idLocalidad = employees.idLocalidad;
    employeesTem.idProvincia = this.idProvincia = employees.idProvincia;
    employeesTem.idEmpresa = employees.idEmpresa;
    employeesTem.condiciones = employees.condiciones;

    // this.fechaAlta=employeesTem.fechaAlta;
    // this.fechaBaja=employeesTem.fechaBaja;
    // this.fechaConversion=employeesTem.fechaConversion;
    if(employeesTem.almacenaHucha) {
      if(!this.isAlmacenaHucha)
        this.changeAlmacenaHucha(true);
      // this.isAlmacenaHucha = (employeesTem.almacenaHucha) ? this.isAlmacenaHucha : !this.isAlmacenaHucha;
    }

    if(employeesTem.fijo == "false") {
      if(!this.isTipoTrabajador)
        this.changeTrabajadorFijo(true);
    }

    this.titleAccion = (employeesTem.fechaBaja == null) ? "Dar de baja" : "Dar de Alta";
    this.iconAccion = (employeesTem.fechaBaja == null) ? "pavyurba-icon-dar-de-baja" : "pavyurba-icon-dar-de-alta";

    console.log(employeesTem.condiciones);

    employeesTem.condiciones.forEach((element: string, index: number) => {
      // if((index + 1) !== employeesTem.condiciones.length)
        this.addRepeatData(element)
    });
    this.changeProvincia();
    return employeesTem;
  }
  // selectToday() {
  //   this.popupModel = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
  // }

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[2], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[0], 10)
      };
    }
    return null;
  }
  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }
  addLocation(modal){
    this.cargaCompleta = true;
    this.locationsService.addModelLocation(modal);
  }
  onSetModal(e) {
    this.obtenerLocations();
  }
  changeTrabajadorFijo(e) {
    this.isTipoTrabajador = !this.isTipoTrabajador;
  }
  changeAlmacenaHucha(e) {
    this.isAlmacenaHucha = !this.isAlmacenaHucha;
  }
  darBajaAlta() {
    this.employees.fechaBaja 

    if(this.employees.fechaBaja == null) {
      this.employeesService.BajaEmpleado(this.idEmployees).subscribe(
        data => {
          this.closeModal.emit(true);
          this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
          // this.getAllData();
          this.submitted = false;
        },
        error => {
          // this.toastr.error(error.mensaje);
          this.toastr.error(error);
        }
      );
    } else {
      this.employeesService.AltaEmpleado(this.idEmployees).subscribe(
        data => {
          this.closeModal.emit(true);
          this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
          // this.getAllData();
          this.submitted = false;
        },
        error => {
          // this.toastr.error(error.mensaje);
          this.toastr.error(error);
        }
      );
    }


    
  }
  changeProvincia() {
    if(this.idProvincia != null) {
      this.obtenerLocations();
    } else {
      this.idLocalidad = null;
    }
  }
  createRepeatData(data): FormGroup {
    return this.formBuilder.group({
      id: [data.id],
      idCondicion: [data.idCondicion, Validators.required],
      valorCondicionDM: [data.valorCondicionDM, Validators.required],
      valorCondicionFM: [data.valorCondicionFM, Validators.required],
      // idsEmpleados: [[this.idEmployees]]
    });    
  }
  addRepeatData(data) {
    this.repeatList = this.employeesForm.get('condiciones') as FormArray;
    this.repeatList.push(this.createRepeatData(data));
  }
}
