export class EmployeesValidator {
    public static EMPLOYEES_VALIDATION_MESSAGES = {
        'nombre': [
            { type: 'required', message: 'Nombre es requerido' },
            { type: 'minlength', message: 'El Nombre no puede tener menos de 3 caracteres' },
            { type: 'maxlength', message: 'El nombre no puede tener más de 100 caracteres' }
        ],
        'identificacion': [
            { type: 'required', message: 'Identificacion es requerido' },
        ],
        'idProvincia': [
            { type: 'required', message: 'Provincia es requerido' },
        ],
        'idLocalidad': [
            { type: 'required', message: 'Localidad es requerido' },
        ],
        'idEmpresa': [
            { type: 'required', message: 'Empresa es requerido' },
        ],
        'idCondicion': [
            { type: 'required', message: 'Condición es requerido' },
        ],
        'direccion': [
            { type: 'required', message: 'Dirección es requerido' },
            { type: 'minlength', message: 'La dirección no puede tener menos de 5 caracteres' },
            { type: 'maxlength', message: 'La dirección no puede tener más de 500 caracteres' }
        ],
        'fijo': [
            { type: 'required', message: 'Fijo es requerido' },
        ],
        'telefono': [
            { type: 'required', message: 'Teléfono es requerido' },
        ],
        'email': [
            { type: 'required', message: 'E-mail es requerido' },
        ],
        'dietaDentroMadrid': [
            { type: 'required', message: 'Dieta en Madrid es requerido' },
        ],
        'dietaFueraMadrid': [
            { type: 'required', message: 'Dieta Fuera Madrid es requerido' },
        ],
        'claseAutonomo': [
            { type: 'required', message: 'Clase Autonomo es requerido' },
        ],
        'estadoCivil': [
            { type: 'required', message: 'Estado Civil es requerido' },
        ],
        'tipo': [
            { type: 'required', message: 'Tipo empleado es requerido' },
        ],
        'numeroSeguroSocial': [
            { type: 'required', message: 'Número de Seguro Social es requerido' },
        ],
        'nacionalidad': [
            { type: 'required', message: 'Nacionalidad es requerido' },
        ],
        'observaciones': [
            { type: 'maxlength', message: 'Las observaciones no pueden tener más de 500 caracteres' }
        ],
        'fechaAlta': [
            { type: 'required', message: 'Fecha Alta es requerido' },
        ],
    }
}