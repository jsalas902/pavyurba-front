export class EmployeeTypes {

    public constructor(init?: Partial<EmployeeTypes>) {
      Object.assign(this, init);
      EmployeeTypes.Asegurado = 'ASEGURADO';
      EmployeeTypes.AseguradoMetro = 'ASEGURADO METRO';
      EmployeeTypes.AseguradoJornal = 'ASEGURADO JORNAL';
      EmployeeTypes.Autonomo = 'AUTONOMO';
    }
  
    static Asegurado = 'ASEGURADO';
    static AseguradoMetro = 'ASEGURADO METRO';
    static AseguradoJornal = 'ASEGURADO JORNAL';
    static Autonomo = 'AUTONOMO';
  }
  