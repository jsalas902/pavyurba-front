import { NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
export class Employee {

    public constructor(init?: Partial<Employee>) {
        Object.assign(this, init);
    }

    id: number;
    identificacion: string;
    nombre: string;
    nacionalidad: string;
    estadoCivil: string;
    numeroSeguroSocial: number;
    tipo: string;
    fijo: string;
    almacenaHucha: boolean;
    repercusionGastosGenerales: boolean;
    fechaAlta: any;
    fechaBaja: any;
    // fechaAltaAutonomo: any;
    fechaConversion: any;
    claseAutonomo: string;
    email: string;
    emailAdicional: string;
    telefono: string;
    telefonoAdicional: string;
    direccion: string;
    codigoPostal: string;
    observaciones: string;
    dietaDentroMadrid: number;
    dietaFueraMadrid: number;
    hucha: number;
    huchaBase: number;
    idLocalidad: number;
    idProvincia: string;
    localidad: string;
    idEmpresa: number;
    empresa: string;
    condiciones: any;
    habilitado: boolean;
    fechaHoraLog: Date;
    usuarioLog: string;
    selected: boolean;

}