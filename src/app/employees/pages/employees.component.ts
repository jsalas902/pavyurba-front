import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { EmployeesService } from '@app/employees/services/employees.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Employee } from '@app/employees/models/Employee';
import { Dieta } from '@app/dietas/models/Dieta';
import { DietasService } from '@app/dietas/services/dietas.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { Pagination } from '@app/_models';
import * as moment from 'moment';
import { Head } from 'src/app/_models/shares/Head'

import { InformesTypes } from '@app/informes/models/constantes/InformesTypes'
import { BusinessService } from '@app/business/services/business.service';
import { Business } from '@app/business/models/Business';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  @BlockUI('componentsEmployees') blockUIList: NgBlockUI;

  @Input() idEmployeesListar: string;

  rows: Employee[];
  rowsFiltered: Employee[];
  business: Business;
  isSelected: boolean;
  pagination: Pagination;
  listaEmployees: Employee[];
  cargaCompleta = false;
  titelModal: string;
  query: string;
  skip: string;
  take: string;
  tipoGeneracion: string = InformesTypes.EMPLEADO;
  skipAux;
  takeAux;
  private modalRef;
  public menuSettingsConfig: any;
  public switch_expression: string;
  size: string;
  edicion: boolean = false;
  type: string;
  dietas: Dieta;  
  idEmpresa: number;  
  dietasDentroMadrid: string;
  dietasFueraMadrid: string;

  // Head Historial de cambio
  rowsHeadHistory: Head[] = [
    { name:'N°', property: 'id', visible: true, restrict: false },
    { name:'Campo', property: 'campo', visible: true, restrict: false} , 
    { name:'Campo Detalle', property: 'campoDetalle', visible: true, restrict: true },
    { name:'Valor', property: 'valor', visible: true, restrict: false },
    { name:'Valor Anterior', property: 'valorAnterior', visible: true, restrict: false },
    { name:'Fecha', property: 'fechaHoraLog', visible: true, restrict: false }
  ];
  // Head Incidencia
  rowsHeadIncidencias: Head[] = [
    { name:'N°', property: 'id', visible: true, restrict: false },
    { name:'Tipo', property: 'tipo', visible: true, restrict: false} , 
    { name:'Incidencia', property: 'incidencia', visible: true, restrict: true },
    { name:'Fecha Desde', property: 'fechaDesde', visible: true, restrict: false },
    { name:'Fecha Hasta', property: 'fechaHasta', visible: true, restrict: false },
    { name:'N° de Dias', property: 'numDias', visible: true, restrict: false },
    { name:'Observaciones', property: 'observaciones', visible: true, restrict: true },
    { name:'Empleado', property: 'empleado', visible: false, restrict: false },
  ];
  // Head Calendario
  rowsHeadCalendario: Head[] = [
    { name:'N°', property: 'id', visible: true, restrict: false },
    { name:'Empleado', property: 'empleado', visible: false, restrict: false },
    { name:'Obra', property: 'obra', visible: true, restrict: true }, 
    { name:'Fecha Desde', property: 'fechaDesde', visible: true, restrict: false },
    { name:'Fecha Hasta', property: 'fechaHasta', visible: true, restrict: false },
    { name:'N° de Dias', property: 'numDias', visible: true, restrict: false },
    { name:'Observaciones', property: 'observaciones', visible: true, restrict: true },
  ];

  constructor(private toastr: ToastrService,
              private employeesService: EmployeesService,
              private businessService: BusinessService,
              private confirmDialogService: ConfirmDialogService,
              private dietasService: DietasService,
              private rutaActiva: ActivatedRoute,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private modalService: NgbModal) 
  {
    this.menuSettingsConfig = {
      vertical_menu: {
        items: [          
          {
            title: 'Datos',
            icon: 'pavyurba-icon-data',
            page: 'datos',
            isSelected: true
          },
          // {
          //   title: 'Calendario',
          //   icon: 'pavyurba-icon-calendar',
          //   page: 'calendario'
          // },
          // {
          //   title: 'Incidencias',
          //   icon: 'pavyurba-icon-incidents',
          //   page: 'incidencias'
          // },
          {
            title: 'Condiciones',
            icon: 'pavyurba-icon-terms',
            page: 'condiciones'
          },
          {
            title: 'Hucha',
            icon: 'pavyurba-icon-paysheet',
            page: 'hucha'
          },
          {
            title: 'Informes',
            icon: 'pavyurba-icon-informes',
            page: 'informes'
          },
          // {
          //   title: 'Nómina',
          //   icon: 'pavyurba-icon-paysheet',
          //   page: 'nomina'
          // },
          // {
          //   title: 'Hist. Cambios',
          //   icon: 'pavyurba-icon-paysheet',
          //   page: 'hist-cambios'
          // },
        ]
      }    
    };
    this.rutaActiva.paramMap.subscribe(params => {
      if (params.get('type')) {
        this.type = params.get('type').toUpperCase();
      }
    });

    this.obtenerBusiness();
  }


  ngOnInit(): void {
    this.isSelected = false;

    this.titelModal = 'Crear Empleado';
    this.switch_expression = 'datos';

    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = ' ';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;
    
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    // this.getAllData();    
    this.obtenerDietas();

  }

  ngAfterViewInit(): void {
    this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }
  obtenerDietas() {
    this.dietasService.ListarTodos().subscribe(
      data => {
        this.dietas = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  } 
  getAllData() {
    var bodyEmployees = {
      idEmpresa: this.idEmpresa
    };

    this.reloadStart();
    this.employeesService.ListarTodos(this.skip, this.take, this.query, this.type, bodyEmployees).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaAlta = (element.fechaAlta != null) ? moment(element.fechaAlta).format('DD-MM-YYYY') : null;
          element.fechaBaja = (element.fechaBaja != null) ? moment(element.fechaBaja).format('DD-MM-YYYY') : null;
        });
        this.listaEmployees = list;
        this.listaEmployees = [...this.listaEmployees];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaEmployees;
    this.rowsFiltered = this.listaEmployees;
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, row: Employee) {
    this.dietasDentroMadrid = this.dietas ? (this.dietas[1].fueraMadrid ? this.dietas[0].valor : this.dietas[1].valor) : null;
    this.dietasFueraMadrid = this.dietas ? (this.dietas[0].fueraMadrid ? this.dietas[0].valor : this.dietas[1].valor) : null;
    this.size = 'lg';
    if(row !== null){
      this.idEmployeesListar =  row.id.toString();
      this.edicion = true;
      this.titelModal = 'Editar Empleado';
    }else{
      this.idEmployeesListar =  null;
      // this.size = 'xll'; // Necesario para setear el size del modal  
      this.edicion = false;
      this.titelModal = 'Crear Empleado';  // Necesario para setear el titulo   
      this.switch_expression = 'datos'; // Necesario para setear el ngSwitch
    }
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: this.size });
  }
  onCloseModal(e) {
    this.modalRef.close();
    this.getAllData();
  }
  onChange($event, id: string) {
    this.confirmDialogService.confirmThis(`Seguro quiere realizar esta acción en el registro N° ${id} ?`, () =>  {
      let result = $event ? this.activarEmployees(id) : this.desactivarEmployees(id);
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario registro N° ${id}`);
      this.getAllData();
    });
  }
  activarEmployees(id: string) {
    this.employeesService.Activar(id).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Habilitado' , { timeOut: 2500, closeButton: true });
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  desactivarEmployees(id: string) {
    this.employeesService.Desctivar(id).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Desactivado' , { timeOut: 2500, closeButton: true });
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  onPageActive(event){
    this.switch_expression = event;
  }

  searchFilter(e){
    var bodyEmployees = {
      idEmpresa: this.idEmpresa
    };

    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.employeesService.ListarTodos(this.skip, this.take, this.query, this.type, bodyEmployees).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaAlta = (element.fechaAlta != null) ? moment(element.fechaAlta).format('DD-MM-YYYY') : null;
          element.fechaBaja = (element.fechaBaja != null) ? moment(element.fechaBaja).format('DD-MM-YYYY') : null;
        });
        this.listaEmployees = list;
        this.listaEmployees = [...this.listaEmployees];

        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  async obtenerBusiness() {
    // this.business = await this.businessService.ListarEmpresasCombo().toPromise();
    this.businessService.ListarEmpresasCombo().subscribe(
      data => {
        this.business = data;
        // this.idEmpresa = (data[0]) ? data[0].id : null
        this.getAllData();
      },
      error => {
        this.toastr.error(error);
      }
    );
  }
    
  setEmpresa(e) {
    this.getAllData();
  }
}