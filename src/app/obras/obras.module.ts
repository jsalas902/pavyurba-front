import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxDatatableFooterModule } from 'ngx-datatable-footer';
import { NgSelectModule } from '@ng-select/ng-select';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';
import { CardModule } from '../content/partials/general/card/card.module';
import { BreadcrumbModule } from '../_layout/breadcrumb/breadcrumb.module';
import { BlockUIModule } from 'ng-block-ui';
import { BlockTemplateComponent } from '../_layout/blockui/block-template.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared/shared.module';

import { ObrasComponent } from './pages/obras.component';
import { ObrasDetailComponent } from './components/obras-detail.component';
import { ContratosModule } from '../contratos/contratos.module';
import { FacturasModule } from '../facturas/facturas.module';
import { ParteTrabajoModule } from '../parte-trabajo/parte-trabajo.module';
import { InformesModule } from '../informes/informes.module';
// import { GastosGeneralesModule } from '../gastos-generales/gastos-generales.module';
import { CalendariosObrasComponent } from '../estadisticas/components/calendarios-obras/calendarios-obras.component';

@NgModule({
  imports: [
    CommonModule,
    CardModule,
    NgbModule,
    BreadcrumbModule,
    NgSelectModule,
    UiSwitchModule,
    FormsModule,
    ReactiveFormsModule,
    ClipboardModule,
    PerfectScrollbarModule,
    NgxDatatableModule,
    BlockUIModule.forRoot({
      template: BlockTemplateComponent
    }),
    NgxDatatableFooterModule.forRoot(),
    SharedModule,
    ContratosModule,
    FacturasModule,
    ParteTrabajoModule,
    InformesModule,
    // GastosGeneralesModule,
    RouterModule.forChild([
      {
        path: 'list',
        component: ObrasComponent
      },
    ]),
  ],
  declarations: [ObrasComponent, ObrasDetailComponent],
  // exports: [RouterModule, ObrasComponent]
})
export class ObrasModule { }
