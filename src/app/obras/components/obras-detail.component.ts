import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbDateStruct, NgbTimeStruct, NgbDate, NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Obra } from '@app/obras/models/Obra';
import { Location } from '@app/locations/models/Location';
import { Business } from '@app/business/models/Business';
import { Builder } from '../../builders/models/Builder';
import { Client } from '../../clients/models/Client';

import { ObrasService } from '@app/obras/services/obras.service';
import { LocationsService } from '@app/locations/services/locations.service';
import { BusinessService } from '@app/business/services/business.service';
import { BuildersService } from '@app/builders/services/builders.service';
import { ClientsService } from '@app/clients/services/clients.service';
import { ObrasValidator } from "@app/obras/validation/obras.validator";
import { CivilStates } from '@app/_models/constantes/CivilStates';
import { AutonomousClasses } from '@app/_models/constantes/AutonomousClasses';

import { NgbDateCustomParserFormatter} from '@app/_helpers/dateformat';
import { CustomDatepickerI18nService, I18n } from '@app/calendarios/services/custom-datepicker-i18n.service';

@Component({
  selector: 'app-obras-detail',
  templateUrl: './obras-detail.component.html',
  styleUrls: ['./obras-detail.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18nService }
  ]
})
export class ObrasDetailComponent implements OnInit {
  @BlockUI('componentsObrasDetail') blockUIList: NgBlockUI;
  @Input() idObras: string;
  @Output() closeModal = new EventEmitter<boolean>();
  @ViewChild('labelImport', { static: true })
  labelImport: ElementRef;
  fileToUpload: File = null;

  obras: Obra = new Obra();
  locations: Location;
  idProvincia: string = null;
  idLocalidad: string = null;
  otrosDatosFact: string = "false";
  provincias: any;
  business: Business;
  clients: Client;
  builders: Builder;
  obrasForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  titleDocumento: string;
  sizeArchivo: number;
  typeArchivo: string;
  baseArchivo: string | ArrayBuffer;
  contratoValid: boolean = false;
  uploadFile: boolean = false;
  size: string;
  obras_validation_messages = ObrasValidator.OBRAS_VALIDATION_MESSAGES;
  fechaInicio;
  fechaFin;
  dateNow = moment().format("YYYY-MM-DD");
  d: any;
  d2: any;
  readonly DELIMITER = '-';
    // Modal location
    cargaCompleta = false;
    // boolean Otros datos de acturacion
    isOtrosDatos: boolean = false;
    
  mensaje: string = '';
  mensajeDir: string = '';
  mensajeDirFact: string = '';
  maxLength: number = 500;
  constructor(
    private toastr: ToastrService, 
    private obrasService: ObrasService,
    private locationsService: LocationsService,
    private businessService: BusinessService,
    private buildersService: BuildersService,
    private clientsService: ClientsService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.fechaInicio = this.fromModel(this.dateNow);
    this.titleDocumento = "Subir Documento de Presupuesto";
    this.reloadStart();
    this.obrasForm = this.formBuilder.group({
      id: [null],
      nombre: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(250)]],
      volumen: [null],
      fechaInicio: [this.fechaInicio, [Validators.required]],
      fechaFin: [null],
      direccion: [''],
      codigoPostal: [null],
      fueraMadrid: [false],
      otrosDatosFact: ["false"],
      observaciones: ['', [Validators.maxLength(500)]],
      identificacionFact: [''],
      nombreFact: [''],
      emailFact: [''],
      telefonoFact: [''],
      direccionFact: [''],
      idProvincia: [null, [Validators.required]],
      idLocalidad: [null, [Validators.required]],
      idCliente: [null, [Validators.required]],
      idEmpresa: [null, [Validators.required]],
      idConstructora: [null, [Validators.required]],
      presupuesto: [null],
    });
   
    if (this.idObras != null) {
      this.edicion = true;
      this.size = "9";
      this.obtenerObras();
    } else {
      this.size = "6";
    }
    // this.obtenerLocations();
    this.obtenerProvincias();
    this.obtenerBusiness();
    this.obtenerBuilders();
    this.obtenerClients();
    this.reloadStop();
  }

  obtenerObras() {
    this.obrasService.ObtenerPorId(this.idObras).subscribe(
      data => {
        this.obtenerPatchObra(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  obtenerBusiness() {
    this.businessService.ListarEmpresasCombo().subscribe(
      data => {
        this.business = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  obtenerClients() {
    this.clientsService.ListarClientesCombo().subscribe(
      data => {
        this.clients = data;
      },
      error => {
        this.toastr.error(error);
      }
    );
  }
  obtenerBuilders() {
    this.buildersService.ListarConstructorasCombo().subscribe(
      data => {
        this.builders = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  obtenerProvincias() {
    this.locationsService.ListarProvinciasCombo().subscribe(
      data => {
        this.provincias = data;
      },
      error => {
        // this.toastr.error(error);
        this.toastr.error(error);
      }
    );
  }

  obtenerLocations() {
    this.locationsService.ListarLocalidadesCombo(this.idProvincia).subscribe(
      data => {
        this.locations = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  get f() { return this.obrasForm.controls; }
  add() {
    this.submitted = true;
    if (this.obrasForm.invalid) {
        return;
    } else {
      if(this.uploadFile) {
        this.submitted = false;
        this.uploadFile = false;
        return;
      }
    }

    this.obras = this.obrasForm.value;
    // console.log(this.obrasForm.value instanceof Obra);

    this.obras.fechaInicio = "";
    this.obras.fechaInicio = this.toModelFix(this.fechaInicio);
    this.obras.fechaFin = "";
    this.obras.fechaFin = (this.fechaFin != null) ? this.toModelFix(this.fechaFin) : null;

    // if((this.obras.presupuesto == null) && !this.edicion){
    //   this.contratoValid = true;
    //   return
    // } else {
    //   this.contratoValid = false
    // }
    this.reloadStart();
    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }

  }
  create(){
    let obrasTem = new Obra();

    this.obras.fechaInicio = (this.fechaInicio == undefined || this.fechaInicio == "" || this.fechaInicio == null) ? null : this.toModel(this.fechaInicio);
    this.obras.fechaFin = (this.fechaFin == undefined || this.fechaFin == "" || this.fechaFin == null) ? null : this.toModel(this.fechaFin);
    obrasTem = this.changeObject(this.obras, true);
    obrasTem.fechaInicio = (this.fechaInicio == undefined || this.fechaInicio == "" || this.fechaInicio == null) ? null : this.toModel(this.fechaInicio);
    obrasTem.fechaFin = (this.fechaFin == undefined || this.fechaFin == "" || this.fechaFin == null) ? null : this.toModel(this.fechaFin);

    if(this.isOtrosDatos) {
      obrasTem.otrosDatosFact = "true";
      this.otrosDatosFact = "true";
    } else {
      obrasTem.otrosDatosFact = "false";
      this.otrosDatosFact = "false";
    }
    // obrasTem = this.changeObject(this.obras);
    // obrasTem.fechaInicio = this.toModel(this.fechaInicio);
    // obrasTem.fechaFin = (this.fechaFin != null) ? this.toModel(this.fechaFin) : null;
    
    // this.obras.fechaInicio = "";
    // this.obras.fechaInicio = this.toModelFix(this.fechaInicio);
    // this.obras.fechaFin = "";
    // this.obras.fechaFin = (this.fechaFin != null) ? this.toModelFix(this.fechaFin) : null;

    this.obrasService.Crear(obrasTem).subscribe(
      data => {
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        this.reloadStop();
        // this.getAllData();
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        this.reloadStop();
    });
  }

  edit(){
    let obrasTem = new Obra();

    this.obras.fechaInicio = (this.fechaInicio == undefined || this.fechaInicio == "" || this.fechaInicio == null) ? null : this.toModel(this.fechaInicio);
    this.obras.fechaFin = (this.fechaFin == undefined || this.fechaFin == "" || this.fechaFin == null) ? null : this.toModel(this.fechaFin);
    obrasTem = this.changeObject(this.obras, true);
    obrasTem.fechaInicio = (this.fechaInicio == undefined || this.fechaInicio == "" || this.fechaInicio == null) ? null : this.toModel(this.fechaInicio);
    obrasTem.fechaFin = (this.fechaFin == undefined || this.fechaFin == "" || this.fechaFin == null) ? null : this.toModel(this.fechaFin);

    if(this.isOtrosDatos) {
      obrasTem.otrosDatosFact = "true";
      this.otrosDatosFact = "true";
    } else {
      obrasTem.otrosDatosFact = "false";
      this.otrosDatosFact = "false";
    }
    // obrasTem = this.changeObject(this.obras);
    // obrasTem.fechaInicio = this.toModel(this.fechaInicio);
    // obrasTem.fechaFin = this.toModel(this.fechaFin);

    // this.obras.fechaInicio = "";
    // this.obras.fechaInicio = this.toModelFix(this.fechaInicio);
    // this.obras.fechaFin = "";
    // this.obras.fechaFin = this.toModelFix(this.fechaFin);

    this.obrasService.Editar(obrasTem).subscribe(
      data => {
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  reiniciarFormulario() {
    this.obras = new Obra();
    this.obrasForm.reset();
    this.submitted = false;
  }

  obtenerPatchObra(data){
    this.obras = this.changeObject(data, false);
    this.obrasForm.patchValue({
      id: this.obras.id,
      nombre: this.obras.nombre,
      volumen: this.obras.volumen,
      // fechaInicio: this.fechaInicio = this.fromModel(this.obras.fechaInicio),
      // fechaFin: this.fechaFin = this.fromModel(this.obras.fechaFin),
      direccion: this.obras.direccion,
      codigoPostal: this.obras.codigoPostal,
      fueraMadrid: this.obras.fueraMadrid,
      observaciones: this.obras.observaciones,
      otrosDatosFact: this.obras.otrosDatosFact,
      identificacionFact: this.obras.identificacionFact,
      nombreFact: this.obras.nombreFact,
      emailFact: this.obras.emailFact,
      telefonoFact: this.obras.telefonoFact,
      direccionFact: this.obras.direccionFact,
      idLocalidad: this.obras.idLocalidad,
      idProvincia: this.obras.idProvincia,
      idEmpresa: this.obras.idEmpresa,
      idCliente: this.obras.idCliente,
      idConstructora: this.obras.idConstructora,
      presupuesto: this.obras.presupuesto
    });
  }

  changeObject(obras: Obra, valid: boolean) {
    // console.log(obras.direccionFact);
    let obrasTem: Obra = new Obra();
    obrasTem.id = obras.id;
    obrasTem.nombre = obras.nombre;
    obrasTem.volumen = obras.volumen;
    // obrasTem.fechaInicio = obras.fechaInicio;
    // obrasTem.fechaFin = obras.fechaFin;
    obrasTem.fechaInicio = this.fechaInicio = (obras.fechaInicio !== null) ? this.fromModel(obras.fechaInicio) : null;
    obrasTem.fechaFin = this.fechaFin = (obras.fechaFin !== null) ? this.fromModel(obras.fechaFin) : null;
    obrasTem.direccion = obras.direccion;
    obrasTem.codigoPostal = obras.codigoPostal;
    obrasTem.fueraMadrid = obras.fueraMadrid;
    obrasTem.observaciones = obras.observaciones;
    // obrasTem.otrosDatosFact = obras.otrosDatosFact;

    if(valid) {
      obrasTem.otrosDatosFact = (obras.otrosDatosFact == "true") ? 'true' : 'false';
      this.otrosDatosFact = obrasTem.otrosDatosFact;
    } else {
      obrasTem.otrosDatosFact = (obras.otrosDatosFact) ? 'true' : 'false';
      this.otrosDatosFact = obrasTem.otrosDatosFact;
    }

    if(obrasTem.otrosDatosFact == "true") {
      if(!this.isOtrosDatos)
        this.changeOtrosDatosFact(true);
    }
    
    obrasTem.identificacionFact = obras.identificacionFact;
    obrasTem.nombreFact = (obras.nombreFact == "") ? null : obras.nombreFact;
    obrasTem.emailFact = (obras.emailFact == "") ? null : obras.emailFact;
    obrasTem.telefonoFact = (obras.telefonoFact != null) ? ((obras.telefonoFact.toString() == "") ? null : obras.telefonoFact) : null;
    obrasTem.direccionFact = this.mensajeDirFact = (obras.direccionFact == "") ? null : obras.direccionFact;
    // obrasTem.direccionFact = "holas";
    obrasTem.idLocalidad = obras.idLocalidad;
    obrasTem.idProvincia = this.idProvincia = obras.idProvincia;
    obrasTem.idEmpresa = obras.idEmpresa;
    obrasTem.idCliente = obras.idCliente;
    obrasTem.idConstructora = obras.idConstructora;
    // obrasTem.presupuesto = obras.presupuesto;
    if(obras.presupuesto != null){
      obrasTem.presupuesto = {
        "base64": this.baseArchivo,
        "nombre": this.titleDocumento,
        "mimeType": this.typeArchivo,
        "tamano": this.sizeArchivo  
      };
    } else {
      obrasTem.presupuesto = null;
    }

    this.changeProvincia();
    return obrasTem;
  }

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[2], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[0], 10)
      };
    }
    return null;
  }
  
  toModel(date: NgbDateStruct | null): string | null {
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format();
  }
  toModelFix(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format('DD-MM-YYYY');
  }
  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }
  addLocation(modal){
    this.cargaCompleta = true;
    this.locationsService.addModelLocation(modal);
  }
  onSetModal(e) {
    this.obtenerLocations();
  }
  changeOtrosDatosFact(e) {
    this.isOtrosDatos = !this.isOtrosDatos;

    this.otrosDatosFact = this.isOtrosDatos ? "true" : "false";
  }
  onFileChange(event: any) {
    console.log(event);
    this.fileToUpload  = event.target.files.item(0);

    var reader = new FileReader();
    reader.onload = this.convertFileSimple.bind(this, this.fileToUpload);
    reader.readAsBinaryString(this.fileToUpload);
  }

  convertFileSimple(file: File, event: any) {
    const binaryString = event.target.result;
    this.titleDocumento = file.name;
    this.sizeArchivo = file.size;
    this.typeArchivo = file.type;
    this.baseArchivo = btoa(binaryString);
  }

  document() {
    this.uploadFile = true;
    let element: HTMLElement = document.getElementById('presupuesto') as HTMLElement;
    element.click();
  }
  changeProvincia() {
    if(this.idProvincia != null) {
      this.obtenerLocations();
    } else {
      this.idLocalidad = null;
    }
  }
}