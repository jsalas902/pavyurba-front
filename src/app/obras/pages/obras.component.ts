import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { ObrasService } from '@app/obras/services/obras.service';
import { ParteTrabajoService } from '@app/parte-trabajo/services/parte-trabajo.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Obra } from '@app/obras/models/Obra';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { Pagination } from '@app/_models/util/Pagination';
import * as moment from 'moment';
import { InformesTypes } from '@app/informes/models/constantes/InformesTypes';
import { ClientsService } from '@app/clients/services/clients.service';
import { BuildersService } from '@app/builders/services/builders.service';
import { Client } from '@app/clients/models/Client';
import { Builder } from '@app/builders/models/Builder';

@Component({
  selector: 'app-obras',
  templateUrl: './obras.component.html',
  styleUrls: ['./obras.component.css']
})
export class ObrasComponent implements OnInit {

  @BlockUI('componentsObras') blockUIList: NgBlockUI;

  @Input() idObrasListar: string;

  rows: Obra[];
  rowsFiltered: Obra[];
  pagination: Pagination;
  listaObras: Obra[];
  cargaCompleta = false;
  titelModal: string;
  type: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  tipoGeneracion: string = InformesTypes.OBRA;
  private modalRef;
  public menuSettingsConfig: any;
  public switch_expression: string;
  size: string;
  edicion: boolean = false;
  clientes: Client;
  constructoras: Builder; 
  idCliente: number;  
  idConstructora: number; 

  constructor(private toastr: ToastrService,
              private obrasService: ObrasService,
              private clientsService: ClientsService,
              private buildersService: BuildersService,
              private parteTrabajoService: ParteTrabajoService,
              private confirmDialogService: ConfirmDialogService,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private modalService: NgbModal
              ) {
                this.menuSettingsConfig = {
                  vertical_menu: {
                    items: [          
                      {
                        title: 'Datos',
                        icon: 'pavyurba-icon-data',
                        page: 'datos',
                        isSelected: true
                      },
                      // {
                      //   title: 'Calendario',
                      //   icon: 'pavyurba-icon-calendar',
                      //   page: 'calendario'
                      // },
                      // {
                      //   title: 'Incidencia',
                      //   icon: 'pavyurba-icon-incidents',
                      //   page: 'incidencia'
                      // },
                      // {
                      //   title: 'Condiciones',
                      //   icon: 'pavyurba-icon-terms',
                      //   page: 'condiciones'
                      // },
                      {
                        title: 'Contratos',
                        icon: 'pavyurba-icon-contratos',
                        page: 'contratos'
                      },
                      {
                        title: 'Facturas',
                        icon: 'pavyurba-icon-facturas',
                        page: 'facturas'
                      },
                      {
                        title: 'P. de Trabajo',
                        icon: 'pavyurba-icon-pdetrabajo',
                        page: 'parte-trabajo'
                      },
                      {
                        title: 'Informes',
                        icon: 'pavyurba-icon-informe-new',
                        page: 'informes'
                      },
                      // {
                      //   title: 'G. Generales',
                      //   icon: 'pavyurba-icon-paysheet',
                      //   page: 'gastos-generales'
                      // },
                      {
                        title: 'Calendarios',
                        icon: 'pavyurba-icon-calendar',
                        page: 'calendarios'
                      },
                      // {
                      //   title: 'Materiales',
                      //   icon: 'pavyurba-icon-materiales',
                      //   page: 'materiales'
                      // },
                    ]
                  }    
                };

                this.obtenerClientes();
                this.obtenerConstructoras();
}


  ngOnInit(): void {
    this.titelModal = 'Crear Obra';
    this.switch_expression = 'datos';

    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = ' ';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;
    
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    // this.getAllData();
  }

  ngAfterViewInit(): void {
    this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {

    var bodyObras = {
      idCliente: this.idCliente,
      idConstructora: this.idConstructora
    };

    this.reloadStart();
    this.obrasService.ListarTodos(this.skip, this.take, this.query, bodyObras).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
          element.fechaInicio = moment(element.fechaInicio).format('DD-MM-YYYY');
          element.fechaFin = moment(element.fechaFin).format('DD-MM-YYYY');
        });
        this.listaObras = list;
        this.listaObras = [...this.listaObras];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaObras;
    this.rowsFiltered = this.listaObras;
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, row: Obra) {
    this.size = 'xl';
    if(row !== null){
      this.idObrasListar =  row.id.toString();
      this.edicion = true;
      this.titelModal = 'Editar Obra';
    }else{
      this.idObrasListar =  null;
      // this.size = 'xll'; // Necesario para setear el size del modal  
      this.edicion = false;
      this.titelModal = 'Crear Obra';  // Necesario para setear el titulo   
      this.switch_expression = 'datos'; // Necesario para setear el ngSwitch
    }
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: this.size });
  }
  addParteTrabajo(modal){
    this.cargaCompleta = true;
    this.parteTrabajoService.addModelParteTrabajo(modal);
  }
  onCloseModal(e) {
    this.modalRef.close();
    this.getAllData();
  }
  onChange($event, id: string) {
    this.confirmDialogService.confirmThis(`Seguro quiere realizar esta acción en el registro N° ${id} ?`, () =>  {
      let result = $event ? this.activarObras(id) : this.desactivarObras(id);
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario registro N° ${id}`);
      this.getAllData();
    });
  }
  activarObras(id: string) {
    this.obrasService.Activar(id).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Habilitado' , { timeOut: 2500, closeButton: true });
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  desactivarObras(id: string) {
    this.obrasService.Desctivar(id).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Desactivado' , { timeOut: 2500, closeButton: true });
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  onPageActive(event){
    this.switch_expression = event;
    this.menuSettingsConfig.menu = 'collapse';
  }
  onSetModal(e) {
    console.log('evento setclose:',e);
  }

  searchFilter(e){
    var bodyObras = {
      idCliente: this.idCliente,
      idConstructora: this.idConstructora
    };
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.obrasService.ListarTodos(this.skip, this.take, this.query, bodyObras).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
          element.fechaInicio = moment(element.fechaInicio).format('DD-MM-YYYY');
          element.fechaFin = moment(element.fechaFin).format('DD-MM-YYYY');
        });
        this.listaObras = list;
        this.listaObras = [...this.listaObras];

        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  downloadFile(row) {
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = row.presupuesto.url;
    a.target = "_blank";
    a.download = "TEST";
    a.click();
    a.remove();
  }

  setClientes(e) {
    this.getAllData();
  }

  setConstructoras(e) {
    this.getAllData();
  }

  async obtenerClientes() {
    // this.business = await this.businessService.ListarEmpresasCombo().toPromise();
    this.clientsService.ListarClientesCombo().subscribe(
      data => {
        this.clientes = data;
        // this.idObra = (data[0]) ? data[0].id : null
        // this.getAllData();
      },
      error => {
        this.toastr.error(error);
      }
    );
  }

  async obtenerConstructoras() {
    // this.business = await this.businessService.ListarEmpresasCombo().toPromise();
    this.buildersService.ListarConstructorasCombo().subscribe(
      data => {
        this.constructoras = data;
        // this.idProveedor = (data[0]) ? data[0].id : null
        this.getAllData();
      },
      error => {
        this.toastr.error(error);
      }
    );
  }
}