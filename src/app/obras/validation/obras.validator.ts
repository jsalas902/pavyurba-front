export class ObrasValidator {
    public static OBRAS_VALIDATION_MESSAGES = {     
        'presupuesto': [
            { message: 'Presupuesto es requerido' },
        ],   
        'nombre': [
            { type: 'required', message: 'Nombre es requerido' },
            { type: 'minLength', message: 'Nombre no puede tener menos de 3 caracteres' },
            { type: 'maxlength', message: 'Nombre no puede tener más de 100 caracteres' }
        ],
        'volumen': [
            { type: 'required', message: 'Volumen es requerido' },
        ],
        'direccion': [
            { type: 'required', message: 'Dirección es requerido' },
            { type: 'minLength', message: 'Dirección no puede tener menos de 5 caracteres' },
            { type: 'maxLength', message: 'Dirección no puede tener más de 500 caracteres' },
        ],
        'observaciones': [
            { type: 'maxlength', message: 'Observaciones cannot be more than 500 characters long' }
        ],
        'direccionFact': [
            { type: 'maxlength', message: 'Dirección Facturación cannot be more than 500 characters long' }
        ],
        'idProvincia': [
            { type: 'required', message: 'Provincia es requerido' },
        ],
        'idLocalidad': [
            { type: 'required', message: 'Localidad es requerido' },
        ],
        'idEmpresa': [
            { type: 'required', message: 'Empresa es requerido' },
        ],
        'idCliente': [
            { type: 'required', message: 'Cliente es requerido' },
        ],
        'idConstructora': [
            { type: 'required', message: 'Constructora es requerido' },
        ],
        'fechaInicio': [
            { type: 'required', message: 'Fecha Inicio es requerido' },
        ],
        'fechaFin': [
            { type: 'required', message: 'Fecha Fin es requerido' },
        ],        
    }
}