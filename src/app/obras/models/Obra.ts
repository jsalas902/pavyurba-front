import { Location } from '@app/locations/models/Location';
import { Client } from '@app/clients/models/Client';
import { Builder } from '@app/builders/models/Builder';
import { Business } from '@app/business/models/Business';

export class Obra {

    public constructor(init?: Partial<Obra>) {
        Object.assign(this, init);
    }

    id: number;
    nombre: string;
    volumen: number;
    fechaInicio: any;
    fechaFin: any;
    direccion: string;
    codigoPostal: string;
    fueraMadrid: boolean;
    observaciones: string;
    otrosDatosFact: string;
    identificacionFact: string;
    nombreFact: string;
    emailFact: string;
    telefonoFact: number;
    direccionFact: string;
    localidad: Location;
    empresa: Business;
    cliente: Client;
    constructora: Builder;
    idLocalidad: number;
    idProvincia: string;
    idEmpresa: number;
    idCliente: number;
    idConstructora: number;
    presupuesto: any;
    habilitado: boolean;
    fechaHoraLog: Date;
    usuarioLog: string;

}