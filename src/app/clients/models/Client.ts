export class Client {

    public constructor(init?: Partial<Client>) {
        Object.assign(this, init);
    }

    id: number;
    identificacion: string;
    nombre: string;
    email: string;
    emailAdicional: string;
    telefono: string;
    telefonoAdicional: string;
    direccion: string;
    codigoPostal: string;
    fueraMadrid: boolean;
    observaciones: string;
    idLocalidad: number;
    idProvincia: string;
    habilitado: boolean;
    fechaHoraLog: Date;
    usuarioLog: string;
    selected: boolean;

}