import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { ClientsService } from '@app/clients/services/clients.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Client } from '@app/clients/models/Client';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { Pagination } from '@app/_models';
import * as moment from 'moment';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
 
  @BlockUI('componentsClients') blockUIList: NgBlockUI;

  @Input() idClientsListar: string;

 
  rows: Client[];
  rowsFiltered: Client[];
  pagination: Pagination;
  listaClients: Client[];
  cargaCompleta = false;
  titelModal: string;
  type: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  private modalRef;
  public menuSettingsConfig: any;
  public switch_expression: string;
  size: string;
  edicion: boolean = false;

  constructor(private toastr: ToastrService,
              private clientsService: ClientsService,
              private confirmDialogService: ConfirmDialogService,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private modalService: NgbModal) 
  {
    this.menuSettingsConfig = {
      vertical_menu: {
        items: [          
          {
            title: 'Datos',
            icon: 'pavyurba-icon-data',
            page: 'datos',
            isSelected: true
          },
          {
            title: 'Obras',
            icon: 'pavyurba-icon-plays',
            page: 'obras'
          },
          {
            title: 'Informes',
            icon: 'pavyurba-icon-informes',
            page: 'informes'
          },
          {
            title: 'Factura',
            icon: 'pavyurba-icon-facturas',
            page: 'factura'
          },
          {
            title: 'Contratos',
            icon: 'pavyurba-icon-contratos',
            page: 'contratos'
          },
        ]
      }    
    };
  }


  ngOnInit(): void {

    this.titelModal = 'Crear Cliente';
    this.switch_expression = 'datos';

    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = ' ';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;
    
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    this.getAllData();
  }

  ngAfterViewInit(): void {
    this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();
    this.clientsService.ListarTodos(this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
        });
        this.listaClients = list;
        this.listaClients = [...this.listaClients];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaClients;
    this.rowsFiltered = this.listaClients;
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, row: Client) {
    if(row !== null){
      this.idClientsListar =  row.id.toString();
      this.size = 'lg';
      this.edicion = true;
      this.titelModal = 'Editar Cliente';
    }else{
      this.idClientsListar =  null;
      this.size = 'lg'; // Necesario para setear el size del modal  
      this.edicion = false;
      this.titelModal = 'Crear Cliente';  // Necesario para setear el titulo   
      this.switch_expression = 'datos'; // Necesario para setear el ngSwitch
    }
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: this.size });
  }
  onCloseModal(e) {
    this.modalRef.close();
    this.getAllData();
  }
  onChange($event, id: string) {
    this.confirmDialogService.confirmThis(`Seguro quiere realizar esta acción en el registro N° ${id} ?`, () =>  {
      let result = $event ? this.activarClients(id) : this.desactivarClients(id);
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario registro N° ${id}`);
      this.getAllData();
    });
  }
  activarClients(id: string) {
    this.clientsService.Activar(id).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Habilitado' , { timeOut: 2500, closeButton: true });
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  desactivarClients(id: string) {
    this.clientsService.Desctivar(id).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Desactivado' , { timeOut: 2500, closeButton: true });
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  onPageActive(event){
    this.switch_expression = event;
  }

  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.clientsService.ListarTodos(this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
        });
        this.listaClients = list;
        this.listaClients = [...this.listaClients];

        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }
}
