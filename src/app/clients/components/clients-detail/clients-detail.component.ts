import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Client } from '@app/clients/models/Client';
import { Location } from '@app/locations/models/Location';

import { LocationsService } from '@app/locations/services/locations.service';
import { ClientsService } from '@app/clients/services/clients.service';
import { ClientsValidator } from "@app/clients/validation/clients.validator";

@Component({
  selector: 'app-clients-detail',
  templateUrl: './clients-detail.component.html',
  styleUrls: ['./clients-detail.component.css']
})
export class ClientsDetailComponent implements OnInit {
  
  @Input() idClients: string;
  @Output() closeModal = new EventEmitter<boolean>();

  clients: Client = new Client();
  locations: Location;
  idProvincia: string = null;
  idLocalidad: string = null;
  provincias: any;
  clientsForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  clients_validation_messages = ClientsValidator.CLIENTS_VALIDATION_MESSAGES;
  // Modal location
  cargaCompleta = false;
  
  mensaje: string = '';
  mensajeDir: string = '';
  maxLength: number = 500;
  
  constructor(
    private toastr: ToastrService, 
    private locationsService: LocationsService,
    private clientsService: ClientsService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.clientsForm = this.formBuilder.group({
      id: [null],
      nombre: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      identificacion: [null, [Validators.required]],
      idProvincia: [null, [Validators.required]],
      idLocalidad: [null, [Validators.required]],
      direccion: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(500)]],
      codigoPostal: [null],
      email: [null],
      telefono: [null],
      emailAdicional: [null],
      telefonoAdicional: [null],
      observaciones: [null, [Validators.maxLength(500)]],
    });
   
    if (this.idClients != null) {
      this.edicion = true;
      this.obtenerClients();
    }
    // this.obtenerLocations();
    this.obtenerProvincias();
  }

  obtenerClients() {
    this.clientsService.ObtenerPorId(this.idClients).subscribe(
      data => {
        this.obtenerPatchClient(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  obtenerProvincias() {
    this.locationsService.ListarProvinciasCombo().subscribe(
      data => {
        this.provincias = data;
      },
      error => {
        // this.toastr.error(error);
        this.toastr.error(error.mensaje);
      }
    );
  }

  obtenerLocations() {
    this.locationsService.ListarLocalidadesCombo(this.idProvincia).subscribe(
      data => {
        this.locations = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  get f() { return this.clientsForm.controls; }
  add() {
    this.submitted = true;
    if (this.clientsForm.invalid) {
        return;
    }

    this.clients = this.clientsForm.value;
    // console.log(this.clientsForm.value instanceof Client);
    
    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }

  }
  create(){
    let clientsTem = new Client();
    clientsTem = this.changeObject(this.clients);
   
    this.clientsService.Crear(clientsTem).subscribe(
      data => {
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  edit(){
    let clientsTem = new Client();
    clientsTem = this.changeObject(this.clients);

    this.clientsService.Editar(clientsTem).subscribe(
      data => {
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  reiniciarFormulario() {
    this.clients = new Client();
    this.clientsForm.reset();
    this.submitted = false;
  }
  obtenerPatchClient(data){
    this.clients = this.changeObject(data);
    this.clientsForm.patchValue({
      id: this.clients.id,
      identificacion: this.clients.identificacion,
      nombre: this.clients.nombre,
      idLocalidad: this.clients.idLocalidad,
      idProvincia: this.clients.idProvincia,
      direccion: this.clients.direccion,
      codigoPostal: this.clients.codigoPostal,
      email: this.clients.email,
      emailAdicional: this.clients.emailAdicional,
      telefono: this.clients.telefono,
      telefonoAdicional: this.clients.telefonoAdicional,
      observaciones: this.clients.observaciones,
    });
  }
  changeObject(clients: Client) {
    let clientsTem: Client = new Client();
    clientsTem.id = clients.id;
    clientsTem.nombre = clients.nombre;
    clientsTem.identificacion = clients.identificacion;
    clientsTem.email = (clients.email == "") ? null : clients.email;
    clientsTem.emailAdicional = (clients.emailAdicional == "") ? null : clients.emailAdicional;
    clientsTem.telefono = (clients.telefono == "") ? null : clients.telefono;
    clientsTem.telefonoAdicional = (clients.telefonoAdicional == "") ? null : clients.telefonoAdicional;
    clientsTem.direccion = clients.direccion;
    clientsTem.codigoPostal = clients.codigoPostal;
    clientsTem.observaciones = clients.observaciones;
    clientsTem.idLocalidad = clients.idLocalidad;
    clientsTem.idProvincia = this.idProvincia = clients.idProvincia;

    this.changeProvincia();
    return clientsTem;
  }
  addLocation(modal){
    this.cargaCompleta = true;
    this.locationsService.addModelLocation(modal);
  }
  onSetModal(e) {
    this.obtenerLocations();
  }
  changeProvincia() {
    if(this.idProvincia != null) {
      this.obtenerLocations();
    } else {
      this.idLocalidad = null;
    }
  }
}
