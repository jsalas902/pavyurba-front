import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BlockUIModule } from 'ng-block-ui';
import { BlockTemplateComponent } from '../_layout/blockui/block-template.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared/shared.module';
import { LocationsModule } from '../locations/locations.module';
import { DietasModule } from '../dietas/dietas.module';
import { ConditionsModule } from '../conditions/conditions.module';

import { SettingsComponent } from './pages/settings.component';



@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    PerfectScrollbarModule,
    BlockUIModule.forRoot({
      template: BlockTemplateComponent
    }),
    SharedModule,
    LocationsModule,
    DietasModule,
    ConditionsModule,
    RouterModule.forChild([
      {
        path: '',
        component: SettingsComponent
      },
    ]),
  ],
  declarations: [SettingsComponent],
  exports: [RouterModule, SettingsComponent]
})
export class SettingsModule { }
