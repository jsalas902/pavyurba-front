import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  @BlockUI('componentsSettings') blockUIList: NgBlockUI;
 
  public menuSettingsConfig: any;
  public switch_expression: string;
  cargaCompleta = false;

  constructor(private cdRef: ChangeDetectorRef) { 

    this.menuSettingsConfig = {
      vertical_menu: {
        items: [          
          // {
          //   title: 'Datos',
          //   icon: 'pavyurba-icon-data',
          //   page: 'datos',
          //   isSelected: true
          // },
          // {
          //   title: 'Materiales',
          //   icon: 'pavyurba-icon-materiales',
          //   page: 'materiales'
          // },
          // {
          //   title: 'Obras',
          //   icon: 'pavyurba-icon-plays',
          //   page: 'obras'
          // },
          {
            title: 'Condiciones',
            icon: 'pavyurba-icon-terms',
            page: 'condiciones',
            isSelected: true
          },
          // {
          //   title: 'Albaranes',
          //   icon: 'pavyurba-icon-albaranes',
          //   page: 'albaranes'
          // },
          // {
          //   title: 'Factura',
          //   icon: 'pavyurba-icon-facturas',
          //   page: 'factura'
          // },
          {
            title: 'Localidades',
            icon: 'pavyurba-icon-terms',
            page: 'locations'
          },
          {
            title: 'Dietas',
            icon: 'pavyurba-icon-terms',
            page: 'dietas'
          },
          // {
          //   title: 'Generar Inf.',
          //   icon: 'pavyurba-icon-informes',
          //   page: 'generar-informes'
          // },
        ]
      }    
    };
  }
  ngOnInit(): void {
    this.switch_expression = '';
    console.log('caso:', this.switch_expression);
  }
  ngAfterViewInit(): void {
    this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }
  onPageActive(event){
    this.switch_expression = event;
    console.log('caso:', this.switch_expression);
  }
  
}
