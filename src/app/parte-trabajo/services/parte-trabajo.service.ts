import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { GenericService, AuthenticationService } from '@app/_services';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Pagination } from '@app/_models';
import { ParteTrabajo } from '../models/ParteTrabajo';

@Injectable({
  providedIn: 'root'
})
export class ParteTrabajoService extends GenericService{
  private modalRef;

  constructor(private http: HttpClient,
    public authService: AuthenticationService,
    private injector: Injector,
    private modalService: NgbModal) {
  super();
  }

  ListarParteTrabajoPorObras(idObra: string, conFeriadosFestivos: boolean, skip: string, take: string, query: string): Observable<Pagination> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/partesTrabajos/listarPorObra/' + idObra + '/' + conFeriadosFestivos + '?skip=' + skip + '&take=' + take + '&query=' + query, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  GetParteTrabajoPorId(idParteTrabajo: string): Observable<Pagination> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/partesTrabajos/obtenerPorId/' + idParteTrabajo, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta.data;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  CrearParteTrabajo(parteTrabajo: ParteTrabajo): Observable<any> {

    // this.headers = new HttpHeaders ({
    //   Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    // });
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));

    return this.http.post(environment.apiUrl + '/partesTrabajos', parteTrabajo, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  EditaParteTrabajo(parteTrabajo: ParteTrabajo): Observable<any> {

    // this.headers = new HttpHeaders ({
    //   Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    // });
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));

    return this.http.put(environment.apiUrl + '/partesTrabajos/' + parteTrabajo.id, parteTrabajo, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  EditaParteTrabajoPorObra(parteTrabajo: ParteTrabajo): Observable<any> {

    // this.headers = new HttpHeaders ({
    //   Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    // });
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));

    return this.http.put(environment.apiUrl + '/partesTrabajos/actualizarPartePorObraTrabajo/' + parteTrabajo.id, parteTrabajo, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  addModelParteTrabajo(modal: any) {
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: "md" });
  }
  onCloseModal() {
    this.modalRef.close();
  }
}
