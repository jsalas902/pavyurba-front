export class ParteTrabajo {

    public constructor(init?: Partial<ParteTrabajo>) {
        Object.assign(this, init);
    }

    id: number;
    fecha: any;
    metros: number;
    precio: number;
    valor: number;
    observaciones: string;
    feriadoFestivo: boolean;
    // empleado: string;
    // obra: string;
    // condicion: string;
    fueraMadrid: boolean;
    dieta: number;
    idEmpleado: string;
    idObra: string;
    idCondicion: string; 
}