export class ParteTrabajoValidator {
    public static PARTETRABAJO_VALIDATION_MESSAGES = {        
        'idEmpleado': [
            { type: 'required', message: 'Trabajador es requerido' },
        ],
        'idObra': [
            { type: 'required', message: 'Obra es requerido' },
        ],
        'fecha': [
            { type: 'required', message: 'Fecha es requerido' },
        ],
        'fechaEdit': [
            { message: 'Fecha es requerido' },
        ],
        'observaciones': [
            { type: 'required', message: 'Observaciones es requerido' },
        ],
        'idCondicion': [
            { type: 'required', message: 'Condición es requerido' },
        ],
        'dieta': [
            { type: 'required', message: 'Dieta es requerido' },
        ],
        'metros': [
            { type: 'required', message: 'Metros es requerido' },
        ],
        'precio': [
            { type: 'required', message: 'Valor es requerido' },
        ],
    }
}