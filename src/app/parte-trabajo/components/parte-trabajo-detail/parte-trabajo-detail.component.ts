import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbDateStruct, NgbTimeStruct, NgbDate, NgbModal, NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { Condition } from '@app/conditions/models/condition';
import { Obra } from '@app/obras/models/Obra';
import { Employee } from '@app/employees/models/Employee';
import { ParteTrabajo } from '@app/parte-trabajo/models/ParteTrabajo';
import { ParteTrabajoValidator } from "@app/parte-trabajo/validation/parte-trabajo.validator";
import { ParteTrabajoService } from '@app/parte-trabajo/services/parte-trabajo.service';
import { ConditionsService } from '@app/conditions/services/conditions.service';
import { ObrasService } from '@app/obras/services/obras.service';
import { EmployeesService } from '@app/employees/services/employees.service';

import { NgbDateCustomParserFormatter} from '@app/_helpers/dateformat';
import { CustomDatepickerI18nService, I18n } from '@app/calendarios/services/custom-datepicker-i18n.service';

@Component({
  selector: 'app-parte-trabajo-detail',
  templateUrl: './parte-trabajo-detail.component.html',
  styleUrls: ['./parte-trabajo-detail.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18nService }
  ]
})
export class ParteTrabajoDetailComponent implements OnInit {

  @Input() idObras: string;
  @Input() idParteTrabajo: string;
  @Output() closeModal = new EventEmitter<boolean>();

  parteTrabajo: ParteTrabajo = new ParteTrabajo();
  parteTrabajoForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  fueraMadrid: boolean = false;
  valorDM: number;
  valorFM: number;
  condiciones: any[];
  obrasCombo: Obra;
  employeesCombo: any[];
  partetrabajo_validation_messages = ParteTrabajoValidator.PARTETRABAJO_VALIDATION_MESSAGES;
  fecha;
  fechaEdit;
  fechaValid: boolean = false;
  validFestivo: boolean = false;
  feriadoFestivo: boolean = false;
  totalValor: number = null;
  datoValor: number = null;
  datoMetros: number = null;
  d: any;
  dateNow = moment().format("YYYY-MM-DD");
  readonly DELIMITER = '-';
  
  mensaje: string = '';
  maxLength: number = 500;

  constructor(
    private toastr: ToastrService, 
    private parteTrabajoService: ParteTrabajoService,
    private conditionsService: ConditionsService,
    private employeesService: EmployeesService,
    private obrasService: ObrasService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.fecha = (this.idParteTrabajo != null) ? null : this.fromModel(this.dateNow);
    // this.obtenerCondiciones();
    this.obtenerEmployees();
    if (this.idParteTrabajo != null) {
      this.edicion = true;
      this.parteTrabajoForm = this.formBuilder.group({
        id: [this.idParteTrabajo],
        fecha: [null],
        metros: [null, [Validators.required]],
        valor: [null],
        observaciones: [null],
        idEmpleado: [null, [Validators.required]],
        idObra: [null, [Validators.required]],
        idCondicion: [null, [Validators.required]],
        dieta: [null, [Validators.required]],
        precio: [null, [Validators.required]],
        fueraMadrid: [false],
        feriadoFestivo: [false],
      });
      this.obtenerParteTrabajo();
    } else {
      this.parteTrabajoForm = this.formBuilder.group({
        id: [this.idParteTrabajo],
        fecha: [this.fromModel(this.dateNow), [Validators.required]],
        metros: [null, [Validators.required]],
        valor: [null],
        observaciones: [null],
        idEmpleado: [null, [Validators.required]],
        idObra: [this.idObras, [Validators.required]],
        idCondicion: [null, [Validators.required]],
        dieta: [null, [Validators.required]],
        precio: [null, [Validators.required]],
        fueraMadrid: [false],
        feriadoFestivo: [false],
      });
    }

    if(this.idObras == null) {
      this.obtenerObrasCombos();
    } else {
      this.changeObras(this.idObras);
    }

  }

  get f() { return this.parteTrabajoForm.controls; }
  add() {
    this.submitted = true;

    if (this.parteTrabajoForm.invalid) {
      if(this.edicion) {
        if(this.parteTrabajoForm.value.metros == null ||  
          this.parteTrabajoForm.value.valor == null ||
          this.parteTrabajoForm.value.idEmpleado == null ||
          this.parteTrabajoForm.value.idObra == null ||
          this.parteTrabajoForm.value.idCondicion == null ||
          this.parteTrabajoForm.value.dieta == null) {
            if((this.parteTrabajoForm.value.fecha == null || this.parteTrabajoForm.value.fecha == "")){
              this.fechaValid = true;
            } else {
              this.fechaValid = false;
            }
            return;
          }
      } else {
        return;
      }      
    } 
    
    this.parteTrabajo = this.parteTrabajoForm.value;

    if(this.edicion) {
      if((this.parteTrabajo.fecha == null || this.parteTrabajo.fecha == "")){
        this.fechaValid = true;
        return
      } else {
        this.fechaValid = false;
      }
      this.edit();
    }else {
      this.create();     
    }
  }
  create(){
    this.fecha = this.parteTrabajo.fecha
    let parteTrabajoTem = new ParteTrabajo();

    this.parteTrabajo.fecha = (this.fecha == undefined || this.fecha == "" || this.fecha == null) ? null : this.toModelFix(this.fecha);
    parteTrabajoTem = this.changeObject(this.parteTrabajo);
    parteTrabajoTem.fecha = (this.fecha == undefined || this.fecha == "" || this.fecha == null) ? null : this.toModel(this.fecha);

    // parteTrabajoTem = this.changeObject(this.parteTrabajo);

    // parteTrabajoTem.fecha = this.toModel(this.fecha);
    
    // this.parteTrabajo.fecha = "";
    // this.parteTrabajo.fecha = this.toModelFix(this.fecha);

    this.parteTrabajoService.CrearParteTrabajo(parteTrabajoTem).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
        this.closeModal.emit(false);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  edit(){
    this.fechaEdit = this.parteTrabajo.fecha;
    let parteTrabajoTem = new ParteTrabajo();

    this.parteTrabajo.fecha = (this.fechaEdit == undefined || this.fechaEdit == "" || this.fechaEdit == null) ? null : this.toModelFix(this.fechaEdit);
    parteTrabajoTem = this.changeObjectEdit(this.parteTrabajo, false);
    parteTrabajoTem.fecha = (this.fechaEdit == undefined || this.fechaEdit == "" || this.fechaEdit == null) ? null : this.toModel(this.fechaEdit);
    // this.parteTrabajo.fecha = (this.fecha == undefined || this.fecha == "" || this.fecha == null) ? null : this.toModel(this.fecha);
    // parteTrabajoTem = this.changeObject(this.parteTrabajo);
    // parteTrabajoTem.fecha = (this.fecha == undefined || this.fecha == "" || this.fecha == null) ? null : this.toModel(this.fecha);

    // parteTrabajoTem = this.changeObject(this.parteTrabajo);
    // parteTrabajoTem.fecha = this.toModel(this.fecha);
    parteTrabajoTem.fueraMadrid = this.fueraMadrid;


    
    // this.parteTrabajo.fecha = "";
    // this.parteTrabajo.fecha = this.toModelFix(this.fecha);

    // employeesTem.fijo = (employeesTem.fijo == "true") ? true : false;

    this.parteTrabajoService.EditaParteTrabajo(parteTrabajoTem).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
        this.closeModal.emit(false);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  reiniciarFormulario() {
    this.parteTrabajo = new ParteTrabajo();
    this.parteTrabajoForm.reset();
    this.submitted = false;
  }
  obtenerParteTrabajo() {
    this.parteTrabajoService.GetParteTrabajoPorId(this.idParteTrabajo).subscribe(
      data => {
        this.obtenerPatchParteTrabajo(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  changeObject(parteTrabajo: ParteTrabajo) {
    // this.fecha = this.fromModel(parteTrabajo.fecha);
    let parteTrabajoTem: ParteTrabajo = new ParteTrabajo();
    parteTrabajoTem.id = parteTrabajo.id;
    parteTrabajoTem.metros = parteTrabajo.metros;
    parteTrabajoTem.precio = parteTrabajo.precio;
    parteTrabajoTem.valor = parteTrabajo.valor;
    parteTrabajoTem.observaciones = parteTrabajo.observaciones;
    parteTrabajoTem.idEmpleado = parteTrabajo.idEmpleado;
    // parteTrabajoTem.fecha = this.fromModel(this.fecha);
    parteTrabajoTem.fecha = this.fecha;
    // parteTrabajoTem.fecha = this.fromModel(parteTrabajo.fecha);
    parteTrabajoTem.idObra = parteTrabajo.idObra;
    parteTrabajoTem.idCondicion = parteTrabajo.idCondicion;
    parteTrabajoTem.fueraMadrid = parteTrabajo.fueraMadrid;
    parteTrabajoTem.feriadoFestivo = parteTrabajo.feriadoFestivo;
    parteTrabajoTem.dieta = parteTrabajo.dieta;
    return parteTrabajoTem;
  }
  changeObjectEdit(parteTrabajo: ParteTrabajo, validDate: boolean) {
    if(validDate) {
      this.fechaEdit = this.fromModel(parteTrabajo.fecha);
      this.fechaEdit = this.toModelFix(this.fechaEdit);
    }
    // this.fechaEdit = this.fromModel(parteTrabajo.fecha);
    let parteTrabajoTem: ParteTrabajo = new ParteTrabajo();
    parteTrabajoTem.id = parteTrabajo.id;
    parteTrabajoTem.metros = parteTrabajo.metros;
    parteTrabajoTem.precio = parteTrabajo.precio;
    parteTrabajoTem.valor = parteTrabajo.valor;
    parteTrabajoTem.observaciones = parteTrabajo.observaciones;
    parteTrabajoTem.idEmpleado = parteTrabajo.idEmpleado;
    // parteTrabajoTem.fecha = this.fecha = (parteTrabajo.fecha !== null) ? this.fromModel(parteTrabajo.fecha) : null;
    parteTrabajoTem.fecha = this.fechaEdit;
    parteTrabajoTem.idObra = parteTrabajo.idObra;
    parteTrabajoTem.idCondicion = parteTrabajo.idCondicion;
    parteTrabajoTem.fueraMadrid = parteTrabajo.fueraMadrid;
    parteTrabajoTem.feriadoFestivo = parteTrabajo.feriadoFestivo;
    parteTrabajoTem.dieta = parteTrabajo.dieta;

    return parteTrabajoTem;
  }
  obtenerCondiciones() {
    this.conditionsService.ListarCondicionesCombo().subscribe(
      data => {
        this.condiciones = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  obtenerObrasCombos() {
    this.obrasService.ListarObrasCombo().subscribe(
      data => {
        this.obrasCombo = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  obtenerEmployees() {
    this.employeesService.ListarEmployeesCombo().subscribe(
      data => {
        this.employeesCombo = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  changeObras(id: string) {
    id = (id=='') ? this.parteTrabajoForm.value.idObra : id;
    if(id != null){ 
      this.obrasService.ObtenerPorId(id).subscribe(
        data => {
          // this.condiciones = data;
          this.fueraMadrid = data.fueraMadrid;
  
          if(this.valorDM != null && this.valorDM != undefined) {
            this.parteTrabajoForm.patchValue({
              fueraMadrid: data.fueraMadrid,
              dieta: (this.fueraMadrid) ? this.valorFM : this.valorDM,
              // valor: (this.fueraMadrid) ? this.valorFM : this.valorDM,
            });
  
            if(this.datoMetros != null && this.datoValor != null){
              this.totalValor = this.datoMetros * this.datoValor;
              this.totalValor = Number(this.totalValor.toFixed(2));
            }
          } else {
            this.parteTrabajoForm.patchValue({
              fueraMadrid: data.fueraMadrid,
            });
          }
  
          if(this.parteTrabajoForm.value.idCondicion != null){
            if(this.condiciones != undefined){
              this.condiciones.forEach(element => {
                if(element.id == this.parteTrabajoForm.value.idCondicion) {
                  this.parteTrabajoForm.patchValue({
                    // dieta: (this.fueraMadrid) ? this.valorFM : this.valorDM,
                    precio: (this.fueraMadrid) ? element.valorFM : element.valorDM,
                  });
                }
              });
            }
          } else {
            this.datoMetros = null;
            this.datoValor = null;
            this.totalValor = null;
          }
        },
        error => {
          // this.toastr.error(error.mensaje);
          this.toastr.error(error);
        }
      );
    }
  }
  changeTrabajador(id: string) {
    id = (id=='') ? this.parteTrabajoForm.value.idEmpleado : id;
    this.validFestivo = false;
    this.feriadoFestivo = false;
    this.datoMetros = null;
    this.datoValor = null;
    this.totalValor = null;  
    this.parteTrabajoForm.patchValue({
      idCondicion: null,
    });
    this.conditionsService.ListarCondicionesComboEmpleados(id).subscribe(
      data => {
        /*this.parteTrabajoForm.patchValue({
          fueraMadrid: data.,
        });*/
        this.condiciones = data;

        this.employeesCombo.forEach(element => {
          if(element.id == this.parteTrabajoForm.value.idEmpleado){
            this.valorDM = element.valorDM;
            this.valorFM = element.valorFM;

            if(element.parametro == "ASEGURADO JORNAL") {
              this.validFestivo = true;
            }
          }
          
        });

        if(this.idObras == null) {
          if(this.parteTrabajoForm.value.idObra != null){
            this.parteTrabajoForm.patchValue({
              dieta: (this.fueraMadrid) ? this.valorFM : this.valorDM,
              // valor: (this.fueraMadrid) ? this.valorFM : this.valorDM,
            });
          } 
        } else {
          this.parteTrabajoForm.patchValue({
            dieta: (this.fueraMadrid) ? this.valorFM : this.valorDM,
            // valor: (this.fueraMadrid) ? this.valorFM : this.valorDM,
          });
        }
        

        if(this.datoMetros != null && this.datoValor != null){
          this.totalValor = this.datoMetros * this.datoValor;
          this.totalValor = Number(this.totalValor.toFixed(2));
        }        
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  changeMetros() {
    if(this.datoMetros != null && this.datoValor != null){
      this.totalValor = this.datoMetros * this.datoValor;
      this.totalValor = Number(this.totalValor.toFixed(2));
    }
  }

  obtenerPatchParteTrabajo(data){
    this.parteTrabajo = this.changeObjectEdit(data, true);
    this.changeTrabajador(this.parteTrabajo.idEmpleado);
    if(this.parteTrabajo.feriadoFestivo) {
      this.feriadoFestivo = true;
    }
    this.parteTrabajoForm.patchValue({
      id: this.parteTrabajo.id,
      metros: this.datoMetros = this.parteTrabajo.metros,
      valor: this.totalValor = this.parteTrabajo.valor,
      observaciones: this.parteTrabajo.observaciones,
      idEmpleado: this.parteTrabajo.idEmpleado,
      fecha: this.fromModel(this.parteTrabajo.fecha),
      idObra: this.parteTrabajo.idObra,
      idCondicion: this.parteTrabajo.idCondicion,
      dieta: this.parteTrabajo.dieta,
      fueraMadrid: this.fueraMadrid = this.parteTrabajo.fueraMadrid,
      feriadoFestivo: this.parteTrabajo.feriadoFestivo,
      precio: this.datoValor = this.parteTrabajo.precio
      // totalValor: this.parteTrabajo.metros * this.parteTrabajo.valor
    });
    // this.totalValor = String(this.parteTrabajo.metros * this.parteTrabajo.valor);
  }
  toModel(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format();
  }
  toModelFix(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format('DD-MM-YYYY');
  }
  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[2], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[0], 10)
      };
    }
    return null;
  }
  changeCondicion(){
    if(this.parteTrabajoForm.value.idCondicion != null) {
      if(this.parteTrabajoForm.value.idObra != null || this.idObras != null){
        this.condiciones.forEach(element => {
          if(element.id == this.parteTrabajoForm.value.idCondicion) {
            this.parteTrabajoForm.patchValue({
              // dieta: (this.fueraMadrid) ? this.valorFM : this.valorDM,
              precio: (this.fueraMadrid) ? element.valorFM : element.valorDM,
            });
          }
        });
      }
    }

    this.changeMetros();
  }
}
