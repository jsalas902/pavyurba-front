import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ParteTrabajoService } from '@app/parte-trabajo/services/parte-trabajo.service';

@Component({
  selector: 'app-modal-parte-trabajo-detail',
  templateUrl: './modal-parte-trabajo-detail.component.html',
  styleUrls: ['./modal-parte-trabajo-detail.component.css']
})
export class ModalParteTrabajoDetailComponent implements OnInit {
  @Input() idObras: string;
  @Input() titel: string;
  
  @Input() idParteTrabajo: string;
  @Input() skip: string;
  @Input() take: string;
  @Input() query: string;
  
  @Output() setModal = new EventEmitter<boolean>();
  cargaCompletaModal:boolean;

  constructor(private modalService: NgbModal,private parteTrabajoService: ParteTrabajoService) { }

  ngOnInit(): void {
    this.cargaCompletaModal = false;
  }
  
  ngAfterViewInit(): void {
    this.cargaCompletaModal = true;
  }

  onCloseModal(e) {
    if (!e) this.setModal.emit(true);
        
    this.parteTrabajoService.onCloseModal();   
  }
}
