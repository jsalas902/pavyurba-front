export class Builder {

    public constructor(init?: Partial<Builder>) {
        Object.assign(this, init);
    }

    id: number;
    esCliente: boolean;
    identificacion: string;
    nombre: string;
    nombreContacto: string;
    identificacionContacto: string;
    email: string;
    emailContacto: string;
    telefono: string;
    telefonoContacto: string;
    direccion: string;
    codigoPostal: string;
    fueraMadrid: boolean;
    observaciones: string;
    idLocalidad: number;
    idProvincia: string;
    habilitado: boolean;
    fechaHoraLog: Date;
    usuarioLog: string;
    selected: boolean;
    localidad: string;

}