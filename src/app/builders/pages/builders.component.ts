import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { BuildersService } from '@app/builders/services/builders.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Builder } from '@app/builders/models/Builder';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Pagination } from '@app/_models/util/Pagination';
import * as moment from 'moment';

@Component({
  selector: 'app-builders',
  templateUrl: './builders.component.html',
  styleUrls: ['./builders.component.css']
})
export class BuildersComponent implements OnInit {
 
  @BlockUI('componentsBuilders') blockUIList: NgBlockUI;

  @Input() idBuildersListar: string;

  rows: Builder[];
  rowsFiltered: Builder[];
  pagination: Pagination;
  listaBuilders: Builder[];
  cargaCompleta = false;
  titelModal: string;
  type: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  private modalRef;
  public menuSettingsConfig: any;
  public switch_expression: string;
  size: string;
  edicion: boolean = false;

  constructor(private toastr: ToastrService,
              private buildersService: BuildersService,
              private confirmDialogService: ConfirmDialogService,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private modalService: NgbModal
              ) {
                this.menuSettingsConfig = {
                  vertical_menu: {
                    items: [          
                      {
                        title: 'Datos',
                        icon: 'pavyurba-icon-data',
                        page: 'datos',
                        isSelected: true
                      },
                      {
                        title: 'Obras',
                        icon: 'pavyurba-icon-plays',
                        page: 'obras'
                      },
                      {
                        title: 'Informes',
                        icon: 'pavyurba-icon-informes',
                        page: 'informes'
                      },
                      {
                        title: 'Factura',
                        icon: 'pavyurba-icon-facturas',
                        page: 'factura'
                      },
                      {
                        title: 'Contratos',
                        icon: 'pavyurba-icon-contratos',
                        page: 'contratos'
                      },
                    ]
                  }    
                };
}


  ngOnInit(): void {

    this.titelModal = 'Crear Constructora';
    this.switch_expression = 'datos';

    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = ' ';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;
    
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    this.getAllData();
  }

  ngAfterViewInit(): void {
    this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();
    this.buildersService.ListarTodos(this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // this.pagination.total = data.data.length;
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
        });
        this.listaBuilders = list;
        this.listaBuilders = [...this.listaBuilders];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaBuilders;
    this.rowsFiltered = this.listaBuilders;
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, row: Builder) {
    if(row !== null){
      this.idBuildersListar =  row.id.toString();
      this.size = 'lg';
      this.edicion = true;
      this.titelModal = 'Editar Constructora';
    }else{
      this.idBuildersListar =  null;
      this.size = 'lg'; // Necesario para setear el size del modal  
      this.edicion = false;
      this.titelModal = 'Crear Constructora';  // Necesario para setear el titulo   
      this.switch_expression = 'datos'; // Necesario para setear el ngSwitch
    }
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: this.size });
  }
  onCloseModal(e) {
    this.modalRef.close();
    this.getAllData();
  }
  onChange($event, id: string) {
    this.confirmDialogService.confirmThis(`Seguro quiere realizar esta acción en el registro N° ${id} ?`, () =>  {
      let result = $event ? this.activarBuilders(id) : this.desactivarBuilders(id);
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario registro N° ${id}`);
      this.getAllData();
    });
  }
  activarBuilders(id: string) {
    this.buildersService.Activar(id).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Habilitado' , { timeOut: 2500, closeButton: true });
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  desactivarBuilders(id: string) {
    this.buildersService.Desctivar(id).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Desactivado' , { timeOut: 2500, closeButton: true });
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  onPageActive(event){
    this.switch_expression = event;
  }

  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.buildersService.ListarTodos(this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
        });
        this.listaBuilders = list;
        this.listaBuilders = [...this.listaBuilders];

        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }
}