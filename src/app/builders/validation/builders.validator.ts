export class BuildersValidator {
    public static BUILDERS_VALIDATION_MESSAGES = {
        'nombre': [
            { type: 'required', message: 'Nombre es requerido' },
            { type: 'minlength', message: 'El Nombre no puede tener menos de 3 caracteres' },
            { type: 'maxlength', message: 'El nombre no puede tener más de 100 caracteres' }
        ],
        'nombreContacto': [
            { type: 'required', message: 'Nombre es requerido' },
            { type: 'minlength', message: 'El Nombre no puede tener menos de 3 caracteres' },
            { type: 'maxlength', message: 'El nombre no puede tener más de 100 caracteres' }
        ],
        'identificacion': [
            { type: 'required', message: 'Identificacion es requerido' },
        ],
        'identificacionContacto': [
            { type: 'required', message: 'Identificacion Contacto es requerido' },
        ],
        'idProvincia': [
            { type: 'required', message: 'Provincia es requerido' },
        ],
        'idLocalidad': [
            { type: 'required', message: 'Localidad es requerido' },
        ],
        'direccion': [
            { type: 'required', message: 'Dirección es requerido' },
            { type: 'minlength', message: 'La dirección no puede tener menos de 5 caracteres' },
            { type: 'maxlength', message: 'La dirección no puede tener más de 500 caracteres' }
        ],
        'telefono': [
            { type: 'required', message: 'Teléfono es requerido' },
        ],
        'telefonoContacto': [
            { type: 'required', message: 'Teléfono Contacto es requerido' },
        ],
        'email': [
            { type: 'required', message: 'E-mail es requerido' },
        ],
        'emailContacto': [
            { type: 'required', message: 'E-mail Contacto es requerido' },
        ],
        'observaciones': [
            { type: 'maxlength', message: 'Las observaciones no pueden tener más de 500 caracteres' }
        ],
    }
}