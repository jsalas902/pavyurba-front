import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Builder } from '@app/builders/models/Builder';
import { Location } from '@app/locations/models/Location';

import { LocationsService } from '@app/locations/services/locations.service';
import { BuildersService } from '@app/builders/services/builders.service';
import { BuildersValidator } from "@app/builders/validation/builders.validator";
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-builders-detail',
  templateUrl: './builders-detail.component.html',
  styleUrls: ['./builders-detail.component.css']
})
export class BuildersDetailComponent implements OnInit {
 
  @Input() idBuilders: string;
  @Output() closeModal = new EventEmitter<boolean>();
  @BlockUI('componentsBuildersDetail') blockUIList: NgBlockUI;

  builders: Builder = new Builder();
  locations: Location;
  idProvincia: string = null;
  idLocalidad: string = null;
  provincias: any;
  buildersForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  builders_validation_messages = BuildersValidator.BUILDERS_VALIDATION_MESSAGES;
  // Modal location
  cargaCompleta = false;
  
  mensaje: string = '';
  mensajeDir: string = '';
  maxLength: number = 500;
  
  constructor(
    private toastr: ToastrService, 
    private locationsService: LocationsService,
    private buildersService: BuildersService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.buildersForm = this.formBuilder.group({
      id: [null],
      esCliente: [false],
      nombre: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      nombreContacto: [null],
      identificacion: [null, [Validators.required]],
      identificacionContacto: [null],
      idProvincia: [null, [Validators.required]],
      idLocalidad: [null, [Validators.required]],
      direccion: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(500)]],
      codigoPostal: [null],
      email: [null],
      telefono: [null],
      emailContacto: [null],
      telefonoContacto: [null],
      observaciones: [null, [Validators.maxLength(500)]],
    });
   
    if (this.idBuilders != null) {
      this.edicion = true;
      this.obtenerBuilders();
    }
    // this.obtenerLocations();
    this.obtenerProvincias();
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  obtenerBuilders() {
    this.buildersService.ObtenerPorId(this.idBuilders).subscribe(
      data => {
        this.obtenerPatchBuilder(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  obtenerProvincias() {
    this.locationsService.ListarProvinciasCombo().subscribe(
      data => {
        this.provincias = data;
      },
      error => {
        // this.toastr.error(error);
        this.toastr.error(error.mensaje);
      }
    );
  }

  obtenerLocations() {
    this.locationsService.ListarLocalidadesCombo(this.idProvincia).subscribe(
      data => {
        this.locations = data;
      },
      error => {
        // this.toastr.error(error);
        this.toastr.error(error.mensaje);
      }
    );
  }

  get f() { return this.buildersForm.controls; }
  add() {
    console.log('this.buildersForm: ', this.buildersForm);
    this.submitted = true;
    if (this.buildersForm.invalid) {
      return;
    }

    this.builders = this.buildersForm.value;
    console.log(this.buildersForm.value instanceof Builder);
    
    this.reloadStart();
    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }

  }
  create(){
    let buildersTem = new Builder();
    buildersTem = this.changeObject(this.builders);
    console.log(buildersTem);
   
    this.buildersService.Crear(buildersTem).subscribe(
      data => {
        console.log('Crear: ',data);
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        this.reloadStop();
        // this.getAllData();
      },
      error => {
        this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  edit(){
    let buildersTem = new Builder();
    buildersTem = this.changeObject(this.builders);

    this.buildersService.Editar(buildersTem).subscribe(
      data => {
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
        this.reloadStop();
      },
      error => {
        this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  reiniciarFormulario() {
    this.builders = new Builder();
    this.buildersForm.reset();
    this.submitted = false;
  }
  obtenerPatchBuilder(data){
    this.builders = this.changeObject(data);
    this.buildersForm.patchValue({
      id: this.builders.id,
      esCliente: false,
      nombre: this.builders.nombre,
      nombreContacto: this.builders.nombreContacto,
      identificacion: this.builders.identificacion,
      identificacionContacto: this.builders.identificacionContacto,
      email: this.builders.email,
      emailContacto: this.builders.emailContacto,
      telefono: this.builders.telefono,
      telefonoContacto: this.builders.telefonoContacto,
      direccion: this.builders.direccion,
      codigoPostal: this.builders.codigoPostal,
      idLocalidad: this.builders.idLocalidad,
      idProvincia: this.builders.idProvincia,
      observaciones: this.builders.observaciones,
    });
  }
  changeObject(builders: Builder) {
    let buildersTem: Builder = new Builder();
    buildersTem.id = builders.id;
    buildersTem.esCliente = builders.esCliente;
    buildersTem.nombre = builders.nombre;
    buildersTem.nombreContacto = builders.nombreContacto;
    buildersTem.identificacion = builders.identificacion;
    buildersTem.identificacionContacto = builders.identificacionContacto;
    buildersTem.email = builders.email == "" ? null : builders.email;
    buildersTem.emailContacto = builders.emailContacto == "" ? null : builders.emailContacto;
    buildersTem.telefono = builders.telefono == "" ? null : builders.telefono,
    buildersTem.telefonoContacto = builders.telefonoContacto == "" ? null : builders.telefonoContacto,
    buildersTem.direccion = builders.direccion;
    buildersTem.codigoPostal = builders.codigoPostal;
    buildersTem.observaciones = builders.observaciones;
    buildersTem.idLocalidad = builders.idLocalidad;
    buildersTem.idProvincia = this.idProvincia = builders.idProvincia;

    this.changeProvincia();
    return buildersTem;
  }
  addLocation(modal){
    this.cargaCompleta = true;
    this.locationsService.addModelLocation(modal);
  }
  onSetModal(e) {
    this.obtenerLocations();
  }
  
  changeProvincia() {
    if(this.idProvincia != null) {
      this.obtenerLocations();
    } else {
      this.idLocalidad = null;
    }
  }
}
