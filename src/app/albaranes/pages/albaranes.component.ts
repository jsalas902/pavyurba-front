import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { AlbaranesService } from '@app/albaranes/services/albaranes.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Albaran } from '@app/albaranes/models/Albaran';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { Pagination } from '@app/_models/util/Pagination';
import * as moment from 'moment';
import { ObrasService } from '@app/obras/services/obras.service';
import { Provider } from '../../providers/models/Provider';
import { Material } from '../../materials/models/Material';
import { Obra } from '@app/obras/models/Obra';
import { MaterialsService } from '@app/materials/services/materials.service';
import { ProvidersService } from '@app/providers/services/providers.service';

@Component({
  selector: 'app-albaranes',
  templateUrl: './albaranes.component.html',
  styleUrls: ['./albaranes.component.css']
})
export class AlbaranesComponent implements OnInit {
  @BlockUI('componentsAlbaranes') blockUIList: NgBlockUI;

  @Input() idAlbaranesListar: string;

  rows: Albaran[];
  rowsFiltered: Albaran[];
  allRowsSelected: boolean;
  pagination: Pagination;
  listaAlbaranes: Albaran[];
  cargaCompleta = false;
  titelModal: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  facturado: string = null;
  totalAlbaran: string;
  totalCantidad: string;
  validTotalAlbaran: boolean = false;
  idObra: number;  
  idProveedor: number;  
  idMaterial: number; 
  obras: Obra;
  providers: Provider;
  materials: Material; 
  facturas: any[] = [{id: "true", texto: "Facturado"}, {id:"false", texto: "No Facturado"}]; 
  private modalRef;

  constructor(private toastr: ToastrService,
              private albaranesService: AlbaranesService,
              private obrasService: ObrasService,
              private materialsService: MaterialsService,
              private providersService: ProvidersService,
              private confirmDialogService: ConfirmDialogService,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private modalService: NgbModal
              ) {
                this.obtenerObras();
                this.obtenerProveedores();
                this.obtenerMateriales();
}


  ngOnInit(): void {
    this.allRowsSelected = false;

    this.titelModal = 'Crear Albarán';

    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = '';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;

    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    // this.getAllData();
  }

  ngAfterViewInit(): void {
    this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    var bodyAlbaranes = {
      idObra: this.idObra,
      idProveedor: this.idProveedor,
      idMaterial: this.idMaterial,
      facturado: this.facturado,
    };

    this.reloadStart();
    this.albaranesService.ListarTodos(this.skip, this.take, this.query, bodyAlbaranes).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalAlbaran = Number(data.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
        this.totalCantidad = Number(data.totalCantidad).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
        this.validTotalAlbaran = (data.total > 0) ? true : false;
        var list =  data.data;
        if(list.length > 0) {
          list.forEach(element => {
            // moment.locale('es');
            element.fechaRecepcion = moment(element.fechaRecepcion).format('DD-MM-YYYY');
            element.cantidad = (element.cantidad != null) ? Number(element.cantidad).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.cantidad;      
            element.precio = (element.precio != null) ? Number(element.precio).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.precio;      
            element.total = (element.total != null) ? Number(element.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.total;
            // element.facturado = element.facturado ? "SI" : "PENDIENTE";
          });
        }
        console.log('lista:',data);
        this.listaAlbaranes = list;
        this.listaAlbaranes = [...this.listaAlbaranes];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaAlbaranes;
    this.rowsFiltered = this.listaAlbaranes;
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, row: Albaran) {
    // this.idAlbaranesListar = row !== null ? row.id.toString() : null;
    if(row !== null){
      this.idAlbaranesListar =  row.id.toString();
      // this.edicion = true;
      this.titelModal = 'Editar Albarán';
    }else{
      this.idAlbaranesListar =  null;
      // this.size = 'xll'; // Necesario para setear el size del modal  
      // this.edicion = false;
      this.titelModal = 'Crear Albarán';  // Necesario para setear el titulo   
    }
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown' });
  }
  onCloseModal(e) {
    this.modalRef.close();
    this.getAllData();
  }
  onChange($event, id: string) {
    this.confirmDialogService.confirmThis(`Seguro quiere realizar esta acción en el registro N° ${id} ?`, () =>  {
      // let result = $event ? this.activarAlbaranes(id) : this.desactivarAlbaranes(id);
      let result = $event ? this.facturarAlbaranes(id) : this.toastr.error(`El Albarán N° ${id} ya ha sido facturado`);
      this.getAllData();
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario registro N° ${id}`);
      this.getAllData();
    });
    
  }
  activarAlbaranes(id: string) {
    this.albaranesService.Activar(id).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Habilitado' , { timeOut: 2500, closeButton: true });
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  desactivarAlbaranes(id: string) {
    this.albaranesService.Desctivar(id).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Desactivado' , { timeOut: 2500, closeButton: true });
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }  
  facturarAlbaranes(id: string) {
    this.albaranesService.Facturar(id).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Facturado' , { timeOut: 2500, closeButton: true });
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  downloadFile(row) {
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = row.archivo.url;
    a.target = "_blank";
    a.download = "TEST";
    a.click();
    a.remove();
    /*this.albaranesService.DownloadFile(row.archivo.url).subscribe(
      res => {
        console.log('res: ', res)
        // let url = window.URL.createObjectURL(res.data);
        // let a = document.createElement('a');
        // document.body.appendChild(a);
        // a.setAttribute('style', 'display: none');
        // a.href = url;
        // a.download = res.filename;
        // a.click();
        // window.URL.revokeObjectURL(url);
        // a.remove();
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );*/
  }
  
  searchFilter(e){
    var bodyAlbaranes = {
      idObra: this.idObra,
      idProveedor: this.idProveedor,
      idMaterial: this.idMaterial,
      facturado: this.facturado,
    };

    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.albaranesService.ListarTodos(this.skip, this.take, this.query, bodyAlbaranes).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalAlbaran = Number(data.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
        this.totalCantidad = Number(data.totalCantidad).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
        this.validTotalAlbaran = (data.total > 0) ? true : false;
        var list =  data.data;
        if(list.length > 0) {
          list.forEach(element => {
            // moment.locale('es');
            element.fechaRecepcion = moment(element.fechaRecepcion).format('DD-MM-YYYY');
            element.cantidad = (element.cantidad != null) ? Number(element.cantidad).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.cantidad;      
            element.precio = (element.precio != null) ? Number(element.precio).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.precio;      
            element.total = (element.total != null) ? Number(element.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.total;
            // element.facturado = element.facturado ? "SI" : "PENDIENTE";
          });
        }
        console.log('lista:',data);
        this.listaAlbaranes = list;
        // console.log('lista:',data);
        // this.listaAlbaranes = data.data;
        this.listaAlbaranes = [...this.listaAlbaranes];

        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  onSetFacturado (e) {
    // this.isFacturado = !this.isFacturado;
    this.facturado = e != null ? e.id : null;
    
    this.getAllData();
  }

  setObras(e) {
    this.getAllData();
  }

  setProveedores(e) {
    this.getAllData();
  }

  setMateriales(e) {
    this.getAllData();
  }

  async obtenerObras() {
    // this.business = await this.businessService.ListarEmpresasCombo().toPromise();
    this.obrasService.ListarObrasCombo().subscribe(
      data => {
        this.obras = data;
        // this.idObra = (data[0]) ? data[0].id : null
        // this.getAllData();
      },
      error => {
        this.toastr.error(error);
      }
    );
  }

  async obtenerProveedores() {
    // this.business = await this.businessService.ListarEmpresasCombo().toPromise();
    this.providersService.ListarProveedoresCombo().subscribe(
      data => {
        this.providers = data;
        // this.idProveedor = (data[0]) ? data[0].id : null
        // this.getAllData();
      },
      error => {
        this.toastr.error(error);
      }
    );
  }

  async obtenerMateriales() {
    // this.business = await this.businessService.ListarEmpresasCombo().toPromise();
    this.materialsService.ListarMaterialesCombo().subscribe(
      data => {
        this.materials = data;
        // this.idMaterial = (data[0]) ? data[0].id : null
        this.getAllData();
      },
      error => {
        this.toastr.error(error);
      }
    );
  }

  removeRegistro(row: any) {
    this.confirmDialogService.confirmThis(`Está seguro desea eliminar el albarán seleccionado?`, () =>  {
      this.delete(row);      
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario`);
    });
  }

  delete(row: any) {
    this.reloadStart();
    this.albaranesService.Eliminar(row.id.toString()).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Eliminado' , { timeOut: 2500, closeButton: true });
        this.getAllData();        
      },
      error => {
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        this.reloadStop();
    });
    
  }
}