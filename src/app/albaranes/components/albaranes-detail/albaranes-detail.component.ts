import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbDateParserFormatter, NgbDatepickerI18n, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { Albaran } from '@app/albaranes/models/Albaran';
import { Provider } from '@app/providers/models/Provider';
import { Material } from '@app/materials/models/Material';
import { Obra } from '@app/obras/models/Obra'
import { ProvidersService } from '@app/providers/services/providers.service';
import { MaterialsService } from '@app/materials/services/materials.service';
import { ObrasService } from '@app/obras/services/obras.service';
import { AlbaranesService } from '@app/albaranes/services/albaranes.service';
import { AlbaranesValidator } from "@app/albaranes/validation/albaranes.validator";

import { NgbDateCustomParserFormatter} from '@app/_helpers/dateformat';
import { CustomDatepickerI18nService, I18n } from '@app/calendarios/services/custom-datepicker-i18n.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-albaranes-detail',
  templateUrl: './albaranes-detail.component.html',
  styleUrls: ['./albaranes-detail.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18nService }
  ]
})
export class AlbaranesDetailComponent implements OnInit {

  @BlockUI('componentsAlbaranesDetail') blockUIList: NgBlockUI;
  @Input() idAlbaranes: string;
  @Output() closeModal = new EventEmitter<boolean>();
  @ViewChild('labelImport', { static: true })
  labelImport: ElementRef;
  fileToUpload: File = null;

  albaranes: Albaran = new Albaran();
  providers: Provider;
  materials: any[];
  obras: Obra;
  albaranesForm: FormGroup;
  titleDocumento: string;
  sizeArchivo: number;
  typeArchivo: string;
  baseArchivo: string | ArrayBuffer;
  submitted: boolean = false;
  uploadFile: boolean = false;
  edicion: boolean = false;
  archivoValid: boolean = false;
  fechaRecepcion;
  mensaje: string = '';
  maxLength: number = 500;
  idMaterial: string = null;
  montoTotal: number;
  cantidad: number;
  precio: number;
  // fechaFacturacion;
  d: any;
  d1: any;
  readonly DELIMITER = '-';
  albaranes_validation_messages = AlbaranesValidator.ALBARANES_VALIDATION_MESSAGES;

  
  constructor(
    private toastr: ToastrService,      
    private providersService: ProvidersService,
    private materialsService: MaterialsService,
    private obrasService: ObrasService,
    private albaranesService: AlbaranesService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.titleDocumento = "Subir Documento";
    this.albaranesForm = this.formBuilder.group({
      id: [null],
      numero: [null, [Validators.required]],
      cantidad: [null, [Validators.required]],
      recibido: [false],
      facturado: [false],
      observaciones: [null, [Validators.maxLength(500)]],
      idProveedor: [null, [Validators.required]],
      idMaterial: [null, [Validators.required]],
      idObra: [null, [Validators.required]],
      fechaRecepcion: [null, [Validators.required]],
      precio: [null, [Validators.required]],
      montoTotal: [null],
      // fechaFacturacion: [null, [Validators.required]],
      archivo: [null],
    });

    if (this.idAlbaranes != null) {
      this.edicion = true;
      this.obtenerAlbaran();
    }
    this.obtenerProviders();
    this.obtenerMaterials();
    this.obtenerObras();
  
  }

  obtenerAlbaran() {
    this.albaranesService.ObtenerPorId(this.idAlbaranes).subscribe(
      data => {
        this.obtenerPatchAlbaran(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  obtenerMaterials() {
    this.materialsService.ListarMaterialesCombo().subscribe(
      data => {
        this.materials = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  obtenerProviders() {
    this.providersService.ListarProveedoresCombo().subscribe(
      data => {
        this.providers = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  obtenerObras() {
    this.obrasService.ListarObrasCombo().subscribe(
      data => {
        this.obras = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  get f() { return this.albaranesForm.controls; }
  add() {
    this.submitted = true;

    if (this.albaranesForm.invalid) {
      return;
    } else {
      if(this.uploadFile) {
        this.submitted = false;
        this.uploadFile = false;
        return;
      }
    }
    
    this.albaranes = this.albaranesForm.value;
    
    // console.log(this.albaranesForm.value instanceof Albaran);

    // if((this.albaranes.archivo == null) && !this.edicion){
    //   this.archivoValid = true;
    //   var fechaNew = this.albaranes.fechaRecepcion;
    //   this.albaranes.fechaRecepcion = "";
    //   this.albaranes.fechaRecepcion = this.toModelFix(this.fechaRecepcion);
    //   return
    // } else {
    //   this.archivoValid = false
    // }
    this.reloadStart();
    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }

  }
  create(){
    let albaranesTem = new Albaran();

    this.albaranes.fechaRecepcion = (this.fechaRecepcion == undefined || this.fechaRecepcion == "" || this.fechaRecepcion == null) ? null : this.toModel(this.fechaRecepcion);
    albaranesTem = this.changeObject(this.albaranes);
    albaranesTem.fechaRecepcion = (this.fechaRecepcion == undefined || this.fechaRecepcion == "" || this.fechaRecepcion == null) ? null : this.toModel(this.fechaRecepcion);
    
    // console.log(this.fechaFacturacion)
    // albaranesTem.fechaRecepcion = this.toModel(this.fechaRecepcion);
    // albaranesTem.fechaFacturacion = this.toModel(this.fechaFacturacion);
    // albaranesTem.fechaFacturacion = (albaranesTem.fechaFacturacion != 'Invalid date') ? albaranesTem.fechaFacturacion : null;
    
    // this.albaranes.fechaRecepcion = "";
    // this.albaranes.fechaRecepcion = this.toModelFix(this.fechaRecepcion);

    this.albaranesService.Crear(albaranesTem).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
        this.reloadStop();
        this.closeModal.emit(true);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        this.reloadStop();
    });
  }

  edit(){
    let albaranesTem = new Albaran();

    this.albaranes.fechaRecepcion = (this.fechaRecepcion == undefined || this.fechaRecepcion == "" || this.fechaRecepcion == null) ? null : this.toModel(this.fechaRecepcion);
    albaranesTem = this.changeObject(this.albaranes);
    albaranesTem.fechaRecepcion = (this.fechaRecepcion == undefined || this.fechaRecepcion == "" || this.fechaRecepcion == null) ? null : this.toModel(this.fechaRecepcion);
    // albaranesTem = this.changeObject(this.albaranes);
    // albaranesTem.fechaRecepcion = this.toModel(this.fechaRecepcion);
    // albaranesTem.fechaFacturacion = this.toModel(this.fechaFacturacion);

    // this.albaranes.fechaRecepcion = "";
    // this.albaranes.fechaRecepcion = this.toModelFix(this.fechaRecepcion);

    this.albaranesService.Editar(albaranesTem).subscribe(
      data => {
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
        this.reloadStop();
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        this.reloadStop();
    });
  }

  reiniciarFormulario() {
    this.albaranes = new Albaran();
    this.albaranesForm.reset();
    this.submitted = false;
  }

  obtenerPatchAlbaran(data){
    this.albaranes = this.changeObject(data);
    this.albaranesForm.patchValue({
      id: this.albaranes.id,
      numero: this.albaranes.numero,
      cantidad: this.albaranes.cantidad,
      recibido: this.albaranes.recibido,
      facturado: this.albaranes.facturado,
      // fechaRecepcion: this.fechaRecepcion = this.fromModel(this.albaranes.fechaRecepcion),
      // fechaFacturacion: this.fechaFacturacion = this.fromModel(this.albaranes.fechaFacturacion),
      idProveedor: this.albaranes.idProveedor,
      idMaterial: this.albaranes.idMaterial,
      idObra: this.albaranes.idObra,
      observaciones: this.albaranes.observaciones,
      precio: this.albaranes.precio,
      // archivo: this.albaranes.archivo,
    });

    this.changeCantidad();
  }
  changeObject(albaranes: Albaran) {
    let albaranesTem: Albaran = new Albaran();
    albaranesTem.id = (albaranes.id != null) ? albaranes.id : 0;
    albaranesTem.numero = albaranes.numero;
    albaranesTem.cantidad = albaranes.cantidad;
    albaranesTem.recibido = albaranes.recibido;
    albaranesTem.facturado = albaranes.facturado;
    // albaranesTem.fechaRecepcion = albaranes.fechaRecepcion;
    albaranesTem.fechaRecepcion = this.fechaRecepcion = (albaranes.fechaRecepcion !== null) ? this.fromModel(albaranes.fechaRecepcion) : null;
    // albaranesTem.fechaFacturacion = albaranes.fechaFacturacion;
    albaranesTem.idProveedor = albaranes.idProveedor;
    albaranesTem.idMaterial = albaranes.idMaterial;
    albaranesTem.idObra = albaranes.idObra;
    albaranesTem.observaciones = albaranes.observaciones;
    albaranesTem.precio = albaranes.precio;
    // albaranesTem.archivo = (albaranes.archivo != null && albaranes.archivo != undefined);
    if(albaranes.archivo != null && albaranes.archivo != undefined){
      albaranesTem.archivo = {
        "base64": this.baseArchivo,
        "nombre": this.titleDocumento,
        "mimeType": this.typeArchivo,
        "tamano": this.sizeArchivo  
      };
    } else {
      albaranesTem.archivo = null;
      // albaranesTem.archivo = {
      //   "base64": "",
      //   "nombre": "",
      //   "mimeType": "",
      //   "tamano": 0  
      // };
    }
    return albaranesTem;
  }
  toModel(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format();
  }
  toModelFix(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format('DD-MM-YYYY');
  }
  onFileChange(event: any) {
    this.fileToUpload  = event.target.files.item(0);

    var reader = new FileReader();
    reader.onload = this.convertFileSimple.bind(this, this.fileToUpload);
    reader.readAsBinaryString(this.fileToUpload);
  }

  convertFileSimple(file: File, event: any) {
    const binaryString = event.target.result;
    this.titleDocumento = file.name;
    this.sizeArchivo = file.size;
    this.typeArchivo = file.type;
    this.baseArchivo = btoa(binaryString);
    this.archivoValid = false
    this.uploadFile = false;
  }

  document() {
    this.uploadFile = true;
    let element: HTMLElement = document.getElementById('archivo') as HTMLElement;
    element.click();
  }
  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[2], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[0], 10)
      };
    }
    return null;
  }
  changeMaterial(){
    this.materials.forEach(element => {
      if(element.id == this.idMaterial){
        this.albaranesForm.patchValue({
          precio: element.parametro
          // valor: (this.fueraMadrid) ? this.valorFM : this.valorDM,
        });
      }
    });
  }
  changeCantidad(){
    this.montoTotal = this.precio * this.cantidad;
  }
  
  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }
}