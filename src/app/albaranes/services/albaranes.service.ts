import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { GenericService, AuthenticationService } from '@app/_services';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Pagination } from '@app/_models/util/Pagination';
import { Albaran } from '@app/albaranes/models/Albaran';

@Injectable({
  providedIn: 'root'
})
export class AlbaranesService  extends GenericService {

  constructor(private http: HttpClient,
    public authService: AuthenticationService,
    private injector: Injector) {
  super();
  }

  ListarTodos(skip: string, take: string, query: string, bodyAlbaranes: any): Observable<Pagination> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.post(
      environment.apiUrl + '/albaranes/listarPaginado' + '?skip=' + skip + '&take=' + take + '&query=' + query, bodyAlbaranes, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta.data;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }
  ListarPorObras(idObra: string, skip: string, take: string, query: string, bodyAlbaranes: any): Observable<Pagination> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.post(
      environment.apiUrl + '/albaranes/listarPorObraPaginado/' + idObra + '?skip=' + skip + '&take=' + take + '&query=' + query, bodyAlbaranes, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta.data;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }
  ListarAlbaranesCombo(): Observable<any> {
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/maestros/listarAlbaranesCombo', { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta.data;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  ObtenerPorId(id: string): Observable<Albaran> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.get(environment.apiUrl + '/albaranes/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta.data;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }


  Crear(albarane: Albaran): Observable<any> {
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));

    return this.http.post(environment.apiUrl + '/albaranes/crearAlbaran', albarane, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  Editar(albarane: Albaran): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.put(environment.apiUrl + '/albaranes/' + albarane.id, albarane, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  EditarPorObra(albaran: Albaran): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.put(environment.apiUrl + '/albaranes/actualizarAlbaranPorObra/' + albaran.id, albaran, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  Eliminar(id: string): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.delete(environment.apiUrl + '/albaranes/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  Activar(id: string): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.put(environment.apiUrl + '/albaranes/activarAlbaran/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  Desctivar(id: string): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.put(environment.apiUrl + '/albaranes/desactivarAlbaran/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  Facturar(id: string): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.put(environment.apiUrl + '/albaranes/facturarAlbaran/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  DownloadFile(url: string): Observable<any> {

    this.headers = new HttpHeaders ({
      "Access-Control-Allow-Headers" : "*",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
    });

    return this.http.get(url, { headers: this.headers, responseType: 'blob' })
    .pipe(
      map(
        (respuesta: any) => {
          console.log('respuesta: ', respuesta)
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }
}