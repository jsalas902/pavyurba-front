export class AlbaranesValidator {
    public static ALBARANES_VALIDATION_MESSAGES = {
        'archivo': [
            { message: 'Archivo es requerido' },
        ],        
        'numero': [
            { type: 'required', message: 'Numero es requerido' },
        ],        
        'cantidad': [
            { type: 'required', message: 'Cantidad es requerido' },
        ],       
        'precio': [
            { type: 'required', message: 'Precio es requerido' },
        ],
        'fechaRecepcion': [
            { type: 'required', message: 'Fecha recepción es requerido' },
        ],
        // 'fechaFacturacion': [
        //     { type: 'required', message: 'Fecha facturación es requerido' },
        // ],
        'observaciones': [
            { type: 'maxlength', message: 'Las observaciones no pueden tener más de 500 caracteres' }
        ],
        'idMaterial': [
            { type: 'required', message: 'Material es requerido' },
        ],
        'idProveedor': [
            { type: 'required', message: 'Proveedor es requerido' },
        ],
        'idObra': [
            { type: 'required', message: 'Obra es requerido' },
        ],
    }
}