export class Albaran {

    public constructor(init?: Partial<Albaran>) {
        Object.assign(this, init);
    }

    id: number;
    numero: number;
    cantidad: number;
    precio: number;
    observaciones: string;
    recibido: boolean;
    facturado: boolean;
    fechaRecepcion: any;
    // fechaFacturacion: string;
    archivo: any;
    idProveedor: string;
    proveedor: string;
    material: string;
    idMaterial: number ;
    idObra: number ;
    obra: string;
    habilitado: boolean;
    fechaHoraLog: Date;
    usuarioLog: string ;

}