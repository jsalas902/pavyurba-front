import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { GenericService, AuthenticationService } from '@app/_services';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Pagination } from '@app/_models';
import { Hucha } from '@app/huchas/models/Huchas';

@Injectable({
  providedIn: 'root'
})
export class HuchasService extends GenericService{
  private modalRef;

  constructor(private http: HttpClient,
    public authService: AuthenticationService,
    private injector: Injector,
    private modalService: NgbModal) {
  super();
  }

  ListarTodos(skip: string, take: string, query: string): Observable<Pagination> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/hucha' + '?skip=' + skip + '&take=' + take + '&query=' + query, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  ListarPorEmpleado(idEmpleado: string, skip: string, take: string, query: string): Observable<Pagination> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/hucha/listarEmpleadosHuchaPaginado/' + Number(idEmpleado) + '?skip=' + skip + '&take=' + take + '&query=' + query, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta.data;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  ObtenerPorId(id: string): Observable<Location> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.get(environment.apiUrl + '/hucha/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta.data;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }


  Crear(huchas: any): Observable<any> {

    // this.headers = new HttpHeaders ({
    //   Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    // });
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));

    return this.http.post(environment.apiUrl + '/hucha', huchas, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  Editar(huchas: any): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.put(environment.apiUrl + '/hucha/' + huchas.id, huchas, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  addModelHucha(modal: any) {
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: 'sm' });
  }
  onCloseModal() {
    this.modalRef.close();
  }

  Eliminar(id: string): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.delete(environment.apiUrl + '/hucha/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }
}