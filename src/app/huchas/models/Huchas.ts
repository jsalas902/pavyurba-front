export class Hucha {

    public constructor(init?: Partial<Hucha>) {
        Object.assign(this, init);
    }

    id: number;
    valor: number;
    fechaHoraLog: Date;
    usuarioLog: string ;
    observaciones: string;

}