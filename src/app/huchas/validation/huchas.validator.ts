export class HuchasValidator {
    public static HUCHAS_VALIDATION_MESSAGES = {        
        'observaciones': [
            { type: 'required', message: 'Observaciones es requerido' },
        ],
        'valor': [
            { type: 'required', message: 'Hucha es requerido' },
        ],
    }
}