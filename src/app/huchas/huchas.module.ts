import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HuchasDetailComponent } from './components/huchas-detail/huchas-detail.component';
import { HuchasComponent } from './pages/huchas.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxDatatableFooterModule } from 'ngx-datatable-footer';
import { NgSelectModule } from '@ng-select/ng-select';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';
import { CardModule } from '../content/partials/general/card/card.module';
import { BreadcrumbModule } from '../_layout/breadcrumb/breadcrumb.module';
import { BlockUIModule } from 'ng-block-ui';
import { BlockTemplateComponent } from '../_layout/blockui/block-template.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedComponentsModule } from '../shared/components/shared-components.module';
import { ModalHuchasDetailComponent } from './components/modal-huchas-detail/modal-huchas-detail.component';



@NgModule({
  declarations: [HuchasComponent, HuchasDetailComponent, ModalHuchasDetailComponent],
  exports: [HuchasComponent, HuchasDetailComponent, ModalHuchasDetailComponent],
  imports: [
    CommonModule,
    CardModule,
    NgbModule,
    BreadcrumbModule,
    NgSelectModule,
    UiSwitchModule,
    FormsModule,
    ReactiveFormsModule,
    ClipboardModule,
    PerfectScrollbarModule,
    NgxDatatableModule,
    BlockUIModule.forRoot({
      template: BlockTemplateComponent
    }),
    NgxDatatableFooterModule.forRoot(),
    SharedComponentsModule,
  ]
})
export class HuchasModule { }
