import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Hucha } from '@app/huchas/models/Huchas';

import { HuchasService } from '@app/huchas/services/huchas.service';
import { HuchasValidator } from "@app/huchas/validation/huchas.validator";

@Component({
  selector: 'app-huchas-detail',
  templateUrl: './huchas-detail.component.html',
  styleUrls: ['./huchas-detail.component.css']
})
export class HuchasDetailComponent implements OnInit {

  @Input() idEmployees: string;
  @Input() idHucha: string;
  @Input() dato: number;
  @Output() closeModal = new EventEmitter<boolean>();

  huchas: Hucha = new Hucha();
  huchasForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  huchas_validation_messages = HuchasValidator.HUCHAS_VALIDATION_MESSAGES;
  
  mensaje: string = '';
  maxLength: number = 500;

  
  constructor(
    private toastr: ToastrService, 
    private huchasService: HuchasService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.huchasForm = this.formBuilder.group({
      id: [null],
      valor: [null, [Validators.required]],
      observaciones: [null, [Validators.required]],
    });

    if (this.idHucha != null) {
      this.edicion = true;
      this.obtenerHuchaPorId();
    }
    // this.obtenerLocations();
  }

  // obtenerLocations() {
  //   this.huchasService.ObtenerPorId(this.idEmployees).subscribe(
  //     data => {
  //       this.obtenerPatchLocation(data);
  //     },
  //     error => {
  //       // this.toastr.error(error.mensaje);
  //       this.toastr.error(error);
  //     }
  //   );
  // }

  get f() { return this.huchasForm.controls; }
  add() {
    this.submitted = true;

    if (this.huchasForm.invalid) {
      return;
    }
    
    this.huchas = this.huchasForm.value;
    
    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }

  }
  create(){
    let huchasTem = new Hucha();
    huchasTem = this.changeObject(this.huchas);
    // huchasTem.valor = huchasTem.valor * this.dato;
    // huchasTem.observaciones = huchasTem.observaciones;
    let huchasData = {
      "idEmpleado": this.idEmployees,
      "valor": (huchasTem.valor * this.dato),
      "observaciones": huchasTem.observaciones,
    }
   
    this.huchasService.Crear(huchasData).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
        this.closeModal.emit(false);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  edit(){
    let huchasTem = new Hucha();
    huchasTem = this.changeObject(this.huchas);
    // huchasTem.valor = huchasTem.valor * this.dato;
    // huchasTem.observaciones = huchasTem.observaciones;
    let huchasData = {
      "id": this.idHucha,
      "idEmpleado": this.idEmployees,
      "valor": huchasTem.valor,
      "observaciones": huchasTem.observaciones,
    }

    this.huchasService.Editar(huchasData).subscribe(
      data => {
        this.closeModal.emit(false);
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  reiniciarFormulario() {
    this.huchas = new Hucha();
    this.huchasForm.reset();
    this.submitted = false;
  }

  obtenerPatchHuchas(data){
    this.huchas = this.changeObject(data);
    this.huchasForm.patchValue({
      id: this.huchas.id,
      valor: this.huchas.valor,
      observaciones: this.huchas.observaciones,
    });
  }
  obtenerHuchaPorId(){
    this.huchasService.ObtenerPorId(this.idHucha).subscribe(
      data => {
        this.obtenerPatchHuchas(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  changeObject(huchas: Hucha) {
    let huchasTem: Hucha = new Hucha();
    huchasTem.id = huchas.id;
    huchasTem.valor = huchas.valor;
    huchasTem.observaciones = huchas.observaciones;
    return huchasTem;
  }
  

}
