import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HuchasService } from '@app/huchas/services/huchas.service';

@Component({
  selector: 'app-modal-huchas-detail',
  templateUrl: './modal-huchas-detail.component.html',
  styleUrls: ['./modal-huchas-detail.component.css']
})
export class ModalHuchasDetailComponent implements OnInit {
  @Input() idEmployees: string;
  @Input() idHucha: string;
  @Input() titel: string;
  @Input() dato: number;
  @Output() setModal = new EventEmitter<boolean>();
  cargaCompletaModal:boolean;

  constructor(private modalService: NgbModal,private huchasService: HuchasService) { }

  ngOnInit(): void {
    this.cargaCompletaModal = false;
  }
  
  ngAfterViewInit(): void {
    this.cargaCompletaModal = true;
  }

  onCloseModal(e) {
    if (!e) this.setModal.emit(true);
        
    this.huchasService.onCloseModal();
  }
}
