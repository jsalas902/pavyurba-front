import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { HuchasService } from '@app/huchas/services/huchas.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Hucha } from '@app/huchas/models/Huchas';
import * as moment from 'moment';

import { Router } from '@angular/router';
import { Pagination } from '@app/_models';

@Component({
  selector: 'app-huchas',
  templateUrl: './huchas.component.html',
  styleUrls: ['./huchas.component.css']
})
export class HuchasComponent implements OnInit {

  @BlockUI('componentsHuchas') blockUIList: NgBlockUI;

  @Input() idEmployees: string;  


  
  rows: Hucha[];
  rowsFiltered: Hucha[];
  
  pagination: Pagination;
  listaHuchas: Hucha[];
  cargaCompleta = false;
  titelModal: string;
  dato: number;
  type: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  idHucha: string;
  totalHucha: string;
  validTotalHucha: boolean = false;
  private modalRef;

  constructor(private toastr: ToastrService,
              private huchasService: HuchasService,
              private confirmDialogService: ConfirmDialogService,
              private router: Router,
              private cdRef: ChangeDetectorRef
              
              ) {
}


  ngOnInit(): void {

    this.titelModal = 'Crear Huchas';

    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = '';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;

    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    this.getAllData();
  }

  ngAfterViewInit(): void {
    // this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();
    this.huchasService.ListarPorEmpleado(this.idEmployees, this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalHucha = Number(data.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
        this.validTotalHucha = (data.total > 0) ? true : false;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
          element.valor = (element.valor != null) ? Number(element.valor).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.valor;
        });
        this.listaHuchas = data.data;
        this.listaHuchas = [...this.listaHuchas];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaHuchas;
    this.rowsFiltered = this.listaHuchas;
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, accion: string, row: any) {
    this.idEmployees = this.idEmployees;
    this.titelModal = accion !== null ? `${accion} Hucha` : `Editar Hucha`;
    this.cargaCompleta = true;
    this.dato = (accion == "Pagar") ? 1 : -1;
    this.idHucha = row !== null ? row.id.toString() : null;
    this.huchasService.addModelHucha(modal);
  }
  onSetModal(e) {
    this.getAllData();
  }

  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.huchasService.ListarPorEmpleado(this.idEmployees, this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalHucha = Number(data.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
        this.validTotalHucha = (data.total > 0) ? true : false;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
          element.valor = (element.valor != null) ? Number(element.valor).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.valor;
        });
        this.listaHuchas = data.data;
        this.listaHuchas = [...this.listaHuchas];

        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  removeRegistro(row: any) {
    this.confirmDialogService.confirmThis(`Está seguro desea eliminar la hucha seleccionada?`, () =>  {
      this.delete(row);      
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario`);
    });
  }

  delete(row: any) {
    this.reloadStart();
    this.huchasService.Eliminar(row.id.toString()).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Eliminado' , { timeOut: 2500, closeButton: true });
        this.getAllData();        
      },
      error => {
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        this.reloadStop();
    });
    
  }
}
