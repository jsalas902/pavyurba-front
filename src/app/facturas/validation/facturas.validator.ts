export class FacturasValidator {
    public static FACTURAS_VALIDATION_MESSAGES = {        
        'tipo': [
            { type: 'required', message: 'Tipo es requerido' },
        ],
        'numero': [
            { type: 'required', message: 'Número de factura es requerido' },
        ],
        'total': [
            { type: 'required', message: 'Importe es requerido' },
        ],
        'ano': [
            { type: 'required', message: 'Año es requerido' },
        ],
        'mes': [
            { type: 'required', message: 'Mes es requerido' },
        ],
        'estatus': [
            { type: 'required', message: 'Estatus es requerido' },
        ],
        'archivo': [
            { type: 'required', message: 'La factura es requerida' },
        ],
    }
}