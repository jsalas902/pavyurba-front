export class FacturaTypes {

    public constructor(init?: Partial<FacturaTypes>) {
      Object.assign(this, init);
      FacturaTypes.PROFORMA = 'PROFORMA';
      FacturaTypes.FACTURA = 'FACTURA';
      FacturaTypes.SUBIDA_CLIENTE = 'SUBIDA CLIENTE';
      FacturaTypes.SUBIDA_EMPRESA = 'SUBIDA EMPRESA';
      FacturaTypes.FINALIZADO = 'FINALIZADO';

      FacturaTypes.MES1 = 'ENERO';
      FacturaTypes.MES2 = 'FEBRERO';
      FacturaTypes.MES3 = 'MARZO';
      FacturaTypes.MES4 = 'ABRIL';
      FacturaTypes.MES5 = 'MAYO';
      FacturaTypes.MES6 = 'JUNIO';
      FacturaTypes.MES7 = 'JULIO';
      FacturaTypes.MES8 = 'AGOSTO';
      FacturaTypes.MES9 = 'SEPTIEMBRE';
      FacturaTypes.MES10 = 'OCTUBRE';
      FacturaTypes.MES11 = 'NOVIEMBRE';
      FacturaTypes.MES12 = 'DICIEMBRE';
    }
  
    // Tipos proformas-facturas:
    static PROFORMA = 'PROFORMA';
    static FACTURA = 'FACTURA';
    
    // Estatus proformas facturas:
    static SUBIDA_CLIENTE = 'SUBIDA CLIENTE';
    static SUBIDA_EMPRESA = 'SUBIDA EMPRESA';
    static FINALIZADO = 'FINALIZADO';
    
    static MES1 = 'ENERO';
    static MES2 = 'FEBRERO';
    static MES3 = 'MARZO';
    static MES4 = 'ABRIL';
    static MES5 = 'MAYO';
    static MES6 = 'JUNIO';
    static MES7 = 'JULIO';
    static MES8 = 'AGOSTO';
    static MES9 = 'SEPTIEMBRE';
    static MES10 = 'OCTUBRE';
    static MES11 = 'NOVIEMBRE';
    static MES12 = 'DICIEMBRE';

  }
  