export class Factura {

    public constructor(init?: Partial<Factura>) {
        Object.assign(this, init);
    }

    id: number;
    numero: string;
    ano: string;
    mes: string;
    total: number;
    tipo: string;
    estatus: string;
    archivo: any;
    idObra: string;
}