import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Factura } from '@app/facturas/models/Factura';
import * as moment from 'moment';
import { FacturasService } from '@app/facturas/services/facturas.service';
import { FacturasValidator } from "@app/facturas/validation/facturas.validator";
import { FacturaTypes } from '@app/facturas/models/constantes/FacturaTypes';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-facturas-detail',
  templateUrl: './facturas-detail.component.html',
  styleUrls: ['./facturas-detail.component.css']
})
export class FacturasDetailComponent implements OnInit {

  @Input() idObras: string;
  @Output() closeModal = new EventEmitter<boolean>();
  @BlockUI('componentsFacturasDetail') blockUIList: NgBlockUI;

  factura: Factura = new Factura();
  facturasForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  facturaTypes: any;
  titleDocumento: string;
  sizeArchivo: number;
  typeArchivo: string;
  baseArchivo: string | ArrayBuffer;
  uploadFile: boolean = false;
  archivoValid: boolean = false;
  annoActual: string = moment().format('YYYY');
  annoOrigen: number;
  annoActualN: number;
  mes: number;  
  anno: number; 
  mesArray: any[] = [];
  annosArray: any[] = [];
  facturas_validation_messages = FacturasValidator.FACTURAS_VALIDATION_MESSAGES;

  
  constructor(
    private toastr: ToastrService, 
    private facturasService: FacturasService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.titleDocumento = "Subir Documento";
    this.mesArray = [
      {"texto":"Enero","valor":1},
      {"texto":"Febrero","valor":2},
      {"texto":"Marzo","valor":3},
      {"texto":"Abril","valor":4},
      {"texto":"Mayo","valor":5},
      {"texto":"Junio","valor":6},
      {"texto":"Julio","valor":7},
      {"texto":"Agosto","valor":8},
      {"texto":"Septiembre","valor":9},
      {"texto":"Octubre","valor":10},
      {"texto":"Noviembre","valor":11},
      {"texto":"Diciembre","valor":12},
    ];
    this.annoActualN = Number(this.annoActual); 
    for (let index = 2018; index <= this.annoActualN + 1; index++) {
      this.annosArray.push({"texto":index});
    }

    this.anno = this.annoActualN;
    this.mes = Number(moment().format('M'));
    
    this.facturasForm = this.formBuilder.group({
      numero: [null, [Validators.required]],
      tipo: [null, [Validators.required]],
      total: [null, [Validators.required]],
      mes: [null, [Validators.required]],
      ano: [null, [Validators.required]],
      archivo: [null],
    });

    this.facturaTypes = [{"texto":FacturaTypes.PROFORMA}, {"texto":FacturaTypes.FACTURA}];
  }

  get f() { return this.facturasForm.controls; }
  add() {
    this.submitted = true;

    if (this.facturasForm.invalid) {
      return;
    } else {
      if(this.uploadFile) {
        this.submitted = false;
        this.uploadFile = false;
        return;
      }
    }
    
    this.factura = this.facturasForm.value;

    if((this.factura.archivo == null)){
      this.archivoValid = true;
      return
    } else {
      this.archivoValid = false
    }
    this.reloadStart();

    this.create(); 
  }
  create(){
    let facturasTem = new Factura();
    facturasTem = this.changeObject(this.factura);
   
    this.facturasService.CrearFacturas(facturasTem).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
        this.reloadStop();
        this.closeModal.emit(false);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        this.reloadStop();
    });
  }

  reiniciarFormulario() {
    this.factura = new Factura();
    this.facturasForm.reset();
    this.submitted = false;
  }
  changeObject(factura: Factura) {
    let facturasTem: Factura = new Factura();
    facturasTem.idObra = this.idObras;
    facturasTem.numero = factura.numero;
    facturasTem.tipo = factura.tipo;
    facturasTem.total = factura.total;
    facturasTem.ano = factura.ano;
    facturasTem.mes = factura.mes;

    if(factura.archivo != null && factura.archivo != undefined){
      facturasTem.archivo = {
        "base64": this.baseArchivo,
        "nombre": this.titleDocumento,
        "mimeType": this.typeArchivo,
        "tamano": this.sizeArchivo  
      };
    } else {
      facturasTem.archivo = null;
      // albaranesTem.archivo = {
      //   "base64": "",
      //   "nombre": "",
      //   "mimeType": "",
      //   "tamano": 0  
      // };
    }

    return facturasTem;
  }
  onFileChange(event: any) {
    console.log(event);
    var reader = new FileReader();
    reader.onload = this.convertFileSimple.bind(this, event.target.files.item(0));
    reader.readAsBinaryString(event.target.files.item(0));
  }

  convertFileSimple(file: File, event: any) {
    const binaryString = event.target.result;
    this.titleDocumento = file.name;
    this.sizeArchivo = file.size;
    this.typeArchivo = file.type;
    this.baseArchivo = btoa(binaryString);
    this.archivoValid = false
    this.uploadFile = false;
  }

  document() {
    this.uploadFile = true;
    let element: HTMLElement = document.getElementById('archivo') as HTMLElement;
    element.click();
  }
  
  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }
}
