import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Factura } from '@app/facturas/models/Factura';
import * as moment from 'moment';
import { FacturasService } from '@app/facturas/services/facturas.service';
import { FacturasValidator } from "@app/facturas/validation/facturas.validator";
import { FacturaTypes } from '@app/facturas/models/constantes/FacturaTypes';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-facturas-refrescar-detail',
  templateUrl: './facturas-refrescar-detail.component.html',
  styleUrls: ['./facturas-refrescar-detail.component.css']
})
export class FacturasRefrescarDetailComponent implements OnInit {

  @Input() idObras: string;
  @Input() idFactura: string;
  @Input() nroFactura: string;
  @Input() montoFactura: string;
  @Input() mes: string;
  @Input() anno: string;
  @Input() tipo: string;
  @Output() closeModal = new EventEmitter<boolean>();
  @BlockUI('componentsFacturasDetail') blockUIList: NgBlockUI;

  factura: Factura = new Factura();
  facturasForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  facturaTypes: any;
  titleDocumento: string;
  sizeArchivo: number;
  typeArchivo: string;
  baseArchivo: string | ArrayBuffer;
  uploadFile: boolean = false;
  archivoValid: boolean = false;
  annoActual: string = moment().format('YYYY');
  annoOrigen: number;
  annoActualN: number;
  mesArray: any[] = [];
  annosArray: any[] = [];
  facturas_validation_messages = FacturasValidator.FACTURAS_VALIDATION_MESSAGES;
  
  SUBIDA_CLIENTE: string = FacturaTypes.SUBIDA_CLIENTE;
  SUBIDA_EMPRESA: string = FacturaTypes.SUBIDA_EMPRESA;
  FINALIZADO: string = FacturaTypes.FINALIZADO;

  
  constructor(
    private toastr: ToastrService, 
    private facturasService: FacturasService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    console.log(this.mes);
    switch (this.mes) {
      case "ENERO":
        this.mes = "1";
        break;      
      case "FEBRERO":
        this.mes = "2";
        break;      
      case "MARZO":
        this.mes = "3";
        break;
      case "ABRIL":
        this.mes = "4";
        break;
      case "MAYO":
        this.mes = "5";
        break;
      case "JUNIO":
        this.mes = "6";
        break;
      case "JULIO":
        this.mes = "7";
        break;
      case "AGOSTO":
        this.mes = "8";
        break;
      case "SEPTIEMBRE":
        this.mes = "9";
        break;
      case "OCTUBRE":
        this.mes = "10";
        break;
      case "NOVIEMBRE":
        this.mes = "11";
        break;
      case "DICIEMBRE":
        this.mes = "12";
        break;
    
      default:
        break;
    }
    this.montoFactura = this.montoFactura.replace('.', '');
    this.montoFactura = this.montoFactura.replace(',', '.');
    this.titleDocumento = "Subir Documento";
    
    this.facturasForm = this.formBuilder.group({
      numero: [this.nroFactura, [Validators.required]],
      total: [Number(this.montoFactura), [Validators.required]],
      estatus: [this.SUBIDA_CLIENTE, [Validators.required]],
      mes: [this.mes],
      ano: [this.anno],
      tipo: [this.tipo],
      archivo: [null],
    });
  }

  get f() { return this.facturasForm.controls; }
  add() {
    this.submitted = true;

    if (this.facturasForm.invalid) {
      return;
    } else {
      if(this.uploadFile) {
        this.submitted = false;
        this.uploadFile = false;
        return;
      }
    }
    
    this.factura = this.facturasForm.value;

    if((this.factura.archivo == null)){
      this.archivoValid = true;
      return
    } else {
      this.archivoValid = false
    }
    this.reloadStart();
    this.update(); 
  }
  update(){


    let facturasTem = new Factura();
    facturasTem = this.changeObject(this.factura);
  
    this.facturasService.EditaFactura(facturasTem).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
        this.reloadStop();
        this.closeModal.emit(false);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        this.reloadStop();
    });
  }

  reiniciarFormulario() {
    this.factura = new Factura();
    this.facturasForm.reset();
    this.submitted = false;
  }
  changeObject(factura: Factura) {
    let facturasTem: Factura = new Factura();
    facturasTem.id = Number(this.idFactura);
    facturasTem.idObra = this.idObras;
    facturasTem.numero = factura.numero;
    facturasTem.estatus = factura.estatus;
    facturasTem.total = factura.total;
    facturasTem.tipo = factura.tipo;
    facturasTem.ano = factura.ano;
    facturasTem.mes = factura.mes;

    if(factura.archivo != null && factura.archivo != undefined){
      facturasTem.archivo = {
        "base64": this.baseArchivo,
        "nombre": this.titleDocumento,
        "mimeType": this.typeArchivo,
        "tamano": this.sizeArchivo  
      };
    } else {
      facturasTem.archivo = null;
      // albaranesTem.archivo = {
      //   "base64": "",
      //   "nombre": "",
      //   "mimeType": "",
      //   "tamano": 0  
      // };
    }

    return facturasTem;
  }
  
  onFileChange(event: any) {
    var reader = new FileReader();
    reader.onload = this.convertFileSimple.bind(this, event.target.files.item(0));
    reader.readAsBinaryString(event.target.files.item(0));
  }

  convertFileSimple(file: File, event: any) {
    const binaryString = event.target.result;
    this.titleDocumento = file.name;
    this.sizeArchivo = file.size;
    this.typeArchivo = file.type;
    this.baseArchivo = btoa(binaryString);
    this.archivoValid = false
    this.uploadFile = false;
  }

  document() {
    this.uploadFile = true;
    let element: HTMLElement = document.getElementById('archivo') as HTMLElement;
    element.click();
  }
  
  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }
}
