import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Factura } from '@app/facturas/models/Factura';
import * as moment from 'moment';

import { Router } from '@angular/router';
import { Pagination } from '@app/_models';
import { FacturasService } from '@app/facturas/services/facturas.service';

import { FacturaTypes } from '@app/facturas/models/constantes/FacturaTypes';

@Component({
  selector: 'app-facturas-list-detail',
  templateUrl: './facturas-list-detail.component.html',
  styleUrls: ['./facturas-list-detail.component.css']
})
export class FacturasListDetailComponent implements OnInit {

  @Input() idObras: string;
  @Input() idFactura: string;
  @Output() closeModal = new EventEmitter<boolean>();
  @BlockUI('componentsFacturasDetalle') blockUIList: NgBlockUI;

  rows: Factura[];
  rowsFiltered: Factura[];
  
  pagination: Pagination;
  listaFacturasDetail: Factura[];
  cargaCompleta = false;
  type: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  editFactura: number;
  totalFactura: string;
  validTotalFactura: boolean = false;
  private modalRef;

  constructor(private toastr: ToastrService,
              private facturasService: FacturasService,
              private confirmDialogService: ConfirmDialogService,
              private router: Router,
              private cdRef: ChangeDetectorRef
              
              ) {
}


  ngOnInit(): void {
    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.takeAux = 10;
    this.query = '';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;

    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    this.getAllData();
  }

  ngAfterViewInit(): void {
    // this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();
    this.facturasService.GetFacturaPorId(this.idFactura, this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        console.log('lista:',data);
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalFactura = Number(data.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});        
        this.validTotalFactura = (data.total != null) ? true : false;
        console.log('lista:',data);
        var list =  data.data;
        if(list.length > 0) {
          list.forEach(element => {
            // moment.locale('es');
            element.total = (element.total != null) ? Number(element.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.total;      
            element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
            element.estatus = element.estatus != "FINALIZADO" ? "NO" : "SI";
            switch (element.mes) {
              case "1":
                element.mes = FacturaTypes.MES1
                break;
              case "2":
                element.mes = FacturaTypes.MES2
                break;
              case "3":
                element.mes = FacturaTypes.MES3
                break;
              case "4":
                element.mes = FacturaTypes.MES4
                break;
              case "5":
                element.mes = FacturaTypes.MES5
                break;
              case "6":
                element.mes = FacturaTypes.MES6
                break;
              case "7":
                element.mes = FacturaTypes.MES7
                break;
              case "8":
                element.mes = FacturaTypes.MES8
                break;
              case "9":
                element.mes = FacturaTypes.MES9
                break;
              case "10":
                element.mes = FacturaTypes.MES10
                break;
              case "11":
                element.mes = FacturaTypes.MES11
                break;
              case "12":
                element.mes = FacturaTypes.MES12
                break;
            
              default:
                element.mes = FacturaTypes.MES1
                break;
            }
          });
          this.listaFacturasDetail = list;
          this.listaFacturasDetail = [...this.listaFacturasDetail];
        }

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaFacturasDetail;
    this.rowsFiltered = this.listaFacturasDetail;
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, editar: number, row: any) {
    this.idObras = this.idObras;
    this.cargaCompleta = true;
    this.editFactura = editar;
    this.idFactura = (row != null) ? row.id : null;
    var size = (editar == 1) ? "sm" : (editar == 2) ? "md" : "lg"
    this.facturasService.addModelFactura(modal, size);
  }
  onSetModal(e) {
    this.getAllData();
  }
  downloadFile(row) {
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = row.archivo.url;
    a.target = "_blank";
    a.download = "TEST";
    a.click();
    a.remove();
  }

  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.facturasService.GetFacturaPorId(this.idFactura, this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalFactura = Number(data.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});        
        this.validTotalFactura = (data.total > 0) ? true : false;
        console.log('lista:',data);
        var list =  data.data;
        if(list.length > 0) {          
          list.forEach(element => {
            // moment.locale('es');
            element.total = (element.total != null) ? Number(element.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.total;
            element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
            element.estatus = element.estatus != "FINALIZADO" ? "NO" : "SI";
            switch (element.mes) {
              case "1":
                element.mes = FacturaTypes.MES1
                break;
              case "2":
                element.mes = FacturaTypes.MES2
                break;
              case "3":
                element.mes = FacturaTypes.MES3
                break;
              case "4":
                element.mes = FacturaTypes.MES4
                break;
              case "5":
                element.mes = FacturaTypes.MES5
                break;
              case "6":
                element.mes = FacturaTypes.MES6
                break;
              case "7":
                element.mes = FacturaTypes.MES7
                break;
              case "8":
                element.mes = FacturaTypes.MES8
                break;
              case "9":
                element.mes = FacturaTypes.MES9
                break;
              case "10":
                element.mes = FacturaTypes.MES10
                break;
              case "11":
                element.mes = FacturaTypes.MES11
                break;
              case "12":
                element.mes = FacturaTypes.MES12
                break;
            
              default:
                element.mes = FacturaTypes.MES1
                break;
            }
          });
          this.listaFacturasDetail = list;
          this.listaFacturasDetail = [...this.listaFacturasDetail];
        }

        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

}
