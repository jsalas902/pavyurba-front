import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FacturasService } from '@app/facturas/services/facturas.service';

@Component({
  selector: 'app-modal-facturas-detail',
  templateUrl: './modal-facturas-detail.component.html',
  styleUrls: ['./modal-facturas-detail.component.css']
})
export class ModalFacturasDetailComponent implements OnInit {
  @Input() idObras: string;
  @Input() titel: string;
  @Input() edit: number;
  
  @Input() idFactura: string;
  @Input() nroFactura: string;
  @Input() montoFactura: number;
  @Input() mes: string;
  @Input() anno: string;
  @Input() tipo: string;
  
  @Output() setModal = new EventEmitter<boolean>();
  cargaCompletaModal:boolean;

  constructor(private modalService: NgbModal,private facturasService: FacturasService) { }

  ngOnInit(): void {
    this.cargaCompletaModal = false;
  }
  
  ngAfterViewInit(): void {
    this.cargaCompletaModal = true;
  }

  onCloseModal(e) {
    if (!e) this.setModal.emit(true);
        
    this.facturasService.onCloseModal();
  }
}
