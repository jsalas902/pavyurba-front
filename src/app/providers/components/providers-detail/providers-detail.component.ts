import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Provider } from '@app/providers/models/Provider';
import { Location } from '@app/locations/models/Location';

import { LocationsService } from '@app/locations/services/locations.service';
import { ProvidersService } from '@app/providers/services/providers.service';
import { ProvidersValidator } from "@app/providers/validation/provider.validator";

@Component({
  selector: 'app-providers-detail',
  templateUrl: './providers-detail.component.html',
  styleUrls: ['./providers-detail.component.css']
})
export class ProvidersDetailComponent implements OnInit {
  
  @Input() idProviders: string;
  @Output() closeModal = new EventEmitter<boolean>();

  providers: Provider = new Provider();
  locations: Location;
  idProvincia: string = null;
  idLocalidad: string = null;
  provincias: any;
  providersForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  providers_validation_messages = ProvidersValidator.PROVIDERS_VALIDATION_MESSAGES;
  // Modal location
  cargaCompleta = false;
  sizeid = 10;

  mensaje: string = '';
  maxLength: number = 500;
  
  constructor(
    private toastr: ToastrService, 
    private locationsService: LocationsService,
    private providersService: ProvidersService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.providersForm = this.formBuilder.group({
      id: [null],
      identificacion: [null, [Validators.required]],
      nombre: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      idProvincia: [null, [Validators.required]],
      idLocalidad: [null, [Validators.required]],
      direccion: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(500)]],
      codigoPostal: [null],
      email: [null],
      telefono: [null],
      emailAdicional: [null],
      telefonoAdicional: [null],
    });
   
    if (this.idProviders != null) {
      this.edicion = true;
      this.sizeid = 6;
      this.obtenerProviders();
    } else {
      this.sizeid = 8;
    }
    // this.obtenerLocations();
    this.obtenerProvincias();
  }

  obtenerProviders() {
    this.providersService.ObtenerPorId(this.idProviders).subscribe(
      data => {
        this.obtenerPatchProvider(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  obtenerProvincias() {
    this.locationsService.ListarProvinciasCombo().subscribe(
      data => {
        this.provincias = data;
      },
      error => {
        // this.toastr.error(error);
        this.toastr.error(error.mensaje);
      }
    );
  }

  obtenerLocations() {
    this.locationsService.ListarLocalidadesCombo(this.idProvincia).subscribe(
      data => {
        this.locations = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  get f() { return this.providersForm.controls; }
  add() {
    this.submitted = true;
    if (this.providersForm.invalid) {
        return;
    }

    this.providers = this.providersForm.value;
    
    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }

  }
  create(){
    let providersTem = new Provider();
    providersTem = this.changeObject(this.providers);
   
    this.providersService.Crear(providersTem).subscribe(
      data => {
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  edit(){
    let providersTem = new Provider();
    providersTem = this.changeObject(this.providers);

    this.providersService.Editar(providersTem).subscribe(
      data => {
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  reiniciarFormulario() {
    this.providers = new Provider();
    this.providersForm.reset();
    this.submitted = false;
  }
  obtenerPatchProvider(data){
    this.providers = this.changeObject(data);
    this.providersForm.patchValue({
      id: this.providers.id,
      identificacion: this.providers.identificacion,
      nombre: this.providers.nombre,
      email: this.providers.email,
      emailAdicional: this.providers.emailAdicional,
      telefono: this.providers.telefono,
      telefonoAdicional: this.providers.telefonoAdicional,
      direccion: this.providers.direccion,
      codigoPostal: this.providers.codigoPostal,
      idLocalidad: this.providers.idLocalidad,
      idProvincia: this.providers.idProvincia,
    });
  }
  changeObject(providers: Provider) {
    let providersTem: Provider = new Provider();
    providersTem.id = providers.id;
    providersTem.identificacion = providers.identificacion;
    providersTem.nombre = providers.nombre;
    providersTem.email = (providers.email == "") ? null : providers.email;
    providersTem.emailAdicional = (providers.emailAdicional == "") ? null : providers.emailAdicional;
    providersTem.telefono = (providers.telefono == "") ? null : providers.telefono;
    providersTem.telefonoAdicional = (providers.telefonoAdicional == "") ? null : providers.telefonoAdicional;
    providersTem.direccion = providers.direccion;
    providersTem.codigoPostal = providers.codigoPostal;
    providersTem.idLocalidad = providers.idLocalidad;
    providersTem.idProvincia = this.idProvincia = providers.idProvincia;

    this.changeProvincia();
    return providersTem;
  }
  addLocation(modal){
    this.cargaCompleta = true;
    this.locationsService.addModelLocation(modal);
  }
  onSetModal(e) {
    this.obtenerLocations();
  }
  changeProvincia() {
    if(this.idProvincia != null) {
      this.obtenerLocations();
    } else {
      this.idLocalidad = null;
    }
  }
}
