export class Provider {

    public constructor(init?: Partial<Provider>) {
        Object.assign(this, init);
    }

    id: number;
    identificacion: string;
    nombre: string;
    email: string;
    emailAdicional: string;
    telefono: string;
    telefonoAdicional: string;
    direccion: string;
    codigoPostal: string;
    localidad: string;
    idLocalidad: number;
    idProvincia: string;
    habilitado: boolean;
    fechaHoraLog: Date;
    usuarioLog: string;
    selected: boolean;

}