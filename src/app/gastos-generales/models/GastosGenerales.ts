export class GastosGenerales {

    public constructor(init?: Partial<GastosGenerales>) {
        Object.assign(this, init);
    }

    id: string;
    total: number;
    observaciones: string;
    idObra: string;
    fechaDesde: any;
    fechaHasta: any;
}