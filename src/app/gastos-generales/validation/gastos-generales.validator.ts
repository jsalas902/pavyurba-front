export class GastosGeneralesValidator {
    public static GASTOSGENERALES_VALIDATION_MESSAGES = {        
        'observaciones': [
            { type: 'required', message: 'Observaciones es requerido' },
        ],
        'total': [
            { type: 'required', message: 'Total es requerido' },
        ],
    }
}