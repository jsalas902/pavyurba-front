import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { GenericService, AuthenticationService } from '@app/_services';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Pagination } from '@app/_models';
import { GastosGenerales } from '../models/GastosGenerales';

@Injectable({
  providedIn: 'root'
})
export class GastosGeneralesService extends GenericService{
  private modalRef;

  constructor(private http: HttpClient,
    public authService: AuthenticationService,
    private injector: Injector,
    private modalService: NgbModal) {
  super();
  }

  ListarGastosGenerales(skip: string, take: string, query: string): Observable<Pagination> {
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/gastosGenerales?skip=' + skip + '&take=' + take + '&query=' + query, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  ListarGastosGeneralesPorObras(idObra: string, skip: string, take: string, query: string): Observable<Pagination> {
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/gastosGenerales/listarPorObra/' + idObra + '?skip=' + skip + '&take=' + take + '&query=' + query, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  ObtenerGastosGeneralesPorId(id: string): Observable<Location> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.get(environment.apiUrl + '/gastosGenerales/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta.data;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }


  Crear(gastosGenerales: GastosGenerales): Observable<any> {

    // this.headers = new HttpHeaders ({
    //   Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    // });
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));

    return this.http.post(environment.apiUrl + '/gastosGenerales', gastosGenerales, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  Editar(gastosGenerales: GastosGenerales): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.put(environment.apiUrl + '/gastosGenerales/' + gastosGenerales.id, gastosGenerales, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  Eliminar(id: string): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.delete(environment.apiUrl + '/gastosGenerales/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  addModelGastosGenerales(modal: any) {
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: 'md' });
  }
  onCloseModal() {
    this.modalRef.close();
  }
}