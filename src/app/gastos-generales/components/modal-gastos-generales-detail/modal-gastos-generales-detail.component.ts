import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GastosGeneralesService } from '@app/gastos-generales/services/gastos-generales.service';

@Component({
  selector: 'app-modal-gastos-generales-detail',
  templateUrl: './modal-gastos-generales-detail.component.html',
  styleUrls: ['./modal-gastos-generales-detail.component.css']
})
export class ModalGastosGeneralesDetailComponent implements OnInit {
  @Input() idObras: string;
  @Input() titel: string;
  @Input() idGastoGeneral: string;
  @Output() setModal = new EventEmitter<boolean>();
  cargaCompletaModal:boolean;

  constructor(private modalService: NgbModal,private gastosGeneralesService: GastosGeneralesService) { }

  ngOnInit(): void {
    this.cargaCompletaModal = false;
  }
  
  ngAfterViewInit(): void {
    this.cargaCompletaModal = true;
  }

  onCloseModal(e) {
    if (!e) this.setModal.emit(true);
        
    this.gastosGeneralesService.onCloseModal();
  }
}
