import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { GastosGenerales } from '@app/gastos-generales/models/GastosGenerales';

import { GastosGeneralesService } from '@app/gastos-generales/services/gastos-generales.service';
import { GastosGeneralesValidator } from "@app/gastos-generales/validation/gastos-generales.validator";
import { NgbDateStruct, NgbTimeStruct, NgbDate, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

import { NgbDateCustomParserFormatter} from '@app/_helpers/dateformat';
import { CustomDatepickerI18nService, I18n } from '@app/calendarios/services/custom-datepicker-i18n.service';
import {NgbCalendar, NgbDateAdapter, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-gastos-generales-detail',
  templateUrl: './gastos-generales-detail.component.html',
  styleUrls: ['./gastos-generales-detail.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18nService }
   ]
})
export class GastosGeneralesDetailComponent implements OnInit {
  @Input() idObras: string;
  @Input() idGastoGeneral: string;
  @Output() closeModal = new EventEmitter<boolean>();

  gastosGenerales: GastosGenerales = new GastosGenerales();
  gastosGeneralesForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  gastosgenerales_validation_messages = GastosGeneralesValidator.GASTOSGENERALES_VALIDATION_MESSAGES;
  
  mensaje: string = '';
  maxLength: number = 500;
  fechaDesde;
  fechaHasta;
  fecha;
  dateNow = moment().format("YYYY-MM-DD");
  readonly DELIMITER = '-';
  
  constructor(
    private toastr: ToastrService, 
    private gastosGeneralesService: GastosGeneralesService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.fecha = this.fromModel(this.dateNow);
    this.fechaDesde = this.fecha;
    this.fechaHasta = this.fecha;

    this.gastosGeneralesForm = this.formBuilder.group({
      id: [null],
      total: [null, [Validators.required]],
      observaciones: [null, [Validators.required]],
      fechaDesde: [this.fecha],
      fechaHasta: [this.fecha],
    });

    if (this.idGastoGeneral != null) {
      this.edicion = true;
      this.obtenerGastosGenerales();
    }
    // this.obtenerLocations();
  }

  obtenerGastosGenerales() {
    this.gastosGeneralesService.ObtenerGastosGeneralesPorId(this.idGastoGeneral).subscribe(
      data => {
        this.obtenerPatchGastoGeneral(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  get f() { return this.gastosGeneralesForm.controls; }
  add() {
    this.submitted = true;

    if (this.gastosGeneralesForm.invalid) {
      return;
    }
    
    this.gastosGenerales = this.gastosGeneralesForm.value;
    
    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }

  }
  create(){
    let gastosGeneralesTem = new GastosGenerales();

    this.gastosGenerales.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    this.gastosGenerales.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);
    gastosGeneralesTem = this.changeObject(this.gastosGenerales);
    gastosGeneralesTem.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    gastosGeneralesTem.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);
    // gastosGeneralesTem.idObra = this.idObras;
    /*let gastoGeneralData = {
      "idObra": this.idObras,
      "total": gastosGeneralesTem.total,
      "observaciones": gastosGeneralesTem.observaciones,
    }*/
   
    this.gastosGeneralesService.Crear(gastosGeneralesTem).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
        this.closeModal.emit(false);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  edit(){
    let gastosGeneralesTem = new GastosGenerales();
    this.gastosGenerales.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    this.gastosGenerales.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);
    gastosGeneralesTem = this.changeObject(this.gastosGenerales);
    gastosGeneralesTem.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    gastosGeneralesTem.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);

    this.gastosGeneralesService.Editar(gastosGeneralesTem).subscribe(
      data => {
        this.closeModal.emit(false);
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  reiniciarFormulario() {
    this.gastosGenerales = new GastosGenerales();
    this.gastosGeneralesForm.reset();
    this.submitted = false;
  }

  obtenerPatchGastoGeneral(data){
    this.gastosGenerales = this.changeObject(data);
    this.gastosGeneralesForm.patchValue({
      id: this.gastosGenerales.id,
      total: this.gastosGenerales.total,
      observaciones: this.gastosGenerales.observaciones,
    });
  }
  changeObject(gastosGenerales: GastosGenerales) {
    let gastosGeneralesTem: GastosGenerales = new GastosGenerales();
    gastosGeneralesTem.idObra = this.idObras;
    gastosGeneralesTem.id = gastosGenerales.id;
    gastosGeneralesTem.total = gastosGenerales.total;
    gastosGeneralesTem.observaciones = gastosGenerales.observaciones;
    gastosGeneralesTem.fechaDesde = this.fechaDesde = (gastosGenerales.fechaDesde !== null) ? this.fromModel(gastosGenerales.fechaDesde) : null;
    gastosGeneralesTem.fechaHasta = this.fechaHasta = (gastosGenerales.fechaHasta !== null) ? this.fromModel(gastosGenerales.fechaHasta) : null;
    return gastosGeneralesTem;
  }
  
  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[2], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[0], 10)
      };
    }
    return null;
  }
  toModel(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format();
  }
  toModelFix(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format('DD-MM-YYYY');
  }
  

}
