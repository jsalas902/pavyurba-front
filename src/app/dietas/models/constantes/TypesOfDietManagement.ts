export class TypesOfDietManagement {

    public constructor(init?: Partial<TypesOfDietManagement>) {
      Object.assign(this, init);
      TypesOfDietManagement.Parcial = 'PARCIAL';
      TypesOfDietManagement.Total = 'TOTAL';
      TypesOfDietManagement.Catalago = 'CATALOGO';
    }
  
    static Parcial = 'PARCIAL';
    static Total = 'TOTAL';
    static Catalago = 'CATALOGO';
  }
  