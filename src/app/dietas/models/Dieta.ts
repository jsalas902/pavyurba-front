export class Dieta {

    public constructor(init?: Partial<Dieta>) {
        Object.assign(this, init);
    }

    id: number;
    valor: number;
    valorfueraMadrid: number;
    fueraMadrid: boolean;
    tipoGestion: string;
    valorenMadrid: number;
    habilitado: boolean;
    fechaHoraLog: Date;
    usuarioLog: string;
   
}