export class DietasValidator {
    public static DIETAS_VALIDATION_MESSAGES = {        
        'valorfueraMadrid': [
            { type: 'required', message: 'Valor fuera de Madrid es requerido' },
        ],
        'valorenMadrid': [
            { type: 'required', message: 'Valor en Madrid es requerido' },
        ],
        'idsEmpleados': [
            { type: 'required', message: 'Empleados es requerido' },
        ]
    }
}