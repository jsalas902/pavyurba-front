import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Dieta } from '@app/dietas/models/Dieta';
import { ConfirmDialogService } from '@app/_services';
import { DietasService } from '@app/dietas/services/dietas.service';
import { EmployeesService } from '@app/employees/services/employees.service';
import { DietasValidator } from "@app/dietas/validation/dietas.validator";
import { TypesOfDietManagement } from '@app/dietas/models/constantes/TypesOfDietManagement'

@Component({
  selector: 'app-dietas-detail',
  templateUrl: './dietas-detail.component.html',
  styleUrls: ['./dietas-detail.component.css']
})
export class DietasDetailComponent implements OnInit {
 @Input() tipoGestion: string;
 @Input() data: Dieta;
 @Output() reset = new EventEmitter<boolean>();

  types: any;
  dietas: any;
  dietasTem: Dieta = new Dieta();
  dietasForm: FormGroup;
  submitted: boolean = false;
  titel: string = '';
  multipleMultiSelect: any;
  public multipleSelectArray: any;
  dietas_validation_messages = DietasValidator.DIETAS_VALIDATION_MESSAGES;
  constructor(
    private toastr: ToastrService, 
    private formBuilder: FormBuilder,
    private confirmDialogService: ConfirmDialogService,
    private employeesService: EmployeesService,
    private dietasService: DietasService
  ) {}

  ngOnInit(): void {
    
    if(this.tipoGestion !== TypesOfDietManagement.Parcial ){
      this.dietasForm = this.formBuilder.group({
        tipoGestion: [''],
        valorfueraMadrid: ['', [Validators.required]],
        valorenMadrid: ['', [Validators.required]],
        idsEmpleados: []
      });
    }else {
      this.dietasForm = this.formBuilder.group({
        tipoGestion: [''],
        valorfueraMadrid: ['', [Validators.required]],
        valorenMadrid: ['', [Validators.required]],
        idsEmpleados: [[], [Validators.required]]
      });
    }
    this.obtenerPatchDieta(this.data);
    this.types = {
      "Total": TypesOfDietManagement.Total,
      "Parcial": TypesOfDietManagement.Parcial,
      "Catalago": TypesOfDietManagement.Catalago
    };
    this.titel = this.tipoGestion === TypesOfDietManagement.Total ? `Seguro quiere realizar el cambio de dieta a todos los trabajadores, esta acción es Irreversible  ?`: this.tipoGestion === TypesOfDietManagement.Parcial ? `Seguro quiere realizar el cambio de dieta a estos trabajadores ?`: `Seguro quiere realizar el cambio solo en el modulo de Dieta ?`;

    this.obtenerEmployees();
  }
  obtenerEmployees() {
    this.employeesService.ListarEmployeesCombo().subscribe(
      data => {
        this.multipleSelectArray = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  get f() { return this.dietasForm.controls; }
  add() {
    this.submitted = true;
    if (this.dietasForm.invalid) {
      return;
    }
    
    this.dietas = this.dietasForm.value;
    
    this.confirmDialogService.confirmThis(`${this.titel}`, () =>  {
      this.edit();      
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario`);
    });

  }
  edit(){
    var empleadosID = []; 
    if(this.dietas.idsEmpleados !== null) {
      this.dietas.idsEmpleados.forEach(element => {
        empleadosID.push(element.id);
      });
    }
    let dietasTem = {
      "dietas": [
          {
            "id": this.dietasTem[0].id,
            "valor": this.dietasTem[0].fueraMadrid ? this.dietas.valorfueraMadrid : this.dietas.valorenMadrid,
            "fueraMadrid": this.dietasTem[0].fueraMadrid
          },
          {
            "id": this.dietasTem[1].id,
            "valor": this.dietasTem[1].fueraMadrid ? this.dietas.valorfueraMadrid : this.dietas.valorenMadrid,
            "fueraMadrid": this.dietasTem[1].fueraMadrid
          }
      ],
      "idsEmpleados": (this.dietas.idsEmpleados !== null) ? empleadosID : this.dietas.idsEmpleados
    }
    this.dietasService.Editar(dietasTem, this.tipoGestion).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        this.reset.emit(true);
        this.submitted = false;
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }
  obtenerPatchDieta(data){
    this.dietasTem = data;
    this.dietas = this.changeObject(data);
    this.dietasForm.patchValue({
      valorfueraMadrid: this.dietas.valorfueraMadrid,
      valorenMadrid: this.dietas.valorenMadrid,
      tipoGestion: this.tipoGestion
    });
  }
  changeObject(dietas: Dieta) {
    let dietasTem: Dieta = new Dieta();
    dietasTem.valorfueraMadrid = dietas ? ((dietas[0].fueraMadrid) ? dietas[0].valor : dietas[1].valor) : null;
    dietasTem.valorenMadrid = dietas ? ((dietas[1].fueraMadrid) ? dietas[0].valor : dietas[1].valor) : null;
    return dietasTem;
  }

}
