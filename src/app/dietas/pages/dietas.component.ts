import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Dieta } from '@app/dietas/models/Dieta';
import { DietasService } from '@app/dietas/services/dietas.service';
import { TypesOfDietManagement } from '@app/dietas/models/constantes/TypesOfDietManagement'

@Component({
  selector: 'app-dietas',
  templateUrl: './dietas.component.html',
  styleUrls: ['./dietas.component.css']
})
export class DietasComponent implements OnInit {
  @BlockUI('componentsDietas') blockUIList: NgBlockUI;

  public rightIconCollapse1 = false;
  public rightIconCollapse2 = false;
  public rightIconCollapse3 = false;

  dietas: Dieta = new Dieta();
  cargaCompleta = false;
  types: any;

  constructor(
    private toastr: ToastrService, 
    private dietasService: DietasService
  ) {}

  ngOnInit(): void {
    this.blockUIList.start('Loading..');
    this.obtenerDietas();
    this.types = {
      "Total": TypesOfDietManagement.Total,
      "Parcial": TypesOfDietManagement.Parcial,
      "Catalago": TypesOfDietManagement.Catalago
    };
  }
  ngAfterViewInit(): void {
    
  }
  onReset($event){
    this.ngOnInit();
  }
  obtenerDietas() {
    this.dietasService.ListarTodos().subscribe(
      data => {
        this.dietas = data;
        this.blockUIList.stop();
        this.cargaCompleta = true;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
        this.blockUIList.stop();
      }
    );
  }  
}
