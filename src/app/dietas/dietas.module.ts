import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CardModule } from '../content/partials/general/card/card.module';
import { BlockUIModule } from 'ng-block-ui';
import { BlockTemplateComponent } from '../_layout/blockui/block-template.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared/shared.module';

import { DietasComponent } from './pages/dietas.component';
import { DietasDetailComponent } from './components/dietas-detail/dietas-detail.component';



@NgModule({
  imports: [
    CommonModule,
    CardModule,
    NgbModule,
    NgSelectModule,
    UiSwitchModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    BlockUIModule.forRoot({
      template: BlockTemplateComponent
    }),
    SharedModule,
    RouterModule.forChild([
      {
        path: 'list',
        component: DietasComponent
      },
    ]),
  ],  
  declarations: [DietasComponent, DietasDetailComponent],
  exports: [RouterModule, DietasComponent]
})
export class DietasModule { }
