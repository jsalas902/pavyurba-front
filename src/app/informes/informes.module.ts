import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InformesComponent } from './pages/informes.component';
import { ModalInformesDetailComponent } from './components/modal-informes-detail/modal-informes-detail.component';
import { InformesDetailComponent } from './components/informes-detail/informes-detail.component';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxDatatableFooterModule } from 'ngx-datatable-footer';
import { NgSelectModule } from '@ng-select/ng-select';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';
import { CardModule } from '../content/partials/general/card/card.module';
import { BreadcrumbModule } from '../_layout/breadcrumb/breadcrumb.module';
import { BlockUIModule } from 'ng-block-ui';
import { BlockTemplateComponent } from '../_layout/blockui/block-template.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedComponentsModule } from '../shared/components/shared-components.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [InformesComponent, ModalInformesDetailComponent, InformesDetailComponent],
  exports: [InformesComponent, ModalInformesDetailComponent, InformesDetailComponent],
  imports: [
    CommonModule,
    CardModule,
    NgbModule,
    BreadcrumbModule,
    NgSelectModule,
    UiSwitchModule,
    FormsModule,
    ReactiveFormsModule,
    ClipboardModule,
    PerfectScrollbarModule,
    NgxDatatableModule,
    BlockUIModule.forRoot({
      template: BlockTemplateComponent
    }),
    NgxDatatableFooterModule.forRoot(),
    SharedComponentsModule,
    RouterModule.forChild([
      {
        path: '',
        component: InformesComponent
      },
    ]),
  ]
})
export class InformesModule { }
