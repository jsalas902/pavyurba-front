import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InformesService } from '@app/informes/services/informes.service';


@Component({
  selector: 'app-modal-informes-detail',
  templateUrl: './modal-informes-detail.component.html',
  styleUrls: ['./modal-informes-detail.component.css']
})
export class ModalInformesDetailComponent implements OnInit {
  @Input() idObras: string;
  @Input() idEmpleado: string;
  @Input() idInforme: string;
  @Input() tipoGeneracion: string;
  
  @Input() titel: string;
  
  @Input() skip: string;
  @Input() take: string;
  @Input() query: string;
  
  @Output() setModal = new EventEmitter<boolean>();
  cargaCompletaModal:boolean;

  constructor(private modalService: NgbModal,private informesService: InformesService) { }

  ngOnInit(): void {
    this.cargaCompletaModal = false;
  }
  
  ngAfterViewInit(): void {
    this.cargaCompletaModal = true;
  }

  onCloseModal(e) {
    if (!e) this.setModal.emit(true);
        
    this.informesService.onCloseModal();   
  }
}
