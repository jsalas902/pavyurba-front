import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbDateStruct, NgbTimeStruct, NgbDate, NgbModal, NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { Condition } from '@app/conditions/models/condition';
import { Obra } from '@app/obras/models/Obra';
import { Employee } from '@app/employees/models/Employee';
import { Informes } from '@app/informes/models/Informes';
import { InformesValidator } from "@app/informes/validation/informes.validator";
import { InformesService } from '@app/informes/services/informes.service';
import { ObrasService } from '@app/obras/services/obras.service';
import { EmployeesService } from '@app/employees/services/employees.service';

import { NgbDateCustomParserFormatter} from '@app/_helpers/dateformat';
import { InformesTypes } from '@app/informes/models/constantes/InformesTypes';
import { CustomDatepickerI18nService, I18n } from '@app/calendarios/services/custom-datepicker-i18n.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-informes-detail',
  templateUrl: './informes-detail.component.html',
  styleUrls: ['./informes-detail.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18nService }
  ]
})
export class InformesDetailComponent implements OnInit {

  @Input() idObras: string;
  @Input() idEmpleado: string;
  @Input() idInforme: string;
  @Input() tipoGeneracion: string;

  @Output() closeModal = new EventEmitter<boolean>();
  @BlockUI('componentsInformesDetail') blockUIList: NgBlockUI;

  informe: Informes = new Informes();
  informeForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  isGuardar: boolean = false;
  isProforma: boolean = false;
  isProveedores: boolean = false;
  validInforme: boolean = false;
  tipoCombo: any[] = [];
  employeesCombo: Employee;
  informes_validation_messages = InformesValidator.INFORMES_VALIDATION_MESSAGES;
  fechaDesde;
  fechaHasta;
  d: any;
  d1: any;
  mes: number;  
  anno: number; 
  mesInicio: number;  
  annoInicio: number; 
  mesFin: number;  
  annoFin: number; 
  fechaArray: any[] = [];
  fecha;
  dateNow = moment().format("YYYY-MM-DD");
  annoActual: string = moment().format('YYYY');
  annoOrigen: number;
  annoActualN: number;
  mesArray: any[] = [];
  annosArray: any[] = [];
  archivoValid: boolean = false;
  fechaDesdeValid: boolean = false;
  fechaHastaValid: boolean = false;
  tipoValid: boolean = false;
  anoValid: boolean = false;
  mesValid: boolean = false;
  tipoInforme: string = null;
  informeTipo: string = null;
  
  fileToUpload: File = null;
  titleDocumento: string;
  sizeArchivo: number;
  typeArchivo: string;
  baseArchivo: string | ArrayBuffer;
  uploadFile: boolean = false;
  readonly DELIMITER = '-';

  constructor(
    private toastr: ToastrService, 
    private informesService: InformesService,
    private employeesService: EmployeesService,
    private obrasService: ObrasService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {  
    this.titleDocumento = "Seleccionar Documento";

    this.fecha = this.fromModel(this.dateNow);
    
    this.mesArray = [
      {"texto":"Enero","valor":1},
      {"texto":"Febrero","valor":2},
      {"texto":"Marzo","valor":3},
      {"texto":"Abril","valor":4},
      {"texto":"Mayo","valor":5},
      {"texto":"Junio","valor":6},
      {"texto":"Julio","valor":7},
      {"texto":"Agosto","valor":8},
      {"texto":"Septiembre","valor":9},
      {"texto":"Octubre","valor":10},
      {"texto":"Noviembre","valor":11},
      {"texto":"Diciembre","valor":12},
    ];
    this.annoActualN = Number(this.annoActual); 
    for (let index = 2018; index <= this.annoActualN + 1; index++) {
      this.annosArray.push({"texto":index});
    }

    this.anno = this.annoActualN;
    this.annoInicio = this.annoActualN;
    this.annoFin = this.annoActualN;
    this.mes = Number(moment().format('M'));
    this.mesInicio = Number(moment().format('M'));
    this.mesFin = Number(moment().format('M'));

    if(this.tipoGeneracion == "GENERAL") {
      this.tipoCombo = [
        {"texto":InformesTypes.NOMINA_POR_FECHA},
        {"texto":InformesTypes.TOTALES_PRODUCTOS_PROVEEDORES},
        {"texto":InformesTypes.TOTAL_PRODUCTOS_PROVEEDORES_EMPRESAS},
        {"texto":InformesTypes.INFORME_PROFORMA_CLIENTES},
        {"texto":InformesTypes.INFORME_NOMINAS_POR_EMPRESAS_TRABAJADORES},
        {"texto":InformesTypes.DIETAS_POR_TRABAJADORES},
        {"texto":InformesTypes.PARTES_TRABAJOS_TRABAJADOR_ASEGURADO_POR_METRO},
        {"texto":InformesTypes.PARTES_TRABAJOS_TRABAJADOR_ASEGURADO_JORNAL},
        {"texto":InformesTypes.INFORME_CLIENTES_POR_FECHA_OBRA},
        {"texto":InformesTypes.CAMBIO_OBRA}
      ];
    } else if(this.tipoGeneracion == "EMPLEADO") {
      this.obtenerEmployees();
    } else if(this.tipoGeneracion == "OBRA") {
      this.tipoCombo = [
        {"texto":InformesTypes.MATERIALES_POR_OBRA},
        {"texto":InformesTypes.INFORME_CLIENTES_POR_FECHA_OBRA}
      ];
    }

    this.informeForm = this.formBuilder.group({
      id: [this.idInforme],
      fechaDesde: [this.fecha],
      fechaHasta: [this.fecha],
      tipo: [null],
      ano: [null],
      mes: [null],
      fecha: [null],
      tipoGeneracion: [this.tipoGeneracion],
      noGuardar: [false],
      idObra: [this.idObras],
      idEmpleado: [this.idEmpleado],
      archivo: [null]
    });

    if (this.idInforme != null && this.idInforme != undefined) {
      this.edicion = true;
      // this.obtenerParteTrabajo();
    } 

  }

  obtenerEmployees() {
    this.employeesService.ObtenerPorId(this.idEmpleado).subscribe(
      data => {
        this.tipoInforme = data.tipo;
        if(this.tipoInforme == "AUTONOMO") {
          this.tipoCombo = [
            {"texto":InformesTypes.DIETAS_POR_TRABAJADORES}
          ];
        } else if(this.tipoInforme == "ASEGURADO METRO"){
          this.tipoCombo = [
            {"texto":InformesTypes.DIETAS_POR_TRABAJADORES},
            {"texto":InformesTypes.PARTES_TRABAJOS_TRABAJADOR_ASEGURADO_POR_METRO}
          ];
        } else {
          this.tipoCombo = [
            {"texto":InformesTypes.DIETAS_POR_TRABAJADORES},
            {"texto":InformesTypes.PARTES_TRABAJOS_TRABAJADOR_ASEGURADO_JORNAL}
          ];
        }
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  get f() { return this.informeForm.controls; }
  add() {
    this.submitted = true;


    // if (this.informeForm.invalid) {
    //   return;
    // } 
    
    this.informe = this.informeForm.value;

    if(this.edicion) {
      if((this.informe.archivo == null)){
        this.archivoValid = true;
        return
      } else {
        this.archivoValid = false
      }
    } else {
      this.archivoValid = false;
      this.anoValid = false;
      this.mesValid = false
      this.fechaDesdeValid = false;
      this.fechaHastaValid = false; 

      if((this.informe.tipo == null)){
        this.tipoValid = true;
      } else {
        this.tipoValid = false;
      }

      if(!this.informe.noGuardar) {
        if((this.informe.ano == null)){
          this.anoValid = true;
        } else {
          this.anoValid = false
        }
  
        if((this.informe.mes == null)){
          this.mesValid = true;
        } else {
          this.mesValid = false;
        }

        if((this.informe.tipo == null) ||
        (this.informe.ano == null) ||
        (this.informe.mes == null)) {
          return
        }
      } else {
        if((this.informe.fechaDesde == null)){
          this.fechaDesdeValid = true;
        } else {
          this.fechaDesdeValid = false;
        }
  
        if((this.informe.fechaHasta == null)){
          this.fechaHastaValid = true;
        } else {
          this.fechaHastaValid = false;
        }

        if((this.informe.tipo == null) ||
        (this.informe.fechaDesde == null) ||
        (this.informe.fechaHasta == null)) {
          return
        }
      }
    }
    
    this.reloadStart();

    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }
  }
  create(){
    this.fechaArray = [];
    let informeTem = new Informes();
    // this.informe.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    // this.informe.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);
    informeTem = this.changeObject(this.informe);

    if(this.informe.noGuardar) {
      informeTem.mes = null;
      informeTem.ano = null;
    } 

    if(this.isProforma || this.isProveedores) {
      if(Number(this.annoInicio) > Number(this.annoFin)) {
        this.toastr.error(`El año Fin no puede ser mayor al año de Inicio`, 'Error',  { timeOut: 2500, closeButton: true });
        this.reloadStop();
        return
      } else {
        if(Number(this.annoInicio) == Number(this.annoFin)) {
          if(Number(this.mesInicio) > Number(this.mesFin)) {
            this.toastr.error(`El rango de Inicio debe ser menor al rango de Fin`, 'Error',  { timeOut: 2500, closeButton: true });
            this.reloadStop();
            return
          } else {
            if(Number(this.mesInicio) < Number(this.mesFin)) {
              for (let index = this.mesInicio; index <= this.mesFin; index++) {
                this.fechaArray.push({
                  mes: Number(index),
                  anno: Number(this.annoInicio)
                });              
              }            
            } else {
              this.fechaArray.push({
                mes: Number(this.mesInicio),
                anno: Number(this.annoInicio)
              })
            }
          }
        } else {
          for (let index = this.annoInicio; index <= this.annoFin; index++) {
            let inicioFechas = (this.annoInicio == index) ? this.mesInicio : 1;
            let lengthFechas = (index == this.annoFin) ? this.mesFin : 12;
            for (let index2 = inicioFechas; index2 <= lengthFechas; index2++) {
              this.fechaArray.push({
                mes: Number(index2),
                anno: Number(index)
              });
            }                        
          }     
        }
      }
      informeTem.fecha = this.fechaArray;
      informeTem.mes = null;
      informeTem.ano = null;
      informeTem.isProforma = true;
      informeTem.noGuardar = true;

      informeTem.fechaDesde = null;
      informeTem.fechaHasta = null;
    } else {
      informeTem.fecha = null;
      informeTem.isProforma = false;
      informeTem.noGuardar = false;
    }  
    
    // if(this.isProveedores) {
    //   informeTem.noGuardar = true;
    //   informeTem.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    //   informeTem.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);

    //   this.fechaDesde = this.toModelFix(this.fechaDesde);
    //   this.fechaHasta = this.toModelFix(this.fechaHasta);
    //   this.informe.fechaDesde = this.fechaDesde;
    //   this.informe.fechaHasta = this.fechaHasta;
    // }

    this.informesService.CrearInforme(informeTem).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        var registro = data.data;
        let a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        // a.href = 'data:' + registro.mimeType + ';base64,' + registro.base64;
        a.href = `data:${registro.mimeType};base64,${registro.base64}`;
        a.download = `${registro.nombre}.${registro.extension}`;
        a.click();
        a.remove();

        this.reiniciarFormulario();
        // this.getAllData();
        this.reloadStop();
        this.closeModal.emit(false);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error',  { timeOut: 2500, closeButton: true });
        this.reloadStop();        
    });
  }

  edit(){
    var data = {
      "base64": this.baseArchivo,
      "nombre": this.titleDocumento,
      "mimeType": this.typeArchivo,
      "tamano": this.sizeArchivo      
    }

    this.informesService.ReemplazarInformePorId(this.informe.id, data).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
        this.reloadStop();
        this.closeModal.emit(false);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        this.reloadStop();
    });
  }

  reiniciarFormulario() {
    this.informe = new Informes();
    this.informeForm.reset();
    this.submitted = false;
  }
  // obtenerParteTrabajo() {
  //   this.informesService.get(this.idParteTrabajo).subscribe(
  //     data => {
  //       this.obtenerPatchParteTrabajo(data);
  //     },
  //     error => {
  //       // this.toastr.error(error.mensaje);
  //       this.toastr.error(error);
  //     }
  //   );
  // }
  changeObject(informe: Informes) {
    let informeTem: Informes = new Informes();
    informeTem.id = informe.id;
    informeTem.tipo = informe.tipo;
    informeTem.mes = informe.mes;
    informeTem.ano = informe.ano;
    informeTem.fechaDesde = this.fechaDesde = informe.fechaDesde;
    informeTem.fechaHasta = this.fechaHasta = informe.fechaHasta;
    informeTem.noGuardar = informe.noGuardar;
    informeTem.tipoGeneracion = informe.tipoGeneracion;
    informeTem.idObra = informe.idObra;
    informeTem.idEmpleado = informe.idEmpleado;
    

    return informeTem;
  }

  obtenerPatchParteTrabajo(data){
    this.informe = this.changeObject(data);
    this.informeForm.patchValue({
      id: this.informe.id,
    });
  }
  toModel(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format();
  }
  toModelFix(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format('DD-MM-YYYY');
  }
  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[2], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[0], 10)
      };
    }
    return null;
  }
  
  changeGuardar() {
    this.isGuardar = !this.isGuardar;
  }

  onFileChange(event: any) {
    this.fileToUpload  = event.target.files.item(0);

    var reader = new FileReader();
    reader.onload = this.convertFileSimple.bind(this, this.fileToUpload);
    reader.readAsBinaryString(this.fileToUpload);
  }

  convertFileSimple(file: File, event: any) {
    const binaryString = event.target.result;
    this.titleDocumento = file.name;
    this.sizeArchivo = file.size;
    this.typeArchivo = file.type;
    this.baseArchivo = btoa(binaryString);
    this.archivoValid = false
    this.uploadFile = false;
  }

  document() {
    this.uploadFile = true;
    let element: HTMLElement = document.getElementById('archivo') as HTMLElement;
    element.click();
  }

  changeTipoInforme() {
    if(this.informeTipo == "MATERIALES POR OBRA" || 
      this.informeTipo == "INFORME PROFORMA CLIENTES" ||
      this.informeTipo == "TOTAL PRODUCTOS PROVEEDORES POR EMPRESAS") {
      this.validInforme = true;
    } else {
      this.validInforme = false;
    }

    if(this.informeTipo == "INFORME PROFORMA CLIENTES") {
      this.isProforma = true;
    } else {
      this.isProforma = false
    }

    if(this.informeTipo == "TOTAL PRODUCTOS PROVEEDORES POR EMPRESAS") {
      this.isProveedores = true;
    } else {
      this.isProveedores = false
    }
  }
  
  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

}
