export class InformesValidator {
    public static INFORMES_VALIDATION_MESSAGES = {        
        'ano': [
            { message: 'Año es requerido' },
        ],
        'mes': [
            { message: 'Mes es requerido' },
        ],
        'tipo': [
            { message: 'Tipo es requerido' },
        ],
        'tipoGeneracion': [
            { message: 'Tipo Generación es requerido' },
        ],
        'idObra': [
            { message: 'Obra es requerido' },
        ],
        'fechaDesde': [
            { message: 'Fecha Desde es requerido' },
        ],
        'fechaHasta': [
            { message: 'Fecha Hasta es requerido' },
        ],
        'idEmpleado': [
            { message: 'Trabajador es requerido' },
        ],
        'archivo': [
            { message: 'El archivo es requerido' },
        ],
    }
}