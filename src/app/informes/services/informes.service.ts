import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { GenericService, AuthenticationService } from '@app/_services';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Pagination } from '@app/_models';
import { Informes } from '../models/Informes';

@Injectable({
  providedIn: 'root'
})
export class InformesService extends GenericService{
  private modalRef;

  constructor(private http: HttpClient,
    public authService: AuthenticationService,
    private injector: Injector,
    private modalService: NgbModal) {
  super();
  }

  ListarInformes(tipoGeneracion: string, idPropietario: string, skip: string, take: string, query: string): Observable<Pagination> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/informes/' + tipoGeneracion + '/' + idPropietario + '?skip=' + skip + '&take=' + take + '&query=' + query, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  ReemplazarInformePorId(id: number, data: any): Observable<Pagination> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.put(
      environment.apiUrl + '/informes/' + id, data, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  CrearInforme(informe: Informes): Observable<any> {

    // this.headers = new HttpHeaders ({
    //   Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    // });
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));

    return this.http.post(environment.apiUrl + '/informes', informe, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  EliminarInforme(idInforme: string): Observable<any> {

    // this.headers = new HttpHeaders ({
    //   Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    // });
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));

    return this.http.delete(environment.apiUrl + '/informes/' + idInforme, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  addModelInforme(modal: any) {
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: "md" });
  }
  onCloseModal() {
    this.modalRef.close();
  }
}
