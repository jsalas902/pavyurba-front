export class Informes {

    public constructor(init?: Partial<Informes>) {
        Object.assign(this, init);
    }

    id: number;
    ano: string;
    mes: string;
    fecha: any;
    tipo: string;
    tipoGeneracion: string;
    noGuardar: boolean;
    isProforma: boolean;
    fechaDesde: any;
    fechaHasta: any;
    idEmpleado: string;
    idObra: string;
    archivo: any;
}