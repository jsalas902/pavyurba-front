import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Informes } from '@app/informes/models/Informes';
import * as moment from 'moment';

import { Router } from '@angular/router';
import { Pagination } from '@app/_models';
import { InformesService } from '@app/informes/services/informes.service';
import { InformesTypes } from '../models/constantes/InformesTypes';
import { FacturaTypes } from '@app/facturas/models/constantes/FacturaTypes';


@Component({
  selector: 'app-informes',
  templateUrl: './informes.component.html',
  styleUrls: ['./informes.component.css']
})
export class InformesComponent implements OnInit {

  @BlockUI('componentsInforme') blockUIList: NgBlockUI;

  @Input() idObras: string;  
  @Input() tipoGeneracion: string;  
  @Input() idEmpleado: string;  

  rows: Informes[];
  rowsFiltered: Informes[];
  
  pagination: Pagination;
  listarInformes: Informes[];
  cargaCompleta = false;
  titelModal: string;
  type: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  idInforme: string;
  idPropietario: string;
  private modalRef;

  constructor(private toastr: ToastrService,
              private informesService: InformesService,
              private confirmDialogService: ConfirmDialogService,
              private router: Router,
              private cdRef: ChangeDetectorRef
              
              ) {
}


  ngOnInit(): void {
    if(this.idObras != null && this.idObras != undefined){
      this.idPropietario = this.idObras
    } else {
      if(this.idEmpleado != null && this.idEmpleado != undefined){
        this.idPropietario = this.idEmpleado
      } else {
        this.idPropietario = "0";
        this.tipoGeneracion = InformesTypes.GENERAL;
      }
    }

    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = '';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;

    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    this.getAllData();
  }

  ngAfterViewInit(): void {
    // this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();
    this.informesService.ListarInformes(this.tipoGeneracion, this.idPropietario, this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');  
          switch (element.mes) {
            case "1":
              element.mes = FacturaTypes.MES1
              break;
            case "2":
              element.mes = FacturaTypes.MES2
              break;
            case "3":
              element.mes = FacturaTypes.MES3
              break;
            case "4":
              element.mes = FacturaTypes.MES4
              break;
            case "5":
              element.mes = FacturaTypes.MES5
              break;
            case "6":
              element.mes = FacturaTypes.MES6
              break;
            case "7":
              element.mes = FacturaTypes.MES7
              break;
            case "8":
              element.mes = FacturaTypes.MES8
              break;
            case "9":
              element.mes = FacturaTypes.MES9
              break;
            case "10":
              element.mes = FacturaTypes.MES10
              break;
            case "11":
              element.mes = FacturaTypes.MES11
              break;
            case "12":
              element.mes = FacturaTypes.MES12
              break;
          
            default:
              element.mes = FacturaTypes.MES1
              break;
          }        
        });
        this.listarInformes = data.data;
        this.listarInformes = [...this.listarInformes];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listarInformes;
    this.rowsFiltered = this.listarInformes;
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, row: any) {
    this.titelModal = (row != null) ? "Subir Archivo" : "Crear Informe";
    this.idObras = (this.idObras != null && this.idObras != undefined) ? this.idObras : null;
    this.idEmpleado = (this.idEmpleado != null && this.idEmpleado != undefined) ? this.idEmpleado : null;
    this.cargaCompleta = true;
    this.idInforme = (row != null) ? row.id.toString() : null;
    this.informesService.addModelInforme(modal);
  }
  onSetModal(e) {
    this.getAllData();
  }
  
  downloadFile(row) {
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = row.archivo.url;
    a.target = "_blank";
    a.download = "TEST";
    a.click();
    a.remove();
  }
  
  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.informesService.ListarInformes(this.tipoGeneracion, this.idPropietario, this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');          
        });
        this.listarInformes = data.data;
        this.listarInformes = [...this.listarInformes];

        this.getTabledata();
        this.reloadStop();

        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  removeRegistro(row: any) {
    this.confirmDialogService.confirmThis(`Se eliminará el archivo que ya está en el sistema,
    se deberá generar nuevamente el archivo.
    
    Esta acción no se puede deshacer.`, () =>  {
      this.delete(row);      
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario`);
    });
  }

  delete(row: any) {
   
    this.informesService.EliminarInforme(row.id.toString()).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Eliminado' , { timeOut: 2500, closeButton: true });
        this.getAllData();
      },
      error => {
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
    
  }

  reemplazarRegistro(modal: any, row: any) {
    this.confirmDialogService.confirmThis(`Se reemplazará el archivo que ya está en el sistema.
    
    Esta acción no se puede deshacer.`, () =>  {
      this.addModel(modal, row);      
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario`);
    });
  }

}
