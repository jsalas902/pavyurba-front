export class UserValidator {
    public static USER_VALIDATION_MESSAGES = {
        'zip': [
          { type: 'required', message: 'zip is required' },
          { type: 'minlength', message: 'zip must be at least 5 characters long' },
          { type: 'maxlength', message: 'zip cannot be more than 15 characters long' },
          { type: 'pattern', message: 'Your zip must contain only numbers' }
        ],
        'email': [
          { type: 'required', message: 'Email is required' },
          { type: 'pattern', message: 'Enter a valid email' }
        ],
        'address': [
          { type: 'required', message: 'address is required' },
          { type: 'maxlength', message: 'address cannot be more than 15 characters long' }
        ],
        'dni': [
          { type: 'required', message: 'Dni is required' },
          { type: 'pattern', message: 'Your dni must contain only numbers' }
        ],
        'phone': [
            { type: 'required', message: 'Phone is required' },
            { type: 'pattern', message: 'Your phone must contain only numbers' }
        ],
        'name': [
            { type: 'required', message: 'Name is required' },
            { type: 'pattern', message: 'Your name must contain only letters' },            
            { type: 'maxlength', message: 'Name cannot be more than 50 characters long' },
        ],
        'user_type_id': [
            { type: 'required', message: 'ID Type User is required' }
        ]
    }
}