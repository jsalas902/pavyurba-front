export class UserLogin {

  public constructor(init?: Partial<UserLogin>) {
    Object.assign(this, init);
  }

  email: string;
  clave: string;
}
