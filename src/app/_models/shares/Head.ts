export class Head {

    public constructor(init?: Partial<Head>) {
        Object.assign(this, init);
    }

    name?: string | null;
    property?: string;
    visible?: boolean;
    restrict?: boolean;
}