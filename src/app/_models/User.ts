// import { Imagen } from './Imagen';
// import { Role } from './Role';

export class User {

    public constructor(init?: Partial<User>) {
        Object.assign(this, init);
    }

    id: string;
    identificacion: string;
    nombre: string;
    email: string;
    clave: string;
    telefono: string;
    direccion: string;
    imagen: any;
    habilitado: boolean;
    fechaHoraLog: Date;
    usuarioLog: string;
    rol: any;

}