export class Imagen {

    public constructor(init?: Partial<Imagen>) {
        Object.assign(this, init);
    }

    nombre: string;
    url: string;
}