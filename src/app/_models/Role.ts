export class Role {

    public constructor(init?: Partial<Role>) {
        Object.assign(this, init);
    }

    id: number;
    idUsuarioLog: number;
    nombre: string;
    fechaLog: Date;
    horaLog: any ;
    habilitado: boolean;

}