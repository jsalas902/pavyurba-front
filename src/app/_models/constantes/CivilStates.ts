export class CivilStates {

    public constructor(init?: Partial<CivilStates>) {
      Object.assign(this, init);
      CivilStates.Soltero = 'SOLTERO';
      CivilStates.Casado = 'CASADO';
      CivilStates.Divorciado = 'DIVORCIADO';
      CivilStates.Viudo = 'VIUDO';
      CivilStates.UnionLibre = 'UNION LIBRE';
    }
  
    static Soltero = 'SOLTERO';
    static Casado = 'CASADO';
    static Divorciado = 'DIVORCIADO';
    static Viudo = 'VIUDO';
    static UnionLibre = 'UNION LIBRE';
  }
  