export class AutonomousClasses {

    public constructor(init?: Partial<AutonomousClasses>) {
      Object.assign(this, init);
      AutonomousClasses.Empresa = 'EMPRESA';
      AutonomousClasses.Otros = 'OTROS';
    }
  
    static Empresa = 'EMPRESA';
    static Otros = 'OTROS';
  }
  