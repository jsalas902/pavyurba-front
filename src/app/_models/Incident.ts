import { Document } from '@app/_models/Document'
export class Incident {

    public constructor(init?: Partial<Incident>) {
        Object.assign(this, init);
    }

    id: number;
    tipo: string;
    incidencia: string;
    fechaDesde: Date;
    fechaHasta: Date;
    habilitado: boolean;
    usuarioLog: string ;
    documento: Document ;

}