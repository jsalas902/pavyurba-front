export class Document {

    public constructor(init?: Partial<Document>) {
        Object.assign(this, init);
    }

    base64: string;
    nombre: string;
    mimeType: string;
    tamano: number;

}