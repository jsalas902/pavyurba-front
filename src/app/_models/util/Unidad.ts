export class Unidad {

    public constructor(init?: Partial<Unidad>) {
        Object.assign(this, init);
    }

    id: string;
    texto: string;
}