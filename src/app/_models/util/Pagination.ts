export class Pagination {
    mensaje: any;

    public constructor(init?: Partial<Pagination>) {
        Object.assign(this, init);
    }

    current_page: number;
    current_page_aux: number;
    first_page_url: string;
    from: number;
    last_page: number;
    last_page_url: number;
    next_page_url: string;
    path: string;
    per_page: number;
    prev_page_url: string;
    to: number;
    total: number;
    totalCantidad: number;
    data: any;
}