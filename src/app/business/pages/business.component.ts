import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { BusinessService } from '@app/business/services/business.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Business } from '@app/business/models/Business';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Pagination } from '@app/_models';
import * as moment from 'moment';

@Component({
  selector: 'app-business',
  templateUrl: './business.component.html',
  styleUrls: ['./business.component.css']
})
export class BusinessComponent implements OnInit {
 
  @BlockUI('componentsBusiness') blockUIList: NgBlockUI;

  @Input() idBusinessListar: string;
 
  rows: Business[];
  rowsFiltered: Business[]; 
  pagination: Pagination;
  listaBusiness: Business[];
  cargaCompleta = false;
  edicion: boolean = false;
  titelModal: string;
  type: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  private modalRef;
  public menuSettingsConfig: any;
  public switch_expression: string;
  size: string;

  constructor(private toastr: ToastrService,
              private businessService: BusinessService,
              private confirmDialogService: ConfirmDialogService,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private modalService: NgbModal)
  {
    this.menuSettingsConfig = {
      vertical_menu: {
        items: [          
          {
            title: 'Datos',
            icon: 'pavyurba-icon-data',
            page: 'datos',
            isSelected: true
          },
          {
            title: 'Obras',
            icon: 'pavyurba-icon-plays',
            page: 'obras'
          },
          {
            title: 'Calendario',
            icon: 'pavyurba-icon-calendar',
            page: 'calendario'
          },
          {
            title: 'Incidencias',
            icon: 'pavyurba-icon-incidents',
            page: 'incidencias'
          },
          {
            title: 'Condiciones',
            icon: 'pavyurba-icon-terms',
            page: 'condiciones'
          },
          {
            title: 'Informes',
            icon: 'pavyurba-icon-informes',
            page: 'informes'
          },
          {
            title: 'Factura',
            icon: 'pavyurba-icon-facturas',
            page: 'factura'
          },
          {
            title: 'P. de Trabajo',
            icon: 'pavyurba-icon-pdetrabajo',
            page: 'p-de-trabajo'
          },
          {
            title: 'Hist. Cambios',
            icon: 'pavyurba-icon-terms',
            page: 'hist-cambios'
          },
        ]
      }    
    };
  }


  ngOnInit(): void {

    this.titelModal = 'Crear Empresa';
    this.switch_expression = 'datos';

    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = '';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;

    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    this.getAllData();
  }

  ngAfterViewInit(): void {
    this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();
    this.businessService.ListarTodos(this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
        });
        this.listaBusiness = list;
        this.listaBusiness = [...this.listaBusiness];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaBusiness;
    this.rowsFiltered = this.listaBusiness;
  }


  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }
  addModel(modal: any, row: Business) {
    if(row !== null){
      this.idBusinessListar =  row.id.toString();
      this.size = 'lg';
      this.edicion = true;
      this.titelModal = 'Editar Empresa';
    }else{
      this.idBusinessListar =  null;
      this.size = 'lg'; // Necesario para setear el size del modal  
      this.edicion = false;
      this.titelModal = 'Crear Empresa';  // Necesario para setear el titulo   
      this.switch_expression = 'datos'; // Necesario para setear el ngSwitch
    }
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: this.size });
  }
  onCloseModal(e) {
    this.modalRef.close();
    this.getAllData();
  }
  onChange($event, id: string) {
    this.confirmDialogService.confirmThis(`Seguro quiere realizar esta acción en el registro N° ${id} ?`, () =>  {
      let result = $event ? this.activarBusiness(id) : this.desactivarBusiness(id);
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario registro N° ${id}`);
    });
    
  }
  activarBusiness(id: string) {
    this.businessService.Activar(id).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Habilitado' , { timeOut: 2500, closeButton: true });
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  desactivarBusiness(id: string) {
    this.businessService.Desctivar(id).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Desactivado' , { timeOut: 2500, closeButton: true });
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  onPageActive(event){
    this.switch_expression = event;
  }

  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.businessService.ListarTodos(this.skip, this.take, this.query).subscribe(
      (data: Pagination) => {
        if (data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        console.log('lista:',data);
        var list =  data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
        });
        this.listaBusiness = list;
        this.listaBusiness = [...this.listaBusiness];

        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }
}
