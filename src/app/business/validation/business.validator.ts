export class BusinessValidator {
    public static BUSINESS_VALIDATION_MESSAGES = {
        'nombre': [
            { type: 'required', message: 'Nombre es requerido' },
            { type: 'minlength', message: 'El Nombre no puede tener menos de 3 caracteres' },
            { type: 'maxlength', message: 'El Nombre no puede tener más de 100 caracteres' }
        ],
        'identificacion': [
            { type: 'required', message: 'Identificacion es requerido' },
        ],
        'idProvincia': [
            { type: 'required', message: 'Provincia es requerido' },
        ],
        'idLocalidad': [
            { type: 'required', message: 'Localidad es requerido' },
        ],
        'direccion': [
            { type: 'required', message: 'Dirección es requerido' },
            { type: 'minlength', message: 'La dirección no puede tener menos de 5 caracteres' },
            { type: 'maxlength', message: 'La dirección no puede tener más de 500 caracteres' }
        ],
        'fueraMadrid': [
            { type: 'required', message: 'Fuera Madrid es requerido' },
        ],
        'telefono': [
            { type: 'required', message: 'Teléfono es requerido' },
        ],
        'email': [
            { type: 'required', message: 'E-mail es requerido' },
        ],
        'observaciones': [
            { type: 'maxlength', message: 'Las observaciones no pueden tener más de 500 caracteres' }
        ],
    }
}