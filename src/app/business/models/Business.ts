export class Business {

    public constructor(init?: Partial<Business>) {
        Object.assign(this, init);
    }

    id: number;
    identificacion: string;
    nombre: string;
    email: string;
    emailAdicional: string;
    telefono: string;
    telefonoAdicional: string;
    direccion: string;
    codigoPostal: string;
    observaciones: string;
    idLocalidad: number;
    idProvincia: string;
    habilitado: boolean;
    fechaHoraLog: Date;
    usuarioLog: string;
    selected: boolean;

}