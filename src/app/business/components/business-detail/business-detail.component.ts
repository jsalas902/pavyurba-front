import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Business } from '@app/business/models/Business';
import { Location } from '@app/locations/models/Location';

import { LocationsService } from '@app/locations/services/locations.service';
import { BusinessService } from '@app/business/services/business.service';
import { BusinessValidator } from "@app/business/validation/business.validator";
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-business-detail',
  templateUrl: './business-detail.component.html',
  styleUrls: ['./business-detail.component.css']
})
export class BusinessDetailComponent implements OnInit {
  
  @Input() idBusiness: string;
  @Output() closeModal = new EventEmitter<boolean>();
  @BlockUI('componentsBusinessDetail') blockUIList: NgBlockUI;

  business: Business = new Business();
  locations: Location;
  idProvincia: string = null;
  idLocalidad: string = null;
  provincias: any;
  businessForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  business_validation_messages = BusinessValidator.BUSINESS_VALIDATION_MESSAGES;
  // Modal location
  cargaCompleta = false;
  sizeid = 5;
  
  mensaje: string = '';
  mensajeDir: string = '';
  maxLength: number = 500;
  
  constructor(
    private toastr: ToastrService, 
    private locationsService: LocationsService,
    private businessService: BusinessService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.businessForm = this.formBuilder.group({
      id: [null],
      nombre: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      identificacion: [null, [Validators.required]],
      idProvincia: [null, [Validators.required]],
      idLocalidad: [null, [Validators.required]],
      direccion: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(500)]],
      codigoPostal: [null],
      email: [null],
      telefono: [null],
      emailAdicional: [null],
      telefonoAdicional: [null],
      observaciones: [null, [Validators.maxLength(500)]],
    });
   
    if (this.idBusiness != null) {
      this.edicion = true;
      this.sizeid = 3;
      this.obtenerBusiness();
    } else {
      this.sizeid = 5;
    }
    // this.obtenerLocations();
    this.obtenerProvincias();
  }

  obtenerBusiness() {
    this.businessService.ObtenerPorId(this.idBusiness).subscribe(
      data => {
        this.obtenerPatchBusiness(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  obtenerProvincias() {
    this.locationsService.ListarProvinciasCombo().subscribe(
      data => {
        this.provincias = data;
      },
      error => {
        // this.toastr.error(error);
        this.toastr.error(error.mensaje);
      }
    );
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  obtenerLocations() {
    this.locationsService.ListarLocalidadesCombo(this.idProvincia).subscribe(
      data => {
        this.locations = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  get f() { return this.businessForm.controls; }
  add() {
    this.submitted = true;
    console.log(this.businessForm.value);
    if (this.businessForm.invalid) {
        return;
    }

    this.business = this.businessForm.value;
    console.log(this.businessForm.value instanceof Business);
    
    this.reloadStart();
    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }

  }
  create(){
    let businessTem = new Business();
    businessTem = this.changeObject(this.business);
   
    this.businessService.Crear(businessTem).subscribe(
      data => {
        console.log('Crear: ',data);
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
        this.reloadStop();
      },
      error => {
        this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  edit(){
    let businessTem = new Business();
    businessTem = this.changeObject(this.business);

    this.businessService.Editar(businessTem).subscribe(
      data => {
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
        this.reloadStop();
      },
      error => {
        this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  reiniciarFormulario() {
    this.business = new Business();
    this.businessForm.reset();
    this.submitted = false;
  }
  obtenerPatchBusiness(data){
    this.business = this.changeObject(data);
    this.businessForm.patchValue({
      id: this.business.id,
      identificacion: this.business.identificacion,
      nombre: this.business.nombre,
      idLocalidad: this.business.idLocalidad,
      idProvincia: this.business.idProvincia,
      direccion: this.business.direccion,
      codigoPostal: this.business.codigoPostal,
      email: this.business.email,
      emailAdicional: this.business.emailAdicional,
      telefono: this.business.telefono,
      telefonoAdicional: this.business.telefonoAdicional,
      observaciones: this.business.observaciones,
    });
  }
  changeObject(business: Business) {
    let businessTem: Business = new Business();
    businessTem.id = business.id;
    businessTem.nombre = business.nombre;
    businessTem.identificacion = business.identificacion;
    businessTem.email = (business.email == "") ? null : business.email;
    // businessTem.emailAdicional = business.emailAdicional;
    businessTem.emailAdicional = (business.emailAdicional == "") ? null : business.emailAdicional;
    businessTem.telefono = (business.telefono == "") ? null : business.telefono;
    // businessTem.telefonoAdicional = business.telefonoAdicional;
    businessTem.telefonoAdicional = (business.telefonoAdicional == "") ? null : business.telefonoAdicional;
    businessTem.direccion = business.direccion;
    businessTem.codigoPostal = business.codigoPostal;
    businessTem.observaciones = business.observaciones;
    businessTem.idLocalidad = business.idLocalidad;
    businessTem.idProvincia = this.idProvincia = business.idProvincia;

    this.changeProvincia();
    return businessTem;
  }
  addLocation(modal){
    this.cargaCompleta = true;
    this.locationsService.addModelLocation(modal);
  }
  onSetModal(e) {
    this.obtenerLocations();
  }
  changeProvincia() {
    if(this.idProvincia != null) {
      this.obtenerLocations();
    } else {
      this.idLocalidad = null;
    }
  }
}
