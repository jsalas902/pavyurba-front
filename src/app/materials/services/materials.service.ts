import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { GenericService, AuthenticationService } from '@app/_services';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Pagination } from '@app/_models/util/Pagination';
import { Material } from '@app/materials/models/Material';

@Injectable({
  providedIn: 'root'
})
export class MaterialsService extends GenericService {

  constructor(private http: HttpClient,
    public authService: AuthenticationService,
    private injector: Injector) {
  super();
  }

  ListarTodos(skip: string, take: string, query: string): Observable<Pagination> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/materiales' + '?skip=' + skip + '&take=' + take + '&query=' + query, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }
  ListarMaterialesCombo(): Observable<any> {
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/maestros/listarMaterialesCombo', { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta.data;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  ObtenerPorId(id: string): Observable<Material> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.get(environment.apiUrl + '/materiales/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta.data;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }


  Crear(material: Material): Observable<any> {

    // this.headers = new HttpHeaders ({
    //   Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    // });
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));

    return this.http.post(environment.apiUrl + '/materiales', material, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  Editar(material: Material): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.put(environment.apiUrl + '/materiales/' + material.id, material, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  Eliminar(id: string): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.delete(environment.apiUrl + '/materiales/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  Activar(id: string): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.put(environment.apiUrl + '/materiales/activarMaterial/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }

  Desctivar(id: string): Observable<any> {

    this.headers = new HttpHeaders ({
      Authorization: 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS')
    });

    return this.http.put(environment.apiUrl + '/materiales/desactivarMaterial/' + id, { headers: this.headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }
}