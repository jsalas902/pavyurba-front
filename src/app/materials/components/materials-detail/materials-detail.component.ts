import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Material } from '@app/materials/models/Material';
import { Unidad } from '@app/_models/util/Unidad';

import { UnidadesService } from '@app/_services/unidades.service';
import { MaterialsService } from '@app/materials/services/materials.service';
import { MaterialsValidator } from "@app/materials/validation/materials.validator";

@Component({
  selector: 'app-materials-detail',
  templateUrl: './materials-detail.component.html',
  styleUrls: ['./materials-detail.component.css']
})
export class MaterialsDetailComponent implements OnInit {

  @Input() idMaterials: string;
  @Output() closeModal = new EventEmitter<boolean>();

  materials: Material = new Material();
  unidades: Unidad;
  materialsForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  materials_validation_messages = MaterialsValidator.MATERIALS_VALIDATION_MESSAGES;
  
  mensaje: string = '';
  maxLength: number = 500;

  
  constructor(
    private toastr: ToastrService,      
    private unidadesService: UnidadesService,
    private materialsService: MaterialsService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.materialsForm = this.formBuilder.group({
      id: [null],
      nombre: [null, [Validators.required, Validators.maxLength(100)]],
      medida: [null],
      precio: [null, [Validators.required]],
      observaciones: [null, [Validators.maxLength(500)]],
      idUnidadMedida: [null, [Validators.required]],
    });

    if (this.idMaterials != null) {
      this.edicion = true;
      this.obtenerMaterials();
    }
    this.obtenerUnidadesMedidas();
  }

  obtenerMaterials() {
    this.materialsService.ObtenerPorId(this.idMaterials).subscribe(
      data => {
        this.obtenerPatchMaterial(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  obtenerUnidadesMedidas() {
    this.unidadesService.ListarUnidadesMedidasCombo().subscribe(
      data => {
        this.unidades = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  get f() { return this.materialsForm.controls; }
  add() {
    this.submitted = true;

    if (this.materialsForm.invalid) {
      return;
    }
    
    this.materials = this.materialsForm.value;
    
    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }

  }
  create(){
    let materialsTem = new Material();
    materialsTem = this.changeObject(this.materials);
   
    this.materialsService.Crear(materialsTem).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'New' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
        this.closeModal.emit(true);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  edit(){
    let materialsTem = new Material();
    materialsTem = this.changeObject(this.materials);

    this.materialsService.Editar(materialsTem).subscribe(
      data => {
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  reiniciarFormulario() {
    this.materials = new Material();
    this.materialsForm.reset();
    this.submitted = false;
  }

  obtenerPatchMaterial(data){
    this.materials = this.changeObject(data);
    this.materialsForm.patchValue({
      id: this.materials.id,
      nombre: this.materials.nombre,
      precio: this.materials.precio,
      medida: this.materials.medida,
      idUnidadMedida: this.materials.idUnidadMedida,
      observaciones: this.materials.observaciones,
    });
  }
  changeObject(materials: Material) {
    let materialsTem: Material = new Material();
    materialsTem.id = materials.id;
    materialsTem.nombre = materials.nombre;
    materialsTem.precio = materials.precio;
    materialsTem.medida = materials.medida;
    materialsTem.idUnidadMedida = materials.idUnidadMedida;
    materialsTem.observaciones = materials.observaciones;
    return materialsTem;
  }
}