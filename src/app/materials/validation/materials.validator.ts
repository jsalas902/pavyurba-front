export class MaterialsValidator {
    public static MATERIALS_VALIDATION_MESSAGES = {        
        'nombre': [
            { type: 'required', message: 'Nombre es requerido' },
            { type: 'maxlength', message: 'Localidad no puede tener mas de 100 caracteres' }
        ],
        'medida': [
            { type: 'required', message: 'Medida es requerido' },
        ],
        'precio': [
            { type: 'required', message: 'Precio es requerido' },
        ],
        'observaciones': [
            { type: 'maxlength', message: 'Observaciones no puede tener mas de 500 caracteres' }
        ],
        'idUnidadMedida': [
            { type: 'required', message: 'Unidad de Medida es requerido' },
        ],
        'idProveedor': [
            { type: 'required', message: 'Proveedor es requerido' },
        ],
    }
}