export class Material {

    public constructor(init?: Partial<Material>) {
        Object.assign(this, init);
    }

    id: number;
    nombre: string;
    medida: number;
    precio: number;
    observaciones: string;
    unidadMedida: string;
    proveedor: string;
    idUnidadMedida: number ;
    idProveedor: number ;
    habilitado: boolean;
    fechaHoraLog: Date;
    usuarioLog: string ;
    selected: boolean;

}