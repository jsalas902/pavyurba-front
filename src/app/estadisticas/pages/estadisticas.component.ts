import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { EstadisticasService } from '@app/estadisticas/services/estadisticas.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { NgbDateParserFormatter, NgbDatepickerI18n, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { Pagination } from '@app/_models/util/Pagination';
import { Obra } from '@app/obras/models/Obra';
import { ObrasService } from '@app/obras/services/obras.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbDateCustomParserFormatter } from '@app/_helpers/dateformat';
import * as moment from 'moment';
import { Client } from '@app/clients/models/Client';
import { Builder } from '@app/builders/models/Builder';
import { ClientsService } from '@app/clients/services/clients.service';
import { BuildersService } from '@app/builders/services/builders.service';
import { CustomDatepickerI18nService, I18n } from '@app/calendarios/services/custom-datepicker-i18n.service';

@Component({
  selector: 'app-estadisticas',
  templateUrl: './estadisticas.component.html',
  styleUrls: ['./estadisticas.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18nService }
  ]
})
export class EstadisticasComponent implements OnInit {
  
  @BlockUI('componentsEstadisticas') blockUIList: NgBlockUI;

  rows: any[];
  rowsFiltered: any[];
  allRowsSelected: boolean;
  pagination: Pagination;
  listaEstadisticas: any[];
  cargaCompleta = false;
  titelModal: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  idObras: string;
  nombreObra: string;
  idVinculacion: string;
  isFacturado: boolean = false;
  facturado: string = String(this.isFacturado);
  fechaDesde;
  fechaHasta;
  d;
  d1;
  dateNow = moment().format("YYYY-MM-DD");
  totalIngresos: number = 0;
  totalPartesTrabajos: number = 0;
  totalDiferenciasNomina: number = 0;
  totalGastos: number = 0;
  totalGastosGenerales: number = 0;
  totalGastosGeneralesAdministrativos: number = 0;
  totalSeguridadSocial: number = 0;
  totalDiferencias: number = 0;
  totalDias: number = 0;
  validTotalIngresos: boolean = false;
  validTotalPartesTrabajos: boolean = false;
  validTotalDiferenciasNomina: boolean = false;
  validTotalGastos: boolean = false;
  validTotalGastosGenerales: boolean = false;
  validTotalGastosGeneralesAdministrativos: boolean = false;
  validTotalSeguridadSocial: boolean = false;
  validTotalDiferencias: boolean = false;
  validTotalDias: boolean = false;
  readonly DELIMITER = '-';
  clientes: Client;
  constructoras: Builder; 
  idCliente: number;  
  idConstructora: number; 
  private modalRef;

  constructor(private toastr: ToastrService,
              private estadisticasService: EstadisticasService,
              private clientsService: ClientsService,
              private buildersService: BuildersService,
              private confirmDialogService: ConfirmDialogService,
              private obrasService: ObrasService,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private modalService: NgbModal,
              private formBuilder: FormBuilder,
              ) {
                this.obtenerClientes();
                this.obtenerConstructoras();
}


  ngOnInit(): void {
    this.allRowsSelected = false;
    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = '';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;

    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
    // this.fechaDesde = this.fromModel(this.dateNow);
    // this.fechaHasta = this.fromModel(this.dateNow);
    this.getAllData();
  }

  ngAfterViewInit(): void {
    this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();

    var data = {
      // fechaDesde: this.toModel(this.fechaDesde),
      // fechaHasta: this.toModel(this.fechaHasta),
      idCliente: this.idCliente,
      idConstructora: this.idConstructora
    }
    this.estadisticasService.ListarTodos(data, this.skip, this.take, this.query).subscribe(
      (data: any) => {
        if (data.data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalIngresos = (data.data.totalIngresos != null) ? data.data.totalIngresos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalIngresos;
        this.totalPartesTrabajos = (data.data.totalPartesTrabajos != null) ? data.data.totalPartesTrabajos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalPartesTrabajos;
        this.totalDiferenciasNomina = (data.data.totalDiferenciasNomina != null) ? data.data.totalDiferenciasNomina.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalDiferenciasNomina;
        this.totalGastos = (data.data.totalGastos != null) ? data.data.totalGastos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalGastos;
        this.totalGastosGenerales = (data.data.totalGastosGenerales != null) ? data.data.totalGastosGenerales.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalGastosGenerales;
        this.totalGastosGeneralesAdministrativos = (data.data.totalGastosGeneralesAdministrativos != null) ? data.data.totalGastosGeneralesAdministrativos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalGastosGeneralesAdministrativos;
        this.totalSeguridadSocial = (data.data.totalSeguridadSocial != null) ? data.data.totalSeguridadSocial.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalSeguridadSocial;
        this.totalDiferencias = (data.data.totalDiferencias != null) ? data.data.totalDiferencias.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalDiferencias;
        this.totalDias = data.data.totalDias;

        data.data.data.forEach(element => {
          element.ingresos = (element.ingresos != null) ? element.ingresos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.ingresos;
          element.partesTrabajos = (element.partesTrabajos != null) ? element.partesTrabajos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.partesTrabajos;
          element.diferenciasNomina = (element.diferenciasNomina != null) ? element.diferenciasNomina.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.diferenciasNomina;
          element.gastos = (element.gastos != null) ? element.gastos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.gastos;
          element.gastosGenerales = (element.gastosGenerales != null) ? element.gastosGenerales.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.gastosGenerales;
          element.gastosGeneralesAdministrativos = (element.gastosGeneralesAdministrativos != null) ? element.gastosGeneralesAdministrativos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.gastosGeneralesAdministrativos;
          element.seguridadSocial = (element.seguridadSocial != null) ? element.seguridadSocial.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.seguridadSocial;
          element.diferencias = (element.diferencias != null) ? element.diferencias.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.diferencias;
          // element.dias = (element.dias != null) ? element.dias.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.dias;
        });

        this.validTotalIngresos = (data.data.totalIngresos != null) ? true : false;
        this.validTotalPartesTrabajos = (data.data.totalPartesTrabajos != null) ? true : false;
        this.validTotalDiferenciasNomina = (data.data.totalDiferenciasNomina != null) ? true : false;
        this.validTotalGastos = (data.data.totalGastos != null) ? true : false;
        this.validTotalGastosGenerales = (data.data.totalGastosGenerales != null) ? true : false;
        this.validTotalGastosGeneralesAdministrativos = (data.data.totalGastosGeneralesAdministrativos != null) ? true : false;
        this.validTotalSeguridadSocial = (data.data.totalSeguridadSocial != null) ? true : false;
        this.validTotalDiferencias = (data.data.totalDiferencias != null) ? true : false;
        this.validTotalDias = (data.data.totalDias != null) ? true : false;
        console.log('lista:',data);
        this.listaEstadisticas = data.data.data;
        this.listaEstadisticas = [...this.listaEstadisticas];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaEstadisticas;
    this.rowsFiltered = this.listaEstadisticas;
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, row: any) {
    this.idObras = row.id.toString();
    this.nombreObra = row.nombre
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: 'lg' });    
  }
  addModelObraID(modal: any, row: any) {
    this.idObras = row.id.toString();
    this.idVinculacion = (row.idVinculacion != null) ? row.idVinculacion : null;
    this.nombreObra = row.nombre;
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: 'md' });    
  }

  addModelListarPorObraID(modal: any, row: any) {
    this.idObras = row.id.toString();
    this.idVinculacion = (row.idVinculacion != null) ? row.idVinculacion : null;
    this.nombreObra = row.nombre;
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: 'lg' });    
  }
  onCloseModal(e) {
    this.modalRef.close();
    this.getAllData();
  }
  downloadFile(row) {
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = row.archivo.url;
    a.target = "_blank";
    a.download = "TEST";
    a.click();
    a.remove();
  }
  
  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 

    var data = {
      // fechaDesde: this.toModel(this.fechaDesde),
      // fechaHasta: this.toModel(this.fechaHasta),
      idCliente: this.idCliente,
      idConstructora: this.idConstructora
    }

    this.estadisticasService.ListarTodos(data, this.skip, this.take, this.query).subscribe(
      (data: any) => {
        if (data.data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalIngresos = (data.data.totalIngresos != null) ? data.data.totalIngresos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalIngresos;
        this.totalPartesTrabajos = (data.data.totalPartesTrabajos != null) ? data.data.totalPartesTrabajos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalPartesTrabajos;
        this.totalDiferenciasNomina = (data.data.totalDiferenciasNomina != null) ? data.data.totalDiferenciasNomina.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalDiferenciasNomina;
        this.totalGastos = (data.data.totalGastos != null) ? data.data.totalGastos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalGastos;
        this.totalGastosGenerales = (data.data.totalGastosGenerales != null) ? data.data.totalGastosGenerales.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalGastosGenerales;
        this.totalGastosGeneralesAdministrativos = (data.data.totalGastosGeneralesAdministrativos != null) ? data.data.totalGastosGeneralesAdministrativos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalGastosGeneralesAdministrativos;
        this.totalSeguridadSocial = (data.data.totalSeguridadSocial != null) ? data.data.totalSeguridadSocial.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalSeguridadSocial;
        this.totalDiferencias = (data.data.totalDiferencias != null) ? data.data.totalDiferencias.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : data.data.totalDiferencias;
        this.totalDias = data.data.totalDias;

        data.data.data.forEach(element => {
          element.ingresos = (element.ingresos != null) ? element.ingresos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.ingresos;
          element.partesTrabajos = (element.partesTrabajos != null) ? element.partesTrabajos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.partesTrabajos;
          element.diferenciasNomina = (element.diferenciasNomina != null) ? element.diferenciasNomina.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.diferenciasNomina;
          element.gastos = (element.gastos != null) ? element.gastos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.gastos;
          element.gastosGenerales = (element.gastosGenerales != null) ? element.gastosGenerales.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.gastosGenerales;
          element.gastosGeneralesAdministrativos = (element.gastosGeneralesAdministrativos != null) ? element.gastosGeneralesAdministrativos.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.gastosGeneralesAdministrativos;
          element.seguridadSocial = (element.seguridadSocial != null) ? element.seguridadSocial.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.seguridadSocial;
          element.diferencias = (element.diferencias != null) ? element.diferencias.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.diferencias;
          // element.dias = (element.dias != null) ? element.dias.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.dias;
        });

        this.validTotalIngresos = (data.data.totalIngresos != null) ? true : false;
        this.validTotalPartesTrabajos = (data.data.totalPartesTrabajos != null) ? true : false;
        this.validTotalDiferenciasNomina = (data.data.totalDiferenciasNomina != null) ? true : false;
        this.validTotalGastos = (data.data.totalGastos != null) ? true : false;
        this.validTotalGastosGenerales = (data.data.totalGastosGenerales != null) ? true : false;
        this.validTotalGastosGeneralesAdministrativos = (data.data.totalGastosGeneralesAdministrativos != null) ? true : false;
        this.validTotalSeguridadSocial = (data.data.totalSeguridadSocial != null) ? true : false;
        this.validTotalDiferencias = (data.data.totalDiferencias != null) ? true : false;
        this.validTotalDias = (data.data.totalDias != null) ? true : false;
        console.log('lista:',data);
        this.listaEstadisticas = data.data.data;
        this.listaEstadisticas = [...this.listaEstadisticas];

        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }
  
  toModel(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format();
  }
  toModelFix(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format('DD-MM-YYYY');
  }

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[2], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[0], 10)
      };
    }
    return null;
  }

  changeDate() {
    this.getAllData();
  }

  deleteVinculada(row: any) {
    this.confirmDialogService.confirmThis(`Seguro quiere desvincular en la obra "${row.nombre}" ?`, () =>  {
      this.estadisticasService.desvincularObra(row).subscribe(
        data => {
          this.toastr.success(`${data.mensaje}`,'Eliminar' , { timeOut: 2500, closeButton: true });
          this.getAllData();
        },
        error => {
          // this.reloadStop();
          // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
          this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
      });
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario`);
      this.getAllData();
    });
  }

  setClientes(e) {
    this.getAllData();
  }

  setConstructoras(e) {
    this.getAllData();
  }

  async obtenerClientes() {
    // this.business = await this.businessService.ListarEmpresasCombo().toPromise();
    this.clientsService.ListarClientesCombo().subscribe(
      data => {
        this.clientes = data;
        // this.idObra = (data[0]) ? data[0].id : null
        // this.getAllData();
      },
      error => {
        this.toastr.error(error);
      }
    );
  }

  async obtenerConstructoras() {
    // this.business = await this.businessService.ListarEmpresasCombo().toPromise();
    this.buildersService.ListarConstructorasCombo().subscribe(
      data => {
        this.constructoras = data;
        // this.idProveedor = (data[0]) ? data[0].id : null
        // this.getAllData();
      },
      error => {
        this.toastr.error(error);
      }
    );
  }
}
