import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxDatatableFooterModule } from 'ngx-datatable-footer';
import { NgSelectModule } from '@ng-select/ng-select';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';
import { CardModule } from '../content/partials/general/card/card.module';
import { BreadcrumbModule } from '../_layout/breadcrumb/breadcrumb.module';
import { BlockUIModule } from 'ng-block-ui';
import { BlockTemplateComponent } from '../_layout/blockui/block-template.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared/shared.module';

import { EstadisticasComponent } from './pages/estadisticas.component';
import { AlbaranesObrasComponent } from './components/albaranes-obras/albaranes-obras.component';
import { CalendariosObrasComponent } from './components/calendarios-obras/calendarios-obras.component';
import { NominasObrasComponent } from './components/nominas-obras/nominas-obras.component';
import { PartesTrabajosObrasComponent } from './components/partes-trabajos-obras/partes-trabajos-obras.component';
import { GastosGeneralesObrasComponent } from './components/gastos-generales-obras/gastos-generales-obras.component';
import { ProformasFacturasObrasComponent } from './components/proformas-facturas-obras/proformas-facturas-obras.component';
import { ProformasFacturasObrasDetailComponent } from './components/proformas-facturas-obras/proformas-facturas-obras-detail/proformas-facturas-obras-detail.component';
import { PartesTrabajosObrasDetailComponent } from './components/partes-trabajos-obras/partes-trabajos-obras-detail/partes-trabajos-obras-detail.component';
import { NominasObrasDetailComponent } from './components/nominas-obras/nominas-obras-detail/nominas-obras-detail.component';
import { GastosGeneralesObrasDetailComponent } from './components/gastos-generales-obras/gastos-generales-obras-detail/gastos-generales-obras-detail.component';
import { CalendariosObrasDetailComponent } from './components/calendarios-obras/calendarios-obras-detail/calendarios-obras-detail.component';
import { AlbaranesObrasDetailComponent } from './components/albaranes-obras/albaranes-obras-detail/albaranes-obras-detail.component';
import { VincularObrasComponent } from './components/vincular-obras/vincular-obras.component';
import { ObrasVinculadasComponent } from './components/obras-vinculadas/obras-vinculadas.component';



@NgModule({
  declarations: [EstadisticasComponent, AlbaranesObrasComponent, NominasObrasComponent, PartesTrabajosObrasComponent, GastosGeneralesObrasComponent, ProformasFacturasObrasComponent, ProformasFacturasObrasDetailComponent, PartesTrabajosObrasDetailComponent, NominasObrasDetailComponent, GastosGeneralesObrasDetailComponent, AlbaranesObrasDetailComponent, VincularObrasComponent, ObrasVinculadasComponent],
  exports: [RouterModule, EstadisticasComponent],
  imports: [
    CommonModule,
    CardModule,
    NgbModule,
    BreadcrumbModule,
    NgSelectModule,
    UiSwitchModule,
    FormsModule,
    ReactiveFormsModule,
    ClipboardModule,
    PerfectScrollbarModule,
    NgxDatatableModule,
    BlockUIModule.forRoot({
      template: BlockTemplateComponent
    }),
    NgxDatatableFooterModule.forRoot(),
    SharedModule,
    RouterModule.forChild([
      {
        path: 'list',
        component: EstadisticasComponent
      },
    ]),
  ]
})
export class EstadisticasModule { }
