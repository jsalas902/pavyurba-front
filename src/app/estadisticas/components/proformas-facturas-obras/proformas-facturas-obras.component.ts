import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Factura } from '@app/facturas/models/Factura';
import * as moment from 'moment';

import { Router } from '@angular/router';
import { Pagination } from '@app/_models';
import { EstadisticasService } from '@app/estadisticas/services/estadisticas.service';

import { FacturaTypes } from '@app/facturas/models/constantes/FacturaTypes';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-proformas-facturas-obras',
  templateUrl: './proformas-facturas-obras.component.html',
  styleUrls: ['./proformas-facturas-obras.component.css']
})
export class ProformasFacturasObrasComponent implements OnInit {

  @BlockUI('componentsFacturas') blockUIList: NgBlockUI;

  @Input() idObras: string;  


  
  rows: Factura[];
  rowsFiltered: Factura[];
  
  pagination: Pagination;
  listaFacturas: Factura[];
  cargaCompleta = false;
  titelModal: string;
  type: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  editFactura: number;
  idFactura: string;
  totalFactura: string;
  validTotalFactura: boolean = false;
  private modalRef;

  constructor(private toastr: ToastrService,
              private estadisticasService: EstadisticasService,
              private confirmDialogService: ConfirmDialogService,
              private router: Router,
              private modalService: NgbModal,
              private cdRef: ChangeDetectorRef
              
              ) {
}


  ngOnInit(): void {

    this.titelModal = 'Subida de Factura';

    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = '';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;

    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    this.getAllData();
  }

  ngAfterViewInit(): void {
    // this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();
    this.estadisticasService.ListarFacturasPorObras(this.idObras, this.skip, this.take, this.query).subscribe(
      (data: any) => {
        if (data.data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
            } 
          } 
        } 
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalFactura = Number(data.data.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});        
        this.validTotalFactura = (data.data.total != null) ? true : false;
        console.log('lista:',data);
        var list =  data.data.data;
        if(list.length > 0) {
          list.forEach(element => {
            // moment.locale('es');
            element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
            element.total = (element.total != null) ? Number(element.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.total;
            element.estatus = element.estatus != "FINALIZADO" ? "NO" : "SI";
            switch (element.mes) {
              case "1":
                element.mes = FacturaTypes.MES1
                break;
              case "2":
                element.mes = FacturaTypes.MES2
                break;
              case "3":
                element.mes = FacturaTypes.MES3
                break;
              case "4":
                element.mes = FacturaTypes.MES4
                break;
              case "5":
                element.mes = FacturaTypes.MES5
                break;
              case "6":
                element.mes = FacturaTypes.MES6
                break;
              case "7":
                element.mes = FacturaTypes.MES7
                break;
              case "8":
                element.mes = FacturaTypes.MES8
                break;
              case "9":
                element.mes = FacturaTypes.MES9
                break;
              case "10":
                element.mes = FacturaTypes.MES10
                break;
              case "11":
                element.mes = FacturaTypes.MES11
                break;
              case "12":
                element.mes = FacturaTypes.MES12
                break;
            
              default:
                element.mes = FacturaTypes.MES1
                break;
            }
          });
          this.listaFacturas = list;
          this.listaFacturas = [...this.listaFacturas];
        }

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaFacturas;
    this.rowsFiltered = this.listaFacturas;
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, editar: number, row: any) {
    this.titelModal = "Editar Factura";
    this.idObras = this.idObras;
    this.cargaCompleta = true;
    this.editFactura = editar;
    this.idFactura = (row != null) ? row.id : null;
    var size = "md";
    
    // this.estadisticasService.addModelFactura(modal, size);
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown' });
  }

  
  onCloseModal(e) {
    this.modalRef.close();
    this.getAllData();
  }
  
  // onSetModal(e) {
  //   console.log('evento setclose:',e);
  //   this.getAllData();
  // }
  downloadFile(row) {
    let a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = row.archivo.url;
    a.target = "_blank";
    a.download = "TEST";
    a.click();
    a.remove();
  }

  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.estadisticasService.ListarFacturasPorObras(this.idObras, this.skip, this.take, this.query).subscribe(
      (data: any) => {
        if (data.data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalFactura = Number(data.data.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});        
        this.validTotalFactura = (data.data.total != null) ? true : false;
        console.log('lista:',data);
        var list =  data.data.data;
        if(list.length > 0) {          
          list.forEach(element => {
            // moment.locale('es');
            element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
            element.total = (element.total != null) ? Number(element.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.total;
            element.estatus = element.estatus != "FINALIZADO" ? "NO" : "SI";
            switch (element.mes) {
              case "1":
                element.mes = FacturaTypes.MES1
                break;
              case "2":
                element.mes = FacturaTypes.MES2
                break;
              case "3":
                element.mes = FacturaTypes.MES3
                break;
              case "4":
                element.mes = FacturaTypes.MES4
                break;
              case "5":
                element.mes = FacturaTypes.MES5
                break;
              case "6":
                element.mes = FacturaTypes.MES6
                break;
              case "7":
                element.mes = FacturaTypes.MES7
                break;
              case "8":
                element.mes = FacturaTypes.MES8
                break;
              case "9":
                element.mes = FacturaTypes.MES9
                break;
              case "10":
                element.mes = FacturaTypes.MES10
                break;
              case "11":
                element.mes = FacturaTypes.MES11
                break;
              case "12":
                element.mes = FacturaTypes.MES12
                break;
            
              default:
                element.mes = FacturaTypes.MES1
                break;
            }
          });
          this.listaFacturas = list;
          this.listaFacturas = [...this.listaFacturas];
        }

        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }
}
