import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Factura } from '@app/facturas/models/Factura';
import * as moment from 'moment';
import { FacturasService } from '@app/facturas/services/facturas.service';
import { FacturasValidator } from "@app/facturas/validation/facturas.validator";
import { FacturaTypes } from '@app/facturas/models/constantes/FacturaTypes';
import { ObrasService } from '@app/obras/services/obras.service';
import { Obra } from '@app/obras/models/Obra';
import { EstadisticasService } from '@app/estadisticas/services/estadisticas.service';

@Component({
  selector: 'app-proformas-facturas-obras-detail',
  templateUrl: './proformas-facturas-obras-detail.component.html',
  styleUrls: ['./proformas-facturas-obras-detail.component.css']
})
export class ProformasFacturasObrasDetailComponent implements OnInit {

  @Input() idObras: string;
  @Input() titel: string;
  @Input() idFactura: string;
  @Input() skip: string;
  @Input() take: string;
  @Input() query: string;
  @Output() closeModal= new EventEmitter<boolean>();
  @Output() setModal = new EventEmitter<boolean>();

  factura: Factura = new Factura();
  obras: Obra;
  facturasForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  facturaTypes: any;
  titleDocumento: string;
  sizeArchivo: number;
  typeArchivo: string;
  baseArchivo: string | ArrayBuffer;
  archivoValid: boolean = false;
  annoActual: string = moment().format('YYYY');
  annoOrigen: number;
  annoActualN: number;
  mes: number;  
  anno: number; 
  mesArray: any[] = [];
  annosArray: any[] = [];
  facturas_validation_messages = FacturasValidator.FACTURAS_VALIDATION_MESSAGES;

  
  constructor(
    private toastr: ToastrService, 
    private facturasService: FacturasService,
    private estadisticasService: EstadisticasService,
    private obrasService: ObrasService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.obtenerObras();

    this.mesArray = [
      {"texto":"Enero","valor":1},
      {"texto":"Febrero","valor":2},
      {"texto":"Marzo","valor":3},
      {"texto":"Abril","valor":4},
      {"texto":"Mayo","valor":5},
      {"texto":"Junio","valor":6},
      {"texto":"Julio","valor":7},
      {"texto":"Agosto","valor":8},
      {"texto":"Septiembre","valor":9},
      {"texto":"Octubre","valor":10},
      {"texto":"Noviembre","valor":11},
      {"texto":"Diciembre","valor":12},
    ];
    this.annoActualN = Number(this.annoActual); 
    for (let index = 2018; index <= this.annoActualN + 1; index++) {
      this.annosArray.push({"texto":index});
    }

    this.anno = this.annoActualN;
    this.mes = Number(moment().format('M'));
    
    this.facturasForm = this.formBuilder.group({
      numero: [null],
      tipo: [null],
      id: [this.idFactura],
      idObra: [null, [Validators.required]],
      total: [null, [Validators.required]],
      mes: [null, [Validators.required]],
      ano: [null, [Validators.required]],
      archivo: [null],
    });

    this.facturaTypes = [{"texto":FacturaTypes.PROFORMA}, {"texto":FacturaTypes.FACTURA}];
    this.obtenerFacturas();
  }

  get f() { return this.facturasForm.controls; }
  add() {
    this.submitted = true;

    if (this.facturasForm.invalid) {
      return;
    } 
    
    this.factura = this.facturasForm.value;

    this.update(); 
  }
  
  update(){
    let facturasTem = new Factura();
    facturasTem = this.changeObject(this.factura);
  
    this.estadisticasService.EditaFactura(facturasTem).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
        this.closeModal.emit(false);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  obtenerObras() {
    this.obrasService.ListarObrasCombo().subscribe(
      data => {
        this.obras = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  reiniciarFormulario() {
    this.factura = new Factura();
    this.facturasForm.reset();
    this.submitted = false;
  }
  changeObject(factura: any) {
    console.log(factura);
    let facturasTem: Factura = new Factura();
    facturasTem.id = factura.id;
    facturasTem.idObra = factura.idObra;
    facturasTem.numero = factura.numero;
    facturasTem.tipo = factura.tipo;
    facturasTem.total = factura.total;
    facturasTem.ano = factura.ano;
    facturasTem.mes = factura.mes;
    facturasTem.archivo = factura.archivo;

    return facturasTem;
  }

  obtenerFacturas() {
    this.facturasService.GetFacturaPorId(this.idFactura, this.skip, this.take, this.query).subscribe(
      data => {
        console.log(data);
        this.obtenerPatchFactura(data.data[0]);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  obtenerPatchFactura(data){
    console.log(data);
    this.factura = this.changeObject(data);
    this.facturasForm.patchValue({
      id: this.idFactura,
      numero: this.factura.numero,
      tipo: this.factura.tipo,
      idObra: this.idObras,
      total: this.factura.total,
      mes: this.factura.mes,
      ano: this.factura.ano,
      estatus: this.factura.estatus,
      archivo: this.factura.archivo,
    });
  }
}
