import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { ParteTrabajo } from '@app/parte-trabajo/models/ParteTrabajo';
import * as moment from 'moment';

import { Router } from '@angular/router';
import { Pagination } from '@app/_models';
import { ParteTrabajoService } from '@app/parte-trabajo/services/parte-trabajo.service';

import { FacturaTypes } from '@app/facturas/models/constantes/FacturaTypes';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-partes-trabajos-obras',
  templateUrl: './partes-trabajos-obras.component.html',
  styleUrls: ['./partes-trabajos-obras.component.css']
})
export class PartesTrabajosObrasComponent implements OnInit {

  @BlockUI('componentsParteTrabajo') blockUIList: NgBlockUI;

  @Input() idObras: string;  

  rows: ParteTrabajo[];
  rowsFiltered: ParteTrabajo[];
  
  pagination: Pagination;
  listaParteTrabajo: ParteTrabajo[];
  cargaCompleta = false;
  titelModal: string;
  type: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  idParteTrabajo: string;
  totalParteTrabajo: string;
  validTotalParteTrabajo: boolean = false;
  conFeriadosFestivos: boolean = false;
  private modalRef;

  constructor(private toastr: ToastrService,
              private parteTrabajoService: ParteTrabajoService,
              private confirmDialogService: ConfirmDialogService,
              private modalService: NgbModal,
              private router: Router,
              private cdRef: ChangeDetectorRef
              
              ) {
}


  ngOnInit(): void {
    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = '';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;

    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    this.getAllData();
  }

  ngAfterViewInit(): void {
    // this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();
    this.parteTrabajoService.ListarParteTrabajoPorObras(this.idObras, this.conFeriadosFestivos, this.skip, this.take, this.query).subscribe(
      (data: any) => {
        if (data.data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalParteTrabajo = Number(data.data.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});        
        this.validTotalParteTrabajo = (data.data.total != null) ? true : false;
        console.log('lista:',data);
        var list =  data.data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fecha = moment(element.fecha).format('DD-MM-YYYY');          
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
          element.metros = (element.metros != null) ? Number(element.metros).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.metros;
          element.valor = (element.valor != null) ? Number(element.valor).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.valor;
          element.precio = (element.precio != null) ? Number(element.precio).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.precio;
        });
        this.listaParteTrabajo = list;
        this.listaParteTrabajo = [...this.listaParteTrabajo];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaParteTrabajo;
    this.rowsFiltered = this.listaParteTrabajo;
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, row: any) {
    this.titelModal = (row != null) ? "Editar Parte de Trabajo" : "Crear Parte de Trabajo";
    this.idObras = this.idObras;
    this.cargaCompleta = true;
    this.idParteTrabajo = (row != null) ? row.id : null;
    // this.parteTrabajoService.addModelParteTrabajo(modal);
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown' });
  }
  onSetModal(e) {
    this.getAllData();
  }
  onCloseModal(e) {
    this.modalRef.close();
    this.getAllData();
  }
  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.parteTrabajoService.ListarParteTrabajoPorObras(this.idObras, this.conFeriadosFestivos, this.skip, this.take, this.query).subscribe(
      (data: any) => {
        if (data.data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalParteTrabajo = Number(data.data.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});        
        this.validTotalParteTrabajo = (data.data.total != null) ? true : false;
        console.log('lista:',data);
        var list =  data.data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fecha = moment(element.fecha).format('DD-MM-YYYY');  
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');    
          element.metros = (element.metros != null) ? Number(element.metros).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.metros;
          element.valor = (element.valor != null) ? Number(element.valor).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.valor;      
          element.precio = (element.precio != null) ? Number(element.precio).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.precio;      
        });
        this.listaParteTrabajo = list;
        this.listaParteTrabajo = [...this.listaParteTrabajo];

        this.getTabledata();
        this.reloadStop();

        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }
}
