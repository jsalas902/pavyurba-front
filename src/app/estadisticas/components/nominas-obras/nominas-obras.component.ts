import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { NominasService } from '@app/nominas/services/nominas.service';
import { BusinessService } from '@app/business/services/business.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Business } from '@app/business/models/Business';
import { Nominas } from '@app/nominas/models/Nominas';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { Pagination } from '@app/_models';
import * as moment from 'moment';

@Component({
  selector: 'app-nominas-obras',
  templateUrl: './nominas-obras.component.html',
  styleUrls: ['./nominas-obras.component.css']
})
export class NominasObrasComponent implements OnInit {
 
  @BlockUI('componentsNominas') blockUIList: NgBlockUI;

  @Input() idObras: string;
  @Input() tipoFiltroGeneral: string;
  @Input() nomina: string;
  @Input() idNominaListar: string;
 
  rows: Nominas[];
  rowsFiltered: Nominas[]; 
  business: Business;
  pagination: Pagination;
  listaNominas: Nominas[];
  cargaCompleta = false;
  edicion: boolean = false;
  titelModal: string;
  type: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  private modalRef;
  size: string;  
  idEmpresa: number;  
  mes: number;  
  anno: number;  
  annoActual: string = moment().format('YYYY');
  annoActualN: number;
  mesArray: any[] = [];
  annosArray: any[] = [];

  totalEmbargo: string;
  totalIrpf: string;
  totalTotal: string;
  totalTotalBruto: string;
  totalFeriadosFestivos: string;
  totalTotalSeguridadSocial: string;
  validTotalEmbargo: boolean = false;
  validTotalIrpf: boolean = false;
  validTotalTotal: boolean = false;
  validTotalBruto: boolean = false;
  validTotalFeriadosFestivos: boolean = false;
  validTotalSeguridadSocial: boolean = false;

  constructor(private toastr: ToastrService,
              private nominasService: NominasService,
              private businessService: BusinessService,
              private confirmDialogService: ConfirmDialogService,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private modalService: NgbModal){

    this.mesArray = [
      {"texto":"Enero","valor":1},
      {"texto":"Febrero","valor":2},
      {"texto":"Marzo","valor":3},
      {"texto":"Abril","valor":4},
      {"texto":"Mayo","valor":5},
      {"texto":"Junio","valor":6},
      {"texto":"Julio","valor":7},
      {"texto":"Agosto","valor":8},
      {"texto":"Septiembre","valor":9},
      {"texto":"Octubre","valor":10},
      {"texto":"Noviembre","valor":11},
      {"texto":"Diciembre","valor":12},
    ];
    this.annoActualN = Number(this.annoActual); 
    for (let index = 2018; index <= this.annoActualN + 1; index++) {
      this.annosArray.push({"texto":index});
    }
    this.anno = this.annoActualN;
    this.mes = Number(moment().format('M'));
    this.obtenerBusiness();
  }


  ngOnInit(): void {

    this.titelModal = 'Alta de Nómina';

    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.current_page_aux = 1;
    this.pagination.per_page = 10;
    this.query = '';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;

    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };  
    
  }

  ngAfterViewInit(): void {
    this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();
    this.nominasService.ListarTodosPorObras(this.idObras, parseInt(this.tipoFiltroGeneral), this.skip, this.take, this.query).subscribe(
      (data: any) => {
        if (data.data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // }
        // this.pagination.total = data.data.length;
        this.totalEmbargo = Number(data.data.totalEmbargo).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});  
        this.totalIrpf = Number(data.data.totalIrpf).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});  
        this.totalTotal = Number(data.data.totalTotal).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});  
        this.totalTotalBruto = Number(data.data.totalTotalBruto).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});  
        this.totalFeriadosFestivos = Number(data.data.totalFeriadosFestivos).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});  
        this.totalTotalSeguridadSocial = Number(data.data.totalTotalSeguridadSocial).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});  

        this.validTotalEmbargo = (data.data.totalEmbargo != null) ? true : false;
        this.validTotalIrpf = (data.data.totalIrpf != null) ? true : false;
        this.validTotalTotal = (data.data.totalTotal != null) ? true : false;
        this.validTotalBruto = (data.data.totalTotalBruto != null) ? true : false;
        this.validTotalFeriadosFestivos = (data.data.totalFeriadosFestivos != null) ? true : false;
        this.validTotalSeguridadSocial = (data.data.totalTotalSeguridadSocial != null) ? true : false;

        console.log('lista:',data);
        var list =  data.data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
          element.totalBruto = (element.totalBruto != null) ? Number(element.totalBruto).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.totalBruto;      
          element.embargo = (element.embargo != null) ? Number(element.embargo).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.embargo;      
          element.irpf = (element.irpf != null) ? Number(element.irpf).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.irpf;      
          element.total = (element.total != null) ? Number(element.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.total;      
          element.totalSeguridadSocial = (element.totalSeguridadSocial != null) ? Number(element.totalSeguridadSocial).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.totalSeguridadSocial;      
        });
        this.listaNominas = list;
        this.listaNominas = [...this.listaNominas];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaNominas;
    this.rowsFiltered = this.listaNominas;
  }


  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
    // this.getAllData();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }
  addModel(modal: any, row: Nominas) {
    if(row !== null){
      this.idNominaListar =  row.id.toString();
      this.size = 'lg';
      this.edicion = true;
      this.titelModal = 'Editar Diferencia de Nómina';
    }else{
      this.idNominaListar =  null;
      this.size = 'lg'; // Necesario para setear el size del modal  
      this.edicion = false;
      this.titelModal = 'Alta de Nómina';  // Necesario para setear el titulo   
    }
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown', size: this.size });
  }
  onCloseModal(e) {
    this.modalRef.close();
    this.getAllData();
  }  
  setEmpresa(e) {
    this.getAllData();
  }  
  setMes(e) {
    this.getAllData();
  }
  setAnno(e) {
    this.getAllData();
  }
  async obtenerBusiness() {
    // this.business = await this.businessService.ListarEmpresasCombo().toPromise();
    this.businessService.ListarEmpresasCombo().subscribe(
      data => {
        this.business = data;
        this.idEmpresa = (data[0]) ? data[0].id : null
        this.getAllData();
      },
      error => {
        this.toastr.error(error);
      }
    );
  }
  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.nominasService.ListarTodosPorObras(this.idObras, parseInt(this.tipoFiltroGeneral), this.skip, this.take, this.query).subscribe(
      (data: any) => {
        if (data.data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalEmbargo = Number(data.data.totalEmbargo).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});  
        this.totalIrpf = Number(data.data.totalIrpf).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});  
        this.totalTotal = Number(data.data.totalTotal).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});  
        this.totalTotalBruto = Number(data.data.totalTotalBruto).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});  
        this.totalFeriadosFestivos = Number(data.data.totalFeriadosFestivos).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});  
        this.totalTotalSeguridadSocial = Number(data.data.totalTotalSeguridadSocial).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});  

        this.validTotalEmbargo = (data.data.totalEmbargo != null) ? true : false;
        this.validTotalIrpf = (data.data.totalIrpf != null) ? true : false;
        this.validTotalTotal = (data.data.totalTotal != null) ? true : false;
        this.validTotalBruto = (data.data.totalTotalBruto != null) ? true : false;
        this.validTotalFeriadosFestivos = (data.data.totalFeriadosFestivos != null) ? true : false;
        this.validTotalSeguridadSocial = (data.data.totalTotalSeguridadSocial != null) ? true : false;
        console.log('lista:',data);
        var list =  data.data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
          element.totalBruto = (element.totalBruto != null) ? Number(element.totalBruto).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.totalBruto;      
          element.embargo = (element.embargo != null) ? Number(element.embargo).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.embargo;      
          element.irpf = (element.irpf != null) ? Number(element.irpf).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.irpf;      
          element.total = (element.total != null) ? Number(element.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.total;      
          element.totalSeguridadSocial = (element.totalSeguridadSocial != null) ? Number(element.totalSeguridadSocial).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.totalSeguridadSocial;      
        });
        this.listaNominas = list;
        this.listaNominas = [...this.listaNominas];

        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }
}
