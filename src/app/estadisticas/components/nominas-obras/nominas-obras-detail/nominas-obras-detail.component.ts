import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Nominas } from '@app/nominas/models/Nominas';
import { Business } from '@app/business/models/Business';
import * as moment from 'moment';
import { NominasService } from '@app/nominas/services/nominas.service';
import { BusinessService } from '@app/business/services/business.service';
import { NominasValidator } from "@app/nominas/validation/nominas.validator";
import * as XLSX from 'xlsx';
import { read } from 'xlsx';

@Component({
  selector: 'app-nominas-obras-detail',
  templateUrl: './nominas-obras-detail.component.html',
  styleUrls: ['./nominas-obras-detail.component.css']
})
export class NominasObrasDetailComponent implements OnInit {

  @Input() idObras: string;
  @Input() idNominaListar: string;
  @Output() closeModal = new EventEmitter<boolean>();
  
  myInputVariable;

  nominas: Nominas = new Nominas();
  business: Business;
  nominasForm: FormGroup;
  submitted: boolean = false;
  edicion: boolean = false;
  titleDocumento: string;
  trabajador: string;
  sizeArchivo: number;
  typeArchivo: string;
  baseArchivo: string | ArrayBuffer;
  uploadFile: boolean = false;
  archivoValid: boolean = false;
  annoActual: string = moment().format('YYYY');
  annoOrigen: number;
  annoActualN: number;
  mesArray: any[] = [];
  annosArray: any[] = [];
  detalles: any[] = [];
  nominas_validation_messages = NominasValidator.NOMINAS_VALIDATION_MESSAGES;
  mes: number;  
  anno: number; 

  
  constructor(
    private toastr: ToastrService, 
    private nominasService: NominasService,
    private businessService: BusinessService,
    private formBuilder: FormBuilder,
  ) {
    this.mesArray = [
      {"texto":"Enero","valor":1},
      {"texto":"Febrero","valor":2},
      {"texto":"Marzo","valor":3},
      {"texto":"Abril","valor":4},
      {"texto":"Mayo","valor":5},
      {"texto":"Junio","valor":6},
      {"texto":"Julio","valor":7},
      {"texto":"Agosto","valor":8},
      {"texto":"Septiembre","valor":9},
      {"texto":"Octubre","valor":10},
      {"texto":"Noviembre","valor":11},
      {"texto":"Diciembre","valor":12},
    ];
    this.annoActualN = Number(this.annoActual); 
    for (let index = 2018; index <= this.annoActualN + 1; index++) {
      this.annosArray.push({"texto":index});
    }

    this.anno = this.annoActualN;
    this.mes = Number(moment().format('M'));
    
  }

  ngOnInit(): void {
    this.titleDocumento = "Subir Documento";

    if (this.idNominaListar != null) {
      this.nominasForm = this.formBuilder.group({
        idEmpresa: [null],
        mes: [null],
        ano: [null],
        archivo: [null],
        dni: [null, [Validators.required]],
        totalBruto: [null, [Validators.required]],
        embargo: [null, [Validators.required]],
        irpf: [null, [Validators.required]],
        total: [null, [Validators.required]],
        totalSeguridadSocial: [null, [Validators.required]],
      });
      this.edicion = true;
      this.obtenerNomina();
      // this.setearConditions();

    } else {
      this.nominasForm = this.formBuilder.group({
        idEmpresa: [null, [Validators.required]],
        mes: [null, [Validators.required]],
        ano: [null, [Validators.required]],
        archivo: [null],
        dni: [null],
        totalBruto: [null],
        embargo: [null],
        irpf: [null],
        total: [null],
        totalSeguridadSocial: [null],
      });
    }

    this.obtenerBusiness();

  }

  get f() { return this.nominasForm.controls; }
  add() {
    this.submitted = true;

    if (this.nominasForm.invalid) {
      return;
    } else {
      if(this.uploadFile) {
        this.submitted = false;
        this.uploadFile = false;
        return;
      }
    }
    
    this.nominas = this.nominasForm.value;

    if(this.edicion){ 
      this.edit(); 
    } else {
      if((this.nominas.archivo == null)){
        this.archivoValid = true;
        return
      } else {
        this.archivoValid = false
      }
      this.create();
    }
  }
  create(){
    let nominasTem = new Nominas();
    nominasTem = this.changeObject(this.nominas);

    var dataNomina = {
      "ano": nominasTem.ano,
      "mes": nominasTem.mes,
      "idEmpresa": nominasTem.idEmpresa,
      "detalles": this.detalles
    }
   
    this.nominasService.Crear(dataNomina).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
        this.closeModal.emit(false);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }
  edit(){
    let nominasTem = new Nominas();
    nominasTem = this.changeObject(this.nominas);

    var updateNomina = {
      "id": this.idNominaListar,
      "trabajador": nominasTem.trabajador,
      "dni": nominasTem.dni,
      "totalBruto": nominasTem.totalBruto,
      "embargo": nominasTem.embargo,
      "irpf": nominasTem.irpf,
      "total": nominasTem.total,
      "totalSeguridadSocial": nominasTem.totalSeguridadSocial
    }
    
    this.nominasService.EditarPorObra(updateNomina).subscribe(
      data => {
        this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.submitted = false;
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

  reiniciarFormulario() {
    this.nominas = new Nominas();
    this.nominasForm.reset();
    this.submitted = false;
  }
  changeObject(nominas: Nominas) {
    let nominasTem: Nominas = new Nominas();
    nominasTem.id = Number(this.idNominaListar);
    nominasTem.idEmpresa = nominas.idEmpresa;
    nominasTem.ano = this.anno = nominas.ano;
    nominasTem.mes = this.mes = nominas.mes;
    nominasTem.dni = nominas.dni;
    nominasTem.totalBruto = nominas.totalBruto;
    nominasTem.embargo = nominas.embargo;
    nominasTem.irpf = nominas.irpf;
    nominasTem.total = nominas.total;
    nominasTem.totalSeguridadSocial = nominas.totalSeguridadSocial;
    nominasTem.trabajador = (nominas.trabajador == undefined) ? this.trabajador : nominas.trabajador;

    if(nominas.archivo != null && nominas.archivo != undefined){
      nominasTem.archivo = {
        "base64": this.baseArchivo,
        "nombre": this.titleDocumento,
        "mimeType": this.typeArchivo,
        "tamano": this.sizeArchivo  
      };
    } else {
      nominasTem.archivo = null;
      // albaranesTem.archivo = {
      //   "base64": "",
      //   "nombre": "",
      //   "mimeType": "",
      //   "tamano": 0  
      // };
    }

    return nominasTem;
  }
  onFileChange(files: FileList) {
    var reader = new FileReader();
    let workbookkk;
    let XL_row_object;
    let json_object;
    reader.readAsBinaryString(files[0]);

    reader.onloadend = (e: Event) => {

      // console.log(reader.result);   
      
      this.archivoValid = false
      this.uploadFile = false;   
      this.titleDocumento = "Subir Documento";
      
      // XLSX.utils.sheet_to_json

      workbookkk=read( reader.result,{type: 'binary'});
      // console.log(workbookkk);
      var first_sheet_name = workbookkk.SheetNames[0];
      var ws = workbookkk.Sheets[first_sheet_name];
      
      // console.log(XLSX.utils.sheet_to_json(ws,{header:1}));
      XL_row_object = XLSX.utils.sheet_to_json(ws,{header:1});
      XL_row_object.splice(0, 8) // remove addtional lines of header
      // console.log(XL_row_object);
      var validTrabajador = false;
      var validDNI = false;
      var validTotalBruto = false;
      var validEmbargo = false;
      var validIRPF = false;
      var validTotal = false;
      var validTotalSS = false;
      for (let index = 0; index < XL_row_object.length; index++) {
        // trabajador = 2
        // dni = 3
        // totalBruto = 24
        // embargo = 26
        // irpf = 32
        // total = 34
        // totalSeguridadSocial = 38
        const element = XL_row_object[index];
        if(element.length > 0) {
          this.detalles.push({
            "trabajador": (element[2] != undefined) ? element[2] : null,
            "dni": (element[3] != undefined) ? element[3] : null,
            "totalBruto": (element[24] != undefined) ? element[24] : 0,
            "embargo": (element[26] != undefined) ? element[26] : 0,
            "irpf": (element[32] != undefined) ? element[32] : 0,
            "total": (element[34] != undefined) ? element[34] : 0,
            "totalSeguridadSocial": (element[38] != undefined) ? element[38] : 0,
          })
        }

        
        if(element[2] != undefined) {
          validTrabajador = true;
        }

        if(element[3] != undefined) {
          validDNI = true;
        }

        if(element[24] != undefined) {
          validTotalBruto = true;
        }

        if(element[26] != undefined) {
          validEmbargo = true;
        }

        if(element[32] != undefined) {
          validIRPF = true;
        }

        if(element[34] != undefined) {
          validTotal = true;
        }

        if(element[38] != undefined) {
          validTotalSS = true;
        }
                
      }

      if(!validTrabajador) {
        this.toastr.error(`La columna trabajador debe tener algún dato`, 'Error', { timeOut: 2500, closeButton: true });
        this.myInputVariable = "";
        this.nominasForm.patchValue({
          archivo: null
        });
      } else if (!validDNI) {
        this.toastr.error(`La columna DNI debe tener algún dato`, 'Error', { timeOut: 2500, closeButton: true });
        this.myInputVariable = "";
        this.nominasForm.patchValue({
          archivo: null
        });
      } else if(!validTotalBruto) {
        this.toastr.error(`La columna Total Bruto debe tener algún dato`, 'Error', { timeOut: 2500, closeButton: true });
        this.myInputVariable = "";
        this.nominasForm.patchValue({
          archivo: null
        });
      } else if(!validEmbargo) {
        this.toastr.error(`La columna embargo debe tener algún dato`, 'Error', { timeOut: 2500, closeButton: true });
        this.myInputVariable = "";
        this.nominasForm.patchValue({
          archivo: null
        });
      } else if(!validIRPF) {
        this.toastr.error(`La columna IRPF debe tener algún dato`, 'Error', { timeOut: 2500, closeButton: true });
        this.myInputVariable = "";
        this.nominasForm.patchValue({
          archivo: null
        });
      } else if(!validTotal) {
        this.toastr.error(`La columna Total LIQ debe tener algún dato`, 'Error', { timeOut: 2500, closeButton: true });
        this.myInputVariable = "";
        this.nominasForm.patchValue({
          archivo: null
        });
      } else if(!validTotalSS) {
        this.toastr.error(`La columna SS Total debe tener algún dato`, 'Error', { timeOut: 2500, closeButton: true });
        this.myInputVariable = "";
        this.nominasForm.patchValue({
          archivo: null
        });
      } else {
        this.toastr.success(`Archivo Correcto`, 'Success', { timeOut: 2500, closeButton: true });
        this.titleDocumento = files[0].name;
        this.sizeArchivo = files[0].size;
        this.typeArchivo = files[0].type;
        this.baseArchivo = reader.result;
      }
    }
  }

  document() {
    this.uploadFile = true;
    let element: HTMLElement = document.getElementById('archivo') as HTMLElement;
    element.click();
  }
  
  async obtenerBusiness() {
    // this.business = await this.businessService.ListarEmpresasCombo().toPromise();
    this.businessService.ListarEmpresasCombo().subscribe(
      data => {
        this.business = data;
      },
      error => {
        this.toastr.error(error);
      }
    );
  }

  obtenerNomina() {
    this.nominasService.ObtenerPorObraId(this.idNominaListar).subscribe(
      data => {
        this.obtenerPatchNomina(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  obtenerPatchNomina(data){
    this.nominas = this.changeObject(data);
    this.trabajador = data.trabajador;
    this.nominasForm.patchValue({
      dni: this.nominas.dni,
      totalBruto: this.nominas.totalBruto,
      embargo: this.nominas.embargo,
      irpf: this.nominas.irpf,
      total: this.nominas.total,
      totalSeguridadSocial: this.nominas.totalSeguridadSocial,
    });

  }
}
