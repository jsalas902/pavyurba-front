import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { GastosGeneralesService } from '@app/gastos-generales/services/gastos-generales.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { GastosGenerales } from '@app/gastos-generales/models/GastosGenerales';
import * as moment from 'moment';

import { Router } from '@angular/router';
import { Pagination } from '@app/_models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-gastos-generales-obras',
  templateUrl: './gastos-generales-obras.component.html',
  styleUrls: ['./gastos-generales-obras.component.css']
})
export class GastosGeneralesObrasComponent implements OnInit {

  @BlockUI('componentsGastosGenerales') blockUIList: NgBlockUI;

  @Input() idObras: string;  


  
  rows: GastosGenerales[];
  rowsFiltered: GastosGenerales[];
  
  pagination: Pagination;
  listaGastosGenerales: GastosGenerales[];
  cargaCompleta = false;
  titelModal: string;
  dato: number;
  type: string;
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  idGastoGeneral: string;
  totalGastosGenerales: string;
  validTotalGastosGenerales: boolean = false;
  private modalRef;

  constructor(private toastr: ToastrService,
              private gastosGeneralesService: GastosGeneralesService,
              private confirmDialogService: ConfirmDialogService,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private modalService: NgbModal,
              ) {
}


  ngOnInit(): void {

    this.titelModal = 'Crear Huchas';

    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = '';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;

    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    this.getAllData();
  }

  ngAfterViewInit(): void {
    // this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();
    this.gastosGeneralesService.ListarGastosGeneralesPorObras(this.idObras, this.skip, this.take, this.query).subscribe(
      (data: any) => {
        if (data.data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalGastosGenerales = Number(data.data.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});          
        this.validTotalGastosGenerales = (data.data.total != null) ? true : false;
        console.log('lista:',data);
        var list =  data.data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
          element.fechaDesde = moment(element.fechaDesde).format('DD-MM-YYYY');
          element.fechaHasta = moment(element.fechaHasta).format('DD-MM-YYYY');
          element.total = (element.total != null) ? Number(element.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.total;      
        });
        this.listaGastosGenerales = list;
        this.listaGastosGenerales = [...this.listaGastosGenerales];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaGastosGenerales;
    this.rowsFiltered = this.listaGastosGenerales;
  }

  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, row: any) {
    this.idObras = this.idObras;
    this.titelModal = row !== null ? `Editar Gasto General` : `Crear Gasto General`;
    this.idGastoGeneral = row !== null ? row.idgastogeneral.toString() : null;
    this.cargaCompleta = true;
    // this.gastosGeneralesService.addModelGastosGenerales(modal);
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown' });
  }
  onSetModal(e) {
    this.getAllData();
  }
  onCloseModal(e) {
    this.modalRef.close();
    this.getAllData();
  } 

  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.gastosGeneralesService.ListarGastosGeneralesPorObras(this.idObras, this.skip, this.take, this.query).subscribe(
      (data: any) => {
        if (data.data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalGastosGenerales = Number(data.data.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});          
        this.validTotalGastosGenerales = (data.data.total != null) ? true : false;
        console.log('lista:',data);
        var list = data.data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
          element.fechaDesde = moment(element.fechaDesde).format('DD-MM-YYYY');
          element.fechaHasta = moment(element.fechaHasta).format('DD-MM-YYYY');
          element.total = (element.total != null) ? Number(element.total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}) : element.total;      
        });
        this.listaGastosGenerales = list;
        this.listaGastosGenerales = [...this.listaGastosGenerales];

        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }
}
