import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgbDateParserFormatter, NgbDatepickerI18n, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { Obra } from '@app/obras/models/Obra'
import { ProvidersService } from '@app/providers/services/providers.service';
import { MaterialsService } from '@app/materials/services/materials.service';
import { ObrasService } from '@app/obras/services/obras.service';
import { EmployeesService } from '@app/employees/services/employees.service';
import { CalendariosValidator } from "@app/calendarios/validation/calendarios.validator";
import { CalendariosTypes } from "@app/calendarios/models/constantes/CalendariosTypes";
import { IncidenciasTypes } from "@app/calendarios/models/constantes/IncidenciasTypes";

import { NgbDateCustomParserFormatter} from '@app/_helpers/dateformat';
import { Employee } from '@app/employees/models/Employee';
import { CalendariosIncidencias } from '@app/calendarios/models/CalendariosIncidencias';
import { CalendariosService } from '@app/calendarios/services/calendarios.service';
import { CustomDatepickerI18nService, I18n } from '@app/calendarios/services/custom-datepicker-i18n.service';

@Component({
  selector: 'app-calendarios-obras-detail',
  templateUrl: './calendarios-obras-detail.component.html',
  styleUrls: ['./calendarios-obras-detail.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter},
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18nService }
  ]
})
export class CalendariosObrasDetailComponent implements OnInit {

  @Input() idCalendarios: string;
  @Input() tipoEntrada: string;
  @Output() closeModal = new EventEmitter<boolean>();
  @ViewChild('labelImport', { static: true })
  labelImport: ElementRef;
  fileToUpload: File = null;

  calendariosIncidencias: CalendariosIncidencias = new CalendariosIncidencias();
  obras: Obra;
  employeesCombo: Employee;
  calendariosForm: FormGroup;
  titleDocumento: string;
  sizeArchivo: number;
  typeArchivo: string;
  baseArchivo: string | ArrayBuffer;
  submitted: boolean = false;
  uploadFile: boolean = false;
  edicion: boolean = false;
  archivoValid: boolean = false;
  fechaDesde;
  fechaHasta;
  porRango: boolean = true;
  asistenciaIncidenciaNg: string = null;
  mensaje: string = '';
  maxLength: number = 500;
  // fechaFacturacion;
  d: any;
  d1: any;
  calendariosTypes: any;
  incidenciasTypes: any;
  readonly DELIMITER = '-';
  calendarios_validation_messages = CalendariosValidator.CALENDARIOS_VALIDATION_MESSAGES;

  
  constructor(
    private toastr: ToastrService,      
    private providersService: ProvidersService,
    private materialsService: MaterialsService,
    private obrasService: ObrasService,
    private employeesService: EmployeesService,
    private calendariosService: CalendariosService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.calendariosTypes = [{"texto":CalendariosTypes.Asistencia}, {"texto":CalendariosTypes.Incidencia}];
    this.incidenciasTypes = [{"texto":IncidenciasTypes.Vacaciones}, {"texto":IncidenciasTypes.Falta}, {"texto":IncidenciasTypes.Enfermedad}];
    this.titleDocumento = "Subir Documento";
    this.calendariosForm = this.formBuilder.group({
      id: [null],
      porRango: [true],
      tipo: [null],
      observaciones: [null],
      incidencia: [null],
      asistenciaIncidencia: [null],
      idObra: [null],
      idEmpleado: [null, [Validators.required]],
      fechaDesde: [null, [Validators.required]],
      fechaHasta: [null],
      documento: [null],
    });

    if (this.idCalendarios != null) {
      this.edicion = true;
      this.obtenerCalendario();
    }
    this.obtenerObras();
    this.obtenerEmployees();
  
  }

  obtenerCalendario() {
    this.calendariosService.ObtenerPorIdCalendar(this.idCalendarios).subscribe(
      data => {
        this.obtenerPatchAlbaranCalendar(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  obtenerObras() {
    this.obrasService.ListarObrasCombo().subscribe(
      data => {
        this.obras = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  obtenerEmployees() {
    this.employeesService.ListarEmployeesCombo().subscribe(
      data => {
        this.employeesCombo = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  get f() { return this.calendariosForm.controls; }
  add() {
    this.submitted = true;

    if (this.calendariosForm.invalid) {
      return;
    } else {
      if(this.uploadFile) {
        this.submitted = false;
        this.uploadFile = false;
        return;
      }
    }
    
    this.calendariosIncidencias = this.calendariosForm.value;
    
    // console.log(this.calendariosForm.value instanceof Albaran);

    if((this.calendariosIncidencias.documento == null) && (this.asistenciaIncidenciaNg == "INCIDENCIA") && !this.edicion){
      this.archivoValid = true;
      this.calendariosIncidencias.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModelFix(this.fechaDesde);
      this.calendariosIncidencias.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModelFix(this.fechaHasta);
      // var fechaNew = this.calendariosIncidencias.fechaRecepcion;
      // this.calendariosIncidencias.fechaRecepcion = "";
      // this.calendariosIncidencias.fechaRecepcion = this.toModelFix(this.fechaRecepcion);
      return
    } else {
      this.archivoValid = false
    }
    
    if(this.edicion) {
      this.edit();
    }else {
      this.create();     
    }

  }
  create(){
    let calendariosIncidenciasTem = new CalendariosIncidencias();

    this.calendariosIncidencias.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    this.calendariosIncidencias.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);
    calendariosIncidenciasTem = this.changeObject(this.calendariosIncidencias);
    calendariosIncidenciasTem.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    calendariosIncidenciasTem.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);

    calendariosIncidenciasTem.fechaHasta = calendariosIncidenciasTem.fechaHasta == null ? calendariosIncidenciasTem.fechaDesde : calendariosIncidenciasTem.fechaHasta

    if(this.asistenciaIncidenciaNg == "INCIDENCIA"){ 
      var dataIncidencia = {
        id: calendariosIncidenciasTem.id,
        tipo: calendariosIncidenciasTem.tipo,
        incidencia: calendariosIncidenciasTem.incidencia,
        fechaDesde: calendariosIncidenciasTem.fechaDesde,
        fechaHasta: calendariosIncidenciasTem.fechaHasta,
        observaciones: calendariosIncidenciasTem.observaciones,
        documento: calendariosIncidenciasTem.documento, 
        idEmpleado: calendariosIncidenciasTem.idEmpleado
      }

      this.calendariosService.CrearIncidencia(dataIncidencia).subscribe(
        data => {
          this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
          this.reiniciarFormulario();
          // this.getAllData();
          this.closeModal.emit(true);
        },
        error => {
          // this.reloadStop();
          // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
          this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
      });
    } else {
      var dataCalendario = {
        id: calendariosIncidenciasTem.id,
        fechaDesde: calendariosIncidenciasTem.fechaDesde,
        fechaHasta: calendariosIncidenciasTem.fechaHasta,
        observaciones: calendariosIncidenciasTem.observaciones,
        idEmpleado: calendariosIncidenciasTem.idEmpleado,
        idObra: calendariosIncidenciasTem.idObra
      }

      this.calendariosService.CrearCalendario(dataCalendario).subscribe(
        data => {
          this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
          this.reiniciarFormulario();
          // this.getAllData();
          this.closeModal.emit(true);
        },
        error => {
          // this.reloadStop();
          // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
          this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
      });
    }
  }

  edit(){
    let calendariosIncidenciasTem = new CalendariosIncidencias();

    this.calendariosIncidencias.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    this.calendariosIncidencias.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);
    calendariosIncidenciasTem = this.changeObject(this.calendariosIncidencias);
    calendariosIncidenciasTem.fechaDesde = (this.fechaDesde == undefined || this.fechaDesde == "" || this.fechaDesde == null) ? null : this.toModel(this.fechaDesde);
    calendariosIncidenciasTem.fechaHasta = (this.fechaHasta == undefined || this.fechaHasta == "" || this.fechaHasta == null) ? null : this.toModel(this.fechaHasta);

    calendariosIncidenciasTem.fechaHasta = calendariosIncidenciasTem.fechaHasta == null ? calendariosIncidenciasTem.fechaDesde : calendariosIncidenciasTem.fechaHasta

    var dataCalendario = {
      id: calendariosIncidenciasTem.id,
      fechaDesde: calendariosIncidenciasTem.fechaDesde,
      fechaHasta: calendariosIncidenciasTem.fechaHasta,
      observaciones: calendariosIncidenciasTem.observaciones,
      idEmpleado: calendariosIncidenciasTem.idEmpleado,
      idObra: calendariosIncidenciasTem.idObra
    }

    this.calendariosService.EditarCalendarioPorObra(dataCalendario).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        this.reiniciarFormulario();
        // this.getAllData();
        this.closeModal.emit(true);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
    
  }

  reiniciarFormulario() {
    this.calendariosIncidencias = new CalendariosIncidencias();
    this.calendariosForm.reset();
    this.submitted = false;
  }

  obtenerPatchAlbaranCalendar(data){
    this.calendariosIncidencias = this.changeObject(data);

    this.asistenciaIncidenciaNg = this.tipoEntrada;

    this.calendariosForm.patchValue({
      id: this.calendariosIncidencias.id,
      // fechaDesde: this.calendariosIncidencias.fechaDesde,
      // fechaHasta: this.calendariosIncidencias.fechaHasta,
      tipo: this.calendariosIncidencias.tipo,
      idEmpleado: this.calendariosIncidencias.idEmpleado,
      idObra: this.calendariosIncidencias.idObra,
      observaciones: this.calendariosIncidencias.observaciones,
      asistenciaIncidencia: this.tipoEntrada,
      incidencia: this.calendariosIncidencias.incidencia,
      documento: this.calendariosIncidencias.documento,
    });
  }
  changeObject(calendariosIncidencias: CalendariosIncidencias) {
    let calendariosIncidenciasTem: CalendariosIncidencias = new CalendariosIncidencias();
    calendariosIncidenciasTem.id = (calendariosIncidencias.id != null) ? calendariosIncidencias.id : 0;
    calendariosIncidenciasTem.tipo = calendariosIncidencias.tipo;
    calendariosIncidenciasTem.incidencia = calendariosIncidencias.incidencia;
    calendariosIncidenciasTem.idEmpleado = calendariosIncidencias.idEmpleado;
    calendariosIncidenciasTem.fechaDesde = this.fechaDesde = (calendariosIncidencias.fechaDesde !== null) ? this.fromModel(calendariosIncidencias.fechaDesde) : null;
    calendariosIncidenciasTem.fechaHasta = this.fechaHasta = (calendariosIncidencias.fechaHasta !== null) ? this.fromModel(calendariosIncidencias.fechaHasta) : null;
    calendariosIncidenciasTem.idObra = calendariosIncidencias.idObra;
    calendariosIncidenciasTem.observaciones = calendariosIncidencias.observaciones;
    if(calendariosIncidencias.documento != null && calendariosIncidencias.documento != undefined){
      calendariosIncidenciasTem.documento = {
        "base64": this.baseArchivo,
        "nombre": this.titleDocumento,
        "mimeType": this.typeArchivo,
        "tamano": this.sizeArchivo  
      };
    } else {
      calendariosIncidenciasTem.documento = null;
    }

    return calendariosIncidenciasTem;
  }
  toModel(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format();
  }
  toModelFix(date: NgbDateStruct | null): string | null {
    // let fecha = date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null
    // return new Date(date.year,date.month,date.day);
    return moment(date ? date.month + this.DELIMITER + date.day + this.DELIMITER + date.year : null, 'MM/DD/YYYY').format('DD-MM-YYYY');
  }
  onFileChange(files: FileList) {
    // this.labelImport.nativeElement.innerText = Array.from(files)
    //   .map(f => f.name)
    //   .join(', ');
    this.fileToUpload = files.item(0);

    var reader = new FileReader();

    reader.onloadend = (e: Event) => {
      this.titleDocumento = files[0].name;
      this.sizeArchivo = files[0].size;
      this.typeArchivo = files[0].type;
      this.baseArchivo = reader.result;
      this.archivoValid = false
      this.uploadFile = false;
    }

    reader.readAsDataURL(files[0]);
    // var base = this.readBase64(files.item(0));
  }

  document() {
    this.uploadFile = true;
    let element: HTMLElement = document.getElementById('documento') as HTMLElement;
    element.click();
  }
  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        day : parseInt(date[2], 10),
        month : parseInt(date[1], 10),
        year : parseInt(date[0], 10)
      };
    }
    return null;
  }

  changeRango(){
    this.porRango = !this.porRango;
  }

  changeAsistenciaIncidencia(){
    console.log(this.asistenciaIncidenciaNg)
  }
}
