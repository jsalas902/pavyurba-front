import { Component, OnInit, ChangeDetectorRef, Input, ViewChild, TemplateRef } from '@angular/core';
import { ConfirmDialogService } from '@app/_services';
import { BusinessService } from '@app/business/services/business.service';
import { ToastrService } from 'ngx-toastr';
import { Business } from '@app/business/models/Business';
import { Router } from '@angular/router';
import { Pagination } from '@app/_models';
import * as moment from 'moment';
import 'moment/locale/es';

import { CalendariosService } from '@app/calendarios/services/calendarios.service';
import { CalendariosIncidencias } from '@app/calendarios/models/CalendariosIncidencias';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-calendarios-obras',
  templateUrl: './calendarios-obras.component.html',
  styleUrls: ['./calendarios-obras.component.css']
})
export class CalendariosObrasComponent implements OnInit {

  @BlockUI('componentsCalendario') blockUIList: NgBlockUI;

  @Input() idObras: string;
  @Input() idCalendariosListar: string;
  titelModal: string;
  tipoEntrada: string;

  @BlockUI('events') blockUIEvents: NgBlockUI;

  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;

  locale: string = 'es';
  viewDate: Date = new Date();
  activeDayIsOpen = false;
  diaSelect;
  fechaSelect;
  mesSelect;
  mes;
  anno;
  dia;

  rows: CalendariosIncidencias[];
  rowsFiltered: CalendariosIncidencias[]; 
  pagination: Pagination;
  listaCalendarios: CalendariosIncidencias[];
  cargaCompleta = false;
  
  query: string;
  skip: string;
  take: string;
  skipAux;
  takeAux;
  totalCalendario: number;
  validTotalCalendario: boolean = false;
  private modalRef;

  ngOnInit() {
    this.pagination = new Pagination();
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.query = '';
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
    this.skipAux = this.skip;
    this.takeAux = this.take;

    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };

    this.getAllData();
  }
  constructor(private modal: NgbModal,
    private toastr: ToastrService,
    private confirmDialogService: ConfirmDialogService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private modalService: NgbModal,
    private calendariosService: CalendariosService,
    private businessService: BusinessService) {
      moment.locale('es');
      this.diaSelect = moment(this.viewDate).format('dddd');
      this.fechaSelect = moment(this.viewDate).format('DD [de ]');
      this.mesSelect = moment(this.viewDate).format('MMMM');

      this.dia = moment(this.viewDate).format('DD');
      this.mes = moment(this.viewDate).format('MM');
      this.anno = moment(this.viewDate).format('YYYY');
    }

  ngAfterViewInit(): void {
    this.cargaCompleta = true;
    this.cdRef.detectChanges();
  }

  getAllData() {
    this.reloadStart();
    this.calendariosService.ListarPorObra(this.idObras, this.skip, this.take, this.query).subscribe(
      (data: any) => {
        if (data.data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
            } 
          } 
        }  
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalCalendario = data.data.total;        
        this.validTotalCalendario = (data.data.total != null) ? true : false;
        console.log('lista:',data);
        var list =  data.data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaDesde = moment(element.fechaDesde).format('DD-MM-YYYY');
          element.fechaHasta = moment(element.fechaHasta).format('DD-MM-YYYY');
          element.fechaHoraLog = moment(element.fechaHoraLog).format('DD-MM-YYYY');
        });
        this.listaCalendarios = list;
        this.listaCalendarios = [...this.listaCalendarios];

        this.getTabledata();
        this.reloadStop();
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  getTabledata() {
    this.rows = this.listaCalendarios;
    this.rowsFiltered = this.listaCalendarios;
  }


  reloadStart() {
    this.blockUIList.start('Loading..');
  }

  reloadStop() {
    this.blockUIList.stop();
    this.blockUIList.isActive = false;
    this.blockUIList.reset();
  }

  pageChanged(page: number): void {
    this.pagination.current_page_aux = this.pagination.current_page;
    this.pagination.current_page = page;
    this.refreshCountries();
  }
  refreshCountries() {
    if(this.takeAux != this.pagination.per_page) {
      this.pagination.current_page = 1;
      this.takeAux = this.pagination.per_page;
    } else {
      this.takeAux = this.pagination.per_page;
    }
    this.skipAux = this.skip;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
      // this.take = ((this.pagination.current_page - 1) * this.pagination.per_page + this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString();
      this.getAllData();
  }

  addModel(modal: any, row: any) {
    if(row !== null){
      this.idCalendariosListar =  row.id.toString();
      this.titelModal = 'Editar Calendario';
      this.tipoEntrada = row.tipoEntrada;
    }else{
      this.idCalendariosListar =  null;
      this.tipoEntrada = null;
      this.titelModal = 'Nueva Entrada en Calendario';  // Necesario para setear el titulo   
    }
    this.modalRef = this.modalService.open(modal, { windowClass: 'animated fadeInDown' });
  }
  onCloseModal(e) {
    this.modalRef.close();
    this.getAllData();
  }

  searchFilter(e){
    this.reloadStart();
    this.query = e.target.value;
    this.pagination.current_page = 1;
    this.pagination.per_page = 10;
    this.skip = ((this.pagination.current_page - 1) * this.pagination.per_page).toString();
    this.take = (this.pagination.per_page).toString(); 
    this.calendariosService.ListarPorObra(this.idObras, this.skip, this.take, this.query).subscribe(
      (data: any) => {
        if (data.data.data.length === 10) {
          if (this.pagination.current_page === 1) {
            this.pagination.total = data.data.data.length + 10;
          } else {
            this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
          }
        } else {
          if (this.pagination.per_page === data.data.data.length) {
            if (this.pagination.current_page === 1) {
              this.pagination.total =  data.data.data.length + 10;
            } else {
              this.pagination.total = (this.pagination.current_page*data.data.data.length) + 10;
            } 
          } 
        }
        // else {
        //   if (data.data.length < 10) {
        //     this.pagination.total = data.data.length;
        //   }
        // } 
        // this.pagination.total = data.data.length;
        this.totalCalendario = data.data.total;        
        this.validTotalCalendario = (data.data.total != null) ? true : false;
        console.log('lista:',data);
        var list =  data.data.data;
        list.forEach(element => {
          // moment.locale('es');
          element.fechaDesde = moment(element.fechaDesde).format('DD/MM');
          element.fechaHasta = moment(element.fechaHasta).format('DD/MM');
        });
        this.listaCalendarios = list;
        this.listaCalendarios = [...this.listaCalendarios];
        this.getTabledata();
        this.reloadStop();
        // this.query = '';
      },
      error => {
        this.toastr.error(error);
        this.reloadStop();
      }
    );
  }

  removeRegistro(row: any) {
    this.confirmDialogService.confirmThis(`Está seguro desea eliminar el calendario seleccionado?`, () =>  {
      this.delete(row);      
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario`);
    });
  }

  delete(row: any) {
    this.reloadStart();
    this.calendariosService.EliminarCalendario(row.id.toString()).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Eliminado' , { timeOut: 2500, closeButton: true });
        this.getAllData();        
      },
      error => {
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        this.reloadStop();
    });
    
  }
}
