import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EstadisticasService } from '@app/estadisticas/services/estadisticas.service';
import { Obra } from '@app/obras/models/Obra';
import { ObrasService } from '@app/obras/services/obras.service';
import { ConfirmDialogService } from '@app/_services';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-vincular-obras',
  templateUrl: './vincular-obras.component.html',
  styleUrls: ['./vincular-obras.component.css']
})
export class VincularObrasComponent implements OnInit {

  @Input() idObras: string;
  @Input() idVinculacion: string;
  @Output() closeModal = new EventEmitter<boolean>();

  vincularForm: FormGroup;
  obras: Obra;
  vincularObras: any;

  constructor(
    private toastr: ToastrService,
              private estadisticasService: EstadisticasService,
              private confirmDialogService: ConfirmDialogService,
              private obrasService: ObrasService,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private modalService: NgbModal,
              private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.obtenerObras();
    this.vincularForm = this.formBuilder.group({
      id: [this.idObras],
      idObraVincular: [this.idVinculacion, [Validators.required]]
    });
  }

  obtenerObras() {
    this.obrasService.ListarObrasCombo().subscribe(
      data => {
        this.obras = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  add() {

    if (this.vincularForm.invalid) {
      return;
    } 
    
    this.vincularObras = this.vincularForm.value;

    this.estadisticasService.vincularObra(this.vincularObras).subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        this.closeModal.emit(true);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }

}
