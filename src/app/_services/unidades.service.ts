import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { GenericService, AuthenticationService } from '../_services';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UnidadesService extends GenericService {

  
  constructor(private http: HttpClient,
    public authService: AuthenticationService,
    private injector: Injector) {
  super();
  }

  ListarUnidadesMedidasCombo(): Observable<any> {
    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/maestros/listarUnidadesMedidasCombo', { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta.data;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }
}
