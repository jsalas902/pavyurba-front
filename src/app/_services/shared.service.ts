import { Injectable, Injector } from '@angular/core';
import { Subject, Observable, of } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDeleteComponent } from '../shared/components/confirm-delete/confirm-delete.component';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  [x: string]: any;

  isLoading = new Subject<boolean>();

  constructor(private modalService: NgbModal,
              private http: HttpClient,
              public authService: AuthService,
              private injector: Injector) { }

  show() {
      this.isLoading.next(true);
  }

  hide() {
      this.isLoading.next(false);
  }

  confirmDelete(
    title: string = 'Eliminar',
    message: string = 'Esta seguro de eliminar este registro?',
    btnOkText: string = 'OK',
    btnCancelText: string = 'Cancel',
    dialogSize: 'sm'|'lg' = 'sm'): Promise<boolean> {
    const modalRef = this.modalService.open(ConfirmDeleteComponent,
      { ariaLabelledBy: 'modal-basic-title',
      centered: true,
      size: dialogSize
    });
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
    modalRef.componentInstance.btnOkText = btnOkText;
    modalRef.componentInstance.btnCancelText = btnCancelText;

    return modalRef.result;
  }

  confirmDefault(
    title: string = 'Confirmar',
    message: string = 'Esta seguro de alterar este registro?',
    btnOkText: string = 'OK',
    btnCancelText: string = 'Cancel',
    dialogSize: 'sm'|'lg' = 'sm'): Promise<boolean> {
    const modalRef = this.modalService.open(ConfirmDeleteComponent,
      { ariaLabelledBy: 'modal-basic-title',
      centered: true,
      size: dialogSize
    });
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
    modalRef.componentInstance.btnOkText = btnOkText;
    modalRef.componentInstance.btnCancelText = btnCancelText;

    return modalRef.result;
  }

  TransformToDefaultImage(url: string): Observable<any> {
    return this.http
      .get(url, {responseType: 'blob'})
      .pipe(
        map(
          (respuesta: any) => {
            return { url };
          }
        ),
        catchError((error: any) => {
          this.authService = this.injector.get(AuthService);
          return this.HandleError(error);
        })
      );
  }
}
