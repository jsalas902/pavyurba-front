import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError, from } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';

import { environment } from '@environments/environment'
import { User} from '@app/_models/User'; 
import { UserLogin } from '@app/_models/login/UserLogin';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

    headers: HttpHeaders;
    constructor(private http: HttpClient, private router: Router) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('ACCESS_USER_LL_CMS')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(userLogin: UserLogin ): Observable<any> {
      const headers = new HttpHeaders()
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set( 'No-Auth', 'True');
      
      const body = new HttpParams()
      .set('email', userLogin.email)
      .set('clave', userLogin.clave);

      return this.http.post(environment.apiUrl + '/auth/login', body.toString(), { headers: headers })
      .pipe(
        map(
          (respuesta: any) => {
            this.currentUserSubject.next(respuesta.data);
            return respuesta;
          }
        ),
        catchError((error: any) => {
          return this.HandleError(error);
        })
      );
    }

    logout() {
        // remove user from local storage to log user out       
        localStorage.removeItem('ACCESS_TOKEN_LL_CMS');
        localStorage.removeItem('ACCESS_USER_LL_CMS');
        localStorage.removeItem('remember');
        this.currentUserSubject.next(null);
        this.router.navigate(['/login']);
    }

    public loginUser(user: User, token: string) {
      localStorage.setItem('ACCESS_TOKEN_LL_CMS', token);
      localStorage.setItem('ACCESS_USER_LL_CMS', JSON.stringify(user));
    }

    showUser(token: string): Observable<User> {
      // this.headers = new HttpHeaders ({
      //   Authorization: 'Bearer ' + token
      // });
      let headers = new HttpHeaders()
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer ' + token);
      
      const url = environment.apiUrl + '/auth/perfil';
      return this.http.get(url, { headers: headers })
      .pipe(
        map(
          (respuesta: any) => {
            const userRegister = new User(respuesta.data);
            return userRegister;
          }
        ),
        catchError((error: any) => {
          return this.HandleError(error);
        })
      );
    }

    HandleError(error: any) {
      let errorMessage = '';
      if(error.status === 400) {
        errorMessage = 'Error: ' + error.statusText + ', ' + error.error.mensaje;
      } else if (error.status === 401) {
        errorMessage = 'Error: ' + error.statusText + ', ' + error.error.mensaje;
        localStorage.removeItem('ACCESS_TOKEN_LL_CMS');
        localStorage.removeItem('ACCESS_USER_LL_CMS');
        localStorage.removeItem('remember');
        this.ReturnToLogin();
      } else if (error.status === 403) {
        errorMessage = 'Error: ' + error.statusText + ', ' + error.error.mensaje;
      } else if (error.status === 404) {
        errorMessage = 'Error: ' + error.statusText + ', ' + error.error.mensaje;
      } else if (error.status === 500) {
        errorMessage = 'Error: ' + error.statusText + ', ' + error.error.mensaje;
      } else {
        errorMessage = 'Error: ' + (error.error === undefined || error.error === null ? error.error.mensaje : error.message);
      }

      return throwError(errorMessage);
    }

    public ReturnToLogin() {
      this.router.navigateByUrl('/login');
    }
}
