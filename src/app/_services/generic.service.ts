import { AuthenticationService } from './authentication.service';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';

export class GenericService {

  paguinateParams: HttpParams;
  headers: HttpHeaders;
  authService: AuthenticationService;

  HandleError(error: any) {
    let errorMessage = '';
    if(error.status === 400) {
      errorMessage = 'Error: ' + error.statusText + ', ' + error.error.mensaje;
    } else if (error.status === 401) {
      errorMessage = 'Error: ' + error.statusText + ', ' + error.error.mensaje;
      localStorage.removeItem('ACCESS_TOKEN_LL_CMS');
      localStorage.removeItem('ACCESS_USER_LL_CMS');
      localStorage.removeItem('remember');
      this.authService.ReturnToLogin();

    } else if (error.status === 403) {
      errorMessage = 'Error: ' + error.statusText + ', ' + error.error.mensaje;
    } else if (error.status === 404) {
      errorMessage = 'Error: ' + error.statusText + ', ' + error.error.mensaje;
    } else if (error.status === 500) {
      errorMessage = 'Error: ' + error.statusText + ', ' + error.error.mensaje;
    } else {
      errorMessage = 'Error: ' + (error.error === undefined || error.error === null ? error.error.mensaje : error.message);
    }

    return throwError(errorMessage);
  }
}
