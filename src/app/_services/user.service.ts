import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private token = localStorage.getItem('ACCESS_TOKEN_LL_CMS');
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.token })
  };

  // httpOptions = {
  //   // headers: new HttpHeaders({
  //   headers: {
  //     'Access-Control-Allow-Origin': '*',
  //     'Access-Control-Allow-Headers': 'X-Requested-With',
  //     'Authorization': 'Bearer ' + this.token,
  //   }
  // };
  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // Error
      console.error('error:', error.error.message);
    } else {
      // Error
      console.error(
        `Api server returned ${error.status}, ` +
        `error body: ${error.error}`);
    }
    // throwError is observable
    return throwError('Error has happened');
  }

  private extractData(res: Response) {
    const body = res;
    return body || {};
  }

  getAllData(params): Observable<any> {
    const url = environment.apiUrl + `/api/v1/admin/user/index?${params}`;
    return this.http.get(url, this.httpOptions);
  }
  getAllProfile(page): Observable<any> {
    const url = environment.apiUrl + `/api/v1/admin/profiles/index?page=${page}`;
    return this.http.get(url, this.httpOptions);
  }
  create(data): Observable<any> {
    const url = environment.apiUrl + `/api/v1/admin/user/create`;
    return this.http.post(url, data , this.httpOptions);
  }
  update(data, id): Observable<any> {
    const url = environment.apiUrl + `/api/v1/admin/user/update/${id}`;
    return this.http.post(url, data , this.httpOptions);
  }
  deletePhotos(data, id): Observable<any> {
    const url = environment.apiUrl + `/api/v1/admin/user/deletephotos/${id}`;
    return this.http.post(url, data , this.httpOptions);
  }
  delete(id): Observable<any> {
    const url = environment.apiUrl + `/api/v1/admin/user/delete/${id}`;
    return this.http.delete(url , this.httpOptions);
  }
}
