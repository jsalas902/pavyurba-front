import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { GenericService, AuthenticationService } from '@app/_services';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Pagination } from '@app/_models';
import { Incident } from '@app/_models/Incident';

@Injectable({
  providedIn: 'root'
})
export class IncidentsService extends GenericService {

  constructor(private http: HttpClient,
    public authService: AuthenticationService,
    private injector: Injector) {
    super();
  }


  ListarIncidencias(skip: string, take: string, query: string, id: string): Observable<Pagination> {

    let headers = new HttpHeaders()
    .set('Accept', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN_LL_CMS'));
    return this.http.get(
      environment.apiUrl + '/incidencias/listarPorEmpleado/' + id + '?skip=' + skip + '&take=' + take + '&query=' + query, { headers: headers })
    .pipe(
      map(
        (respuesta: any) => {
          return respuesta;
        }
      ),
      catchError((error: any) => {
        this.authService = this.injector.get(AuthenticationService);
        return this.HandleError(error);
      })
    );
  }
}
