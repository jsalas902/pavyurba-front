import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { Condition } from '@app/conditions/models/condition';
import { ConditionsService } from '@app/conditions/services/conditions.service';
import { TypesOfDietManagement } from '@app/dietas/models/constantes/TypesOfDietManagement';

@Component({
  selector: 'app-conditions',
  templateUrl: './conditions.component.html',
  styleUrls: ['./conditions.component.css']
})
export class ConditionsComponent implements OnInit {
  @BlockUI('componentsConditions') blockUIMultipleSelect: NgBlockUI;

  public rightIconCollapse1 = false;
  public rightIconCollapse2 = false;
  public rightIconCollapse3 = false;

  conditions: Condition = new Condition();
  conditionsCombo: Condition = new Condition();
  cargaCompleta = false;
  types: any;

  constructor(
    private toastr: ToastrService, 
    private conditionsService: ConditionsService
  ) {}

  ngOnInit(): void {
  
    this.obtenerConditions();
    this.types = {
      "Total": TypesOfDietManagement.Total,
      "Parcial": TypesOfDietManagement.Parcial,
      "Catalago": TypesOfDietManagement.Catalago
    };
  }
  onReset($event){
    this.ngOnInit();
  }
  obtenerConditions() {
    this.reloadStart();
    this.conditionsService.ListarTodos().subscribe(
      data => {
        this.conditions =data;
        // this.cargaCompleta = true;
        // this.reloadStop();
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );

    this.conditionsService.ListarCondicionesCombo().subscribe(
      data => {
        this.conditionsCombo =data;
        this.cargaCompleta = true;
        this.reloadStop();
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }  
  reloadStart() {
    this.blockUIMultipleSelect.start('Loading..');
  }

  reloadStop() {
    this.blockUIMultipleSelect.stop();
    this.blockUIMultipleSelect.isActive = false;
    this.blockUIMultipleSelect.reset();
  }
}
