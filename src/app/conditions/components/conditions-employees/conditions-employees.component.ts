import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { UnidadesService } from '@app/_services/unidades.service';
import { ConditionsService } from '@app/conditions/services/conditions.service';
import { EmployeesService } from '@app/employees/services/employees.service';
import { TypesOfDietManagement } from '@app/dietas/models/constantes/TypesOfDietManagement'
import { ConditionsValidator } from "@app/conditions/validation/conditions.validator";
import { DietasService } from '@app/dietas/services/dietas.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Employee } from '@app/employees/models/Employee';
import { ConfirmDialogService } from '@app/_services';

@Component({
  selector: 'app-conditions-employees',
  templateUrl: './conditions-employees.component.html',
  styleUrls: ['./conditions-employees.component.css']
})
export class ConditionsEmployeesComponent implements OnInit {

  @Input() idEmployees: string;  
  @Input() dietasFueraMadrid: string;
  @Input() dietasDentroMadrid: string;
  @BlockUI('repeatingForms') blockUIRepeatingForms: NgBlockUI;

  @Output() closeModal = new EventEmitter<boolean>();

  repeatForm: FormGroup;
  public repeatList: FormArray;
  conditions: any;
  data: any;
  submitted: boolean = false;
  conditions_validation_messages = ConditionsValidator.CONDITIONS_VALIDATION_MESSAGES;
  employees: Employee = new Employee();
  employeesForm: FormGroup;
  
  mensaje: string = '';
  maxLength: number = 500;

  get repeatFormGroup() {
    return this.repeatForm.get('repeatArray') as FormArray;
  }
  get f() {
    return this.repeatForm.controls;
  }

  constructor(
    private formBuilder: FormBuilder,
    private unidadesService: UnidadesService,
    private confirmDialogService: ConfirmDialogService,
    private conditionsService: ConditionsService,
    private employeesService: EmployeesService,
    private dietasService: DietasService,
    private toastr: ToastrService) { }

  ngOnInit(): void {    
    this.repeatForm = this.formBuilder.group({
      idsEmpleados:[[this.idEmployees]],
      repeatArray: this.formBuilder.array([]),
      dietaDentroMadrid: [null, Validators.required],
      dietaFueraMadrid: [null, Validators.required],
      observaciones: [null],
    });  
    this.obtenerConditions();
    this.obtenerEmployees();
    this.repeatList = this.repeatForm.get('repeatArray') as FormArray;
  }

  obtenerConditions() {
    // this.data.forEach(element => {
    //   this.addRepeat()
    // });
    this.conditionsService.ListarCondicionesCombo().subscribe(
      data => {
        this.data = data;
        // this.data.forEach((element: string, index: number) => {
        //   if((index + 1) !== this.data.length)
        //     this.addRepeat()
        // });

        // console.log(this.repeatList);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  add() {
    this.submitted = true;
    if (this.repeatForm.invalid) {
      return;
    }
    this.reloadStart();
    this.conditions = this.repeatForm.value; 
    this.create();

  }
  create(){
    /*this.conditionsService.CrearWhithEmpleado(this.conditions.repeatArray,'parcial').subscribe(
      data => {
        this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
        // this.reiniciarFormulario();
        console.log('create',data);
      },
      error => {
        // this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });  */

    // let dietasTem = {
    //   "dietas": [
    //       {
    //         "id": 1,
    //         "valor": this.conditions.dietaFueraMadrid,
    //         "fueraMadrid": true
    //       },
    //       {
    //         "id": 2,
    //         "valor": this.conditions.dietaDentroMadrid,
    //         "fueraMadrid": false
    //       }
    //   ],
    //   "idsEmpleados": [this.idEmployees]
    // }
    
    // this.dietasService.Editar(dietasTem, 'parcial').subscribe(
    //   data => {
    //     // this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
    //     this.submitted = false;
    //   },
    //   error => {
    //     // this.reloadStop();
    //     // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
    //     this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    // });

    let employeesTem = new Employee();
    // console.log(this.employees);
    employeesTem = this.changeObject(this.employees, false);
    employeesTem.observaciones = this.conditions.observaciones;
    employeesTem.dietaDentroMadrid = this.conditions.dietaDentroMadrid;
    employeesTem.dietaFueraMadrid = this.conditions.dietaFueraMadrid;
    employeesTem.condiciones = this.conditions.repeatArray;

    this.employeesService.Editar(employeesTem).subscribe(
      data => {
        // this.closeModal.emit(true);
        this.toastr.success(`${data.mensaje}`,'Actualizado' , { timeOut: 2500, closeButton: true });
        // this.getAllData();
        
        while (this.repeatList.length !== 0) {
          this.repeatList.removeAt(0)
        }
        this.obtenerEmployees();
        this.submitted = false;
      },
      error => {
        this.reloadStop();
        // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
        this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
    });
  }
  createRepeat(data): FormGroup {
    return this.formBuilder.group({
      id: [data.id],
      idCondicion: [data.idCondicion, Validators.required],
      valorCondicionDM: [data.valorCondicionDM, Validators.required],
      valorCondicionFM: [data.valorCondicionFM, Validators.required],
      // idsEmpleados: [[this.idEmployees]]
    });    
  }
  addRepeat(data) {
    this.repeatList = this.repeatForm.get('repeatArray') as FormArray;
    this.repeatList.push(this.createRepeat(data));
  }
  
  createRepeatNew(): FormGroup {
    return this.formBuilder.group({
      id: [null],
      idCondicion: [null, Validators.required],
      valorCondicionDM: ['', Validators.required],
      valorCondicionFM: ['', Validators.required],
      // idsEmpleados: [[this.idEmployees]]
    });    
  }
  addRepeatNew() {
    this.repeatList = this.repeatForm.get('repeatArray') as FormArray;
    this.repeatList.push(this.createRepeatNew());
  }  
  removeRepeat(index, datos) {
    if(datos.id.value != null) {
      this.confirmDialogService.confirmThis(`Está seguro de eliminar esta condición`, () =>  {
        this.conditionsService.Eliminar(datos.id.value).subscribe(
          data => {
            this.toastr.success(`${data.mensaje}`,'Eliminar' , { timeOut: 2500, closeButton: true });
            this.repeatList.removeAt(index);  
          },
          error => {
            this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        });
          
      }, () => {
        this.toastr.error(`Proceso cancelado por el usuario`);
      });  
    }  else {
      this.repeatList.removeAt(index);  
    } 
  }
  reiniciarFormulario() {
		this.repeatFormGroup.reset();
	}
  obtenerEmployees() {
    this.employeesService.ObtenerPorId(this.idEmployees).subscribe(
      data => {
        this.obtenerPatchEmployee(data);
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }
  obtenerPatchEmployee(data){
    this.employees = this.changeObject(data, true);
    this.repeatForm.patchValue({
      observaciones: this.employees.observaciones,
      dietaDentroMadrid: this.employees.dietaDentroMadrid,
      dietaFueraMadrid: this.employees.dietaFueraMadrid,
    });
    this.reloadStop();
  }
  changeObject(employees: Employee, valid: boolean) {
    // console.log(employees);
    let employeesTem: Employee = new Employee();
    employeesTem.id = employees.id;
    employeesTem.identificacion = employees.identificacion;
    employeesTem.nombre = employees.nombre;
    employeesTem.nacionalidad = employees.nacionalidad;
    employeesTem.estadoCivil = employees.estadoCivil;
    employeesTem.numeroSeguroSocial = employees.numeroSeguroSocial;
    employeesTem.tipo = employees.tipo;
    employeesTem.fijo = employees.fijo;
    employeesTem.fechaAlta = employees.fechaAlta;
    employeesTem.fechaBaja = employees.fechaBaja;
    employeesTem.fechaConversion = employees.fechaConversion;
    employeesTem.claseAutonomo = employees.claseAutonomo;
    employeesTem.email = employees.email;
    employeesTem.emailAdicional = employees.emailAdicional;
    employeesTem.telefono = employees.telefono;
    employeesTem.telefonoAdicional = employees.telefonoAdicional;
    employeesTem.observaciones = employees.observaciones;
    employeesTem.direccion = employees.direccion;
    employeesTem.dietaDentroMadrid = employees.dietaDentroMadrid;
    employeesTem.dietaFueraMadrid = employees.dietaFueraMadrid;
    employeesTem.repercusionGastosGenerales = employees.repercusionGastosGenerales;
    employeesTem.almacenaHucha = employees.almacenaHucha;
    employeesTem.hucha = employees.hucha;
    employeesTem.huchaBase = employees.huchaBase;
    employeesTem.idLocalidad = employees.idLocalidad;
    employeesTem.idProvincia = employees.idProvincia;
    employeesTem.idEmpresa = employees.idEmpresa;
    employeesTem.condiciones = employees.condiciones;

    if(valid) {
      employeesTem.condiciones.forEach((element: string, index: number) => {
        // if((index + 1) !== employeesTem.condiciones.length)
          this.addRepeat(element)
      });
    }

    this.repeatForm.controls['observaciones'].setValue(employeesTem.observaciones);
    return employeesTem;
  }

   
  reloadStart() {
    this.blockUIRepeatingForms.start('Loading..');
  }

  reloadStop() {
    this.blockUIRepeatingForms.stop();
    this.blockUIRepeatingForms.isActive = false;
    this.blockUIRepeatingForms.reset();
  }

}
