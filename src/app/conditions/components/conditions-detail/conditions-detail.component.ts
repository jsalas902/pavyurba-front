import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import { ToastrService } from 'ngx-toastr';
import { UnidadesService } from '@app/_services/unidades.service';
import { ConditionsService } from '@app/conditions/services/conditions.service';
import { EmployeesService } from '@app/employees/services/employees.service';
import { TypesOfDietManagement } from '@app/dietas/models/constantes/TypesOfDietManagement'
import { ConditionsValidator } from "@app/conditions/validation/conditions.validator";
import { ConfirmDialogService } from '@app/_services';

@Component({
  selector: 'app-conditions-detail',
  templateUrl: './conditions-detail.component.html',
  styleUrls: ['./conditions-detail.component.css']
})
export class ConditionsDetailComponent implements OnInit {
  @BlockUI('repeatingForms') blockUIRepeatingForms: NgBlockUI;

  @Input() tipoGestion: string;
  @Input() data: any;
  @Input() combo: any;
  @Output() reset = new EventEmitter<boolean>();

  repeatForm: FormGroup;
  public repeatList: FormArray;
  unidades: any;
  conditions: any;
  submitted: boolean = false;
  types: any;
  titel: string = '';
  conditions_validation_messages = ConditionsValidator.CONDITIONS_VALIDATION_MESSAGES;
  multipleMultiSelect: any;
  public multipleSelectEmpleados: any;
  mensaje: string[] = [];  
  maxLength: number = 500;

  get repeatFormGroup() {
    return this.repeatForm.get('repeatArray') as FormArray;
  }
  get f() {
    return this.repeatForm.controls;
  }

  constructor(
    private formBuilder: FormBuilder,
    private unidadesService: UnidadesService,
    private confirmDialogService: ConfirmDialogService,
    private conditionsService: ConditionsService,
    private employeesService: EmployeesService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.reloadStart();
    this.types = {
      "Total": TypesOfDietManagement.Total,
      "Parcial": TypesOfDietManagement.Parcial,
      "Catalago": TypesOfDietManagement.Catalago
    };
    
    this.multipleSelectEmpleados = [];
    this.obtenerUnidadesMedidas();
    this.obtenerConditionsLocal();
    
    this.titel = this.tipoGestion === TypesOfDietManagement.Total ? `Seguro quiere realizar el cambio de condiciones a todos los trabajadores, esta acción es Irreversible  ?`: this.tipoGestion === TypesOfDietManagement.Parcial ? `Seguro quiere realizar el cambio de condiciones a estos trabajadores ?`: `Seguro quiere realizar el cambio solo en el modulo de Condiciones ?`;   
    
  }
  obtenerEmployees() {
    this.employeesService.ListarEmployeesCombo().subscribe(
      data => {
        this.multipleSelectEmpleados = data;
        // console.log('obtenerEmployees',data); 
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  isCatalago(): boolean{
    return this.tipoGestion === TypesOfDietManagement.Catalago;
  }
  obtenerConditions() {
    // console.log(this.data);
    this.data.forEach((element, index) => {
      this.addRepeat()
    });
  }
  obtenerUnidadesMedidas() {
    this.unidadesService.ListarUnidadesMedidasCombo().subscribe(
      data => {
        this.unidades = data;
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  }

  add() {
    if (this.repeatFormGroup.invalid) {
      return;
    }
    this.submitted = true;
    if(this.isCatalago()){
      this.setDirty();
    }else {
      this.setEmpleados();
    }
    this.conditions = this.repeatFormGroup.value; 

    this.confirmDialogService.confirmThis(`${this.titel}`, () =>  {
      this.create();      
    }, () => {
      this.toastr.error(`Proceso cancelado por el usuario`);
    });
    

  }
  setDirty(){
    this.repeatFormGroup.controls.forEach( (element, index, arr) => {
      this.repeatFormGroup.value[index].dirty = element.dirty;
      // console.log('dirty',element.dirty);
    });
  }
  setEmpleados(){
    this.repeatFormGroup.controls.forEach( (element, index, arr) => {
      this.repeatFormGroup.value[index].idsEmpleados = this.repeatForm.value.idsEmpleados;
      // console.log('setEmpleados',this.repeatForm.value);
    });
  }
  obtenerConditionsLocal() {    
    this.conditionsService.ListarTodos().subscribe(
      data => {
        // this.conditions =data;
        this.data = data;
        if(this.isCatalago()){
          this.obtenerConditions();
          this.patchConditionValues();  
        }
        
        // this.cargaCompleta = true;
        // this.reloadStop();
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );

    this.conditionsService.ListarCondicionesCombo().subscribe(
      data => {
        this.combo =data;
        this.reloadStop();
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );

    if(this.isCatalago()){
      this.repeatForm = this.formBuilder.group({
        repeatArray: this.formBuilder.array([])
      });  
      this.repeatList = this.repeatForm.get('repeatArray') as FormArray;     
      
    }else {
      this.repeatForm = this.formBuilder.group({
        idsEmpleados:[null],
        repeatArray: this.formBuilder.array([this.createRepeat()])
      });  
      this.repeatList = this.repeatForm.get('repeatArray') as FormArray;
      this.obtenerEmployees();
    }
  }

  create(){
    this.reloadStart();
    if(this.isCatalago()){    
      this.conditionsService.Crear(this.conditions).subscribe(
        data => {
          this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });
          // this.reiniciarFormulario();

          while (this.repeatList.length !== 0) {
            this.repeatList.removeAt(0)
          }
          this.obtenerConditionsActualizar();          
        },
        error => {
          // this.reloadStop();
          // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
          this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
      });
    }else {
      
      var empleadosID = []; 
      if(this.conditions[0].idsEmpleados !== null) {
        this.conditions[0].idsEmpleados.forEach(element => {
          empleadosID.push(element.id);
        });
      }
      let conditionsTerm = {
        "idCondicion": this.conditions[0].idCondicion,
        "valorCondicionDM": this.conditions[0].valorCondicionDM,
        "valorCondicionFM": this.conditions[0].valorCondicionFM,
        "idsEmpleados": (this.conditions[0].idsEmpleados !== null) ? empleadosID : this.conditions[0].idsEmpleados
      }

      var ConditionsArray = [conditionsTerm];

      this.conditionsService.CrearWhithEmpleado(ConditionsArray,this.tipoGestion).subscribe(
        data => {
          this.toastr.success(`${data.mensaje}`,'Nuevo' , { timeOut: 2500, closeButton: true });          
          // this.reiniciarFormulario();
        },
        error => {
          // this.reloadStop();
          // this.toastr.error('Error', `${error.mensaje}`, { timeOut: 2500, closeButton: true });
          this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
      });
    }
  }
  createRepeat(): FormGroup {
    if(this.isCatalago()){
      // console.log('Catalago', this.tipoGestion );
      return this.formBuilder.group({
        id: [''],
        nombre: ['', Validators.required],
        valor: ['', Validators.required],
        dirty: [false, Validators.required],
        idUnidadMedida: [null, Validators.required],
        valorDM: [null, Validators.required],
        valorFM: [null, Validators.required],
        observaciones: [null],
      });
    }else {
      // console.log('otros', this.tipoGestion );
      return this.formBuilder.group({
        idCondicion: [null],
        valorCondicionDM: ['', Validators.required],
        valorCondicionFM: ['', Validators.required],
        idsEmpleados: []
      });
    }
  }
  addRepeat() {
    this.repeatList = this.repeatForm.get('repeatArray') as FormArray;
    this.repeatList.push(this.createRepeat());
  }
  removeRepeat(index, datos) {
    if(datos.id.value != null && datos.id.value != "") {
      this.confirmDialogService.confirmThis(`Está seguro de eliminar esta condición`, () =>  {
        this.conditionsService.Eliminar(datos.id.value).subscribe(
          data => {
            this.toastr.success(`${data.mensaje}`,'Eliminar' , { timeOut: 2500, closeButton: true });
            this.repeatList.removeAt(index);  
          },
          error => {
            this.toastr.error(`${error}`, 'Error', { timeOut: 2500, closeButton: true });
        });
           
      }, () => {
        this.toastr.error(`Proceso cancelado por el usuario`);
      }); 
    } else {
      this.repeatList.removeAt(index); 
    }
  }
  patchConditionValues() {
    // console.log(this.data);
    this.repeatFormGroup.patchValue(this.data);
    this.data.forEach((element, index) => {
      if(element.observaciones != null) {
        this.mensaje[index] = element.observaciones
      }
    });
    this.reloadStop();
  }
  reiniciarFormulario() {
		this.repeatFormGroup.reset();
	}

  obtenerConditionsActualizar() {
    this.conditionsService.ListarTodos().subscribe(
      data => {
        this.data = data;
        this.obtenerConditions();
        this.patchConditionValues();
      },
      error => {
        // this.toastr.error(error.mensaje);
        this.toastr.error(error);
      }
    );
  } 
  reloadStart() {
    this.blockUIRepeatingForms.start('Loading..');
  }

  reloadStop() {
    this.blockUIRepeatingForms.stop();
    this.blockUIRepeatingForms.isActive = false;
    this.blockUIRepeatingForms.reset();
  }
}
