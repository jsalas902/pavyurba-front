export class ConditionsValidator {
    public static CONDITIONS_VALIDATION_MESSAGES = {
        'nombre': [
            { type: 'required', message: 'Nombre es requerido' },
            { type: 'minlength', message: 'El Nombre no puede tener menos de 3 caracteres' },
            { type: 'maxlength', message: 'El Nombre no puede tener más de 100 caracteres' }
        ],
        'idCondicion': [
            { type: 'required', message: 'Nombre es requerido' }
        ],
        'valor': [
            { type: 'required', message: 'Espesor es requerido' },
        ],
        'valorDM': [
            { type: 'required', message: 'Valor DM es requerido' },
        ],
        'valorFM': [
            { type: 'required', message: 'Valor FM es requerido' },
        ],
        'valorCondicionDM': [
            { type: 'required', message: 'Valor en madrid es requerido' },
        ],
        'valorCondicionFM': [
            { type: 'required', message: 'Valor fuera de Madrid es requerido' },
        ],
        'idUnidadMedida': [
            { type: 'required', message: 'Unidad Medida es requerido' },
        ],
        'idsEmpleados': [
            { type: 'required', message: 'Empleados es requerido' },
        ],
        'dietaDentroMadrid': [
            { type: 'required', message: 'Dieta en Madrid es requerido' },
        ],
        'dietaFueraMadrid': [
            { type: 'required', message: 'Dieta Fuera Madrid es requerido' },
        ],
    }
}