export class Condition {

    public constructor(init?: Partial<Condition>) {
        Object.assign(this, init);
    }

    id: number;
    nombre: string;
    valor: number;
    idUnidadMedida: number;
    unidadMedida: string;
    dirty: boolean;
    habilitado: boolean;
    fechaHoraLog: Date;
    usuarioLog: string;
   
}