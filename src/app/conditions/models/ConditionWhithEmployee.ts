export class ConditionWhithEmployee {

    public constructor(init?: Partial<ConditionWhithEmployee>) {
        Object.assign(this, init);
    }

    idCondicion: number;
    valorCondicionDM: number;
    valorCondicionFM: number;
    idsEmpleados: any;
    empleado: string;
   
}